<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');

class MY_Controller extends CI_Controller {
    private $header;
    private $navbar;
    private $config;

    public function __construct() {
        parent::__construct();

        // load required things
        $this->load->database();
        $this->load->library('session');

        // load configuration
        $this->config_load();
    }

	public function template($page = 'overview', $param = array()) {
        // format page name
        $temp = explode('/', $page);
        $count = count($temp);

        if($count > 1) {
            for($i = 0; $i < $count; $i++)
                $temp[$i] = ucfirst($temp[$i]);

            $title = $temp[0];
            $dashboard = implode(' | ', $temp);
        }else {
            $title = ucfirst($temp[0]);
            $dashboard = ucfirst($temp[0]);
        }

        // set page name
        $this->header['page_name'] = array(
            'title' => $title,
            'dashboard' => $dashboard
        );

        // set param theme
        $param['theme'] = $this->config['system']['theme'];

        $this->load->view($this->config['system']['theme'].'/extension/header', $this->header);
        $this->load->view($this->config['system']['theme'].'/extension/navbar', $this->navbar);
        $this->load->view($this->config['system']['theme'].'/admin/'.$page, $param);
        $this->load->view($this->config['system']['theme'].'/extension/footer');
    }
    
    public function error_404() {
        // set page name
        $this->header['page_name'] = array(
            'title' => '404',
            'dashboard' => 'Error 404'
        );

        $this->load->view($this->config['system']['theme'].'/extension/header', $this->header);
        $this->load->view($this->config['system']['theme'].'/extension/navbar', $this->navbar);
        $this->load->view($this->config['system']['theme'].'/errors/error404');
        $this->load->view($this->config['system']['theme'].'/extension/footer');
    }

    // miscellaneous function
    public function config_load() {
        $config['system']['site'] = 'Bootstrap';
        $config['system']['company'] = 'CC Solutions';
        $config['system']['email'] = 'support@app.nedge.com';
        $config['system']['language'] = 'en';
        $config['system']['theme'] = 'default';
        $config['system']['calendar'] = 1;
        $config['system']['date_format'] = 5;
        $config['system']['time_format'] = 2;
        $config['system']['scroll'] = 1;
        $config['system']['maxrow'] = 20;
        $config['system']['email_protocol'] = 1;

        $config['account']['method'] = 'lifo';
        $config['account']['currency'] = 1;
        // $config['account']['symbol'] = 'PHP';
        $config['account']['symbol'] = '₱';
        $config['account']['format'] = 1;
        $config['account']['decimal'] = 2;
        $config['account']['oversell'] = 2;
        $config['account']['reference'] = 1;
        $config['account']['cart'] = 1;
        $config['account']['tax'] = 1;

        $config['prefix']['sale'] = 'SL';
        $config['prefix']['sale_return'] = 'SR';
        $config['prefix']['payment'] = 'PAY';
        $config['prefix']['purchase'] = 'PU';
        $config['prefix']['purchase_pay'] = 'PPY';
        $config['prefix']['purchase_return'] = 'PR';
        $config['prefix']['delivery'] = 'DR';
        $config['prefix']['quotation'] = 'QU';
        $config['prefix']['transfer'] = 'TR';
        $config['prefix']['expense'] = 'EX';
        $config['prefix']['adjust'] = 'AT';

        $config['prefix']['rewards'] = 1;
        $config['prefix']['rewards_erate'] = '1000,1';
        $config['prefix']['rewards_crate'] = '1000,2';

        // build header and navbar info
        $header['meta_description'] = 'pos inventory';
        $header['meta_keywords'] = 'pos, inventory, stock manager';
        $header['meta_author'] = 'james malatabon';
        $header['site_name'] = $config['system']['site'];

        $navbar['site_logo'] = '';
        $navbar['quantity_alert'] = 3;
        $navbar['user_image'] = '';
        $navbar['user_fullname'] = $this->session->userdata('name');

        // set local values
        $this->config = $config;
        $this->header = $header;
        $this->navbar = $navbar;
    }

    public function config($filter = 'internal') {
        if($filter != 'internal') {
            $temp = array(
                'scroll' => $this->config['system']['scroll'],
                'symbol' => $this->config['account']['symbol'],
                'prefix' => $this->config['prefix']
            );
        }else
            $temp = $this->config;

        return $temp;
    }
}
