<div v-if="pageContent == 'system'" class="card bg-dark text-dark text-center">
    <div class="card-header pb-0">
        <ul class="nav nav-tabs border-bottom-0" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#site" role="tab" aria-selected="true">
                    Site Configuration
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#accounting" role="tab">
                    Accounting & Sales
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#products" role="tab">
                    Products
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#prefix" role="tab">
                    Prefix & Rewards
                </a>
            </li>
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="site" role="tabpanel">

                <h2 class="font-weight-light mb-1">Site Cofiguration</h2>
                <p class="font-weight-light">
                    Update your site information and preferences.
                </p>
                <hr class="my-4">
                <div class="content text-left">
                    <!-- notification message -->
                    <div v-if="notif.target == 'system.site'" class="alert alert-info mb-4" role="alert">
                        <h4 class="alert-heading">{{ notif.heading }}</h4>
                        <p>{{ notif.message }}</p>
                        <hr>
                        <p class="mb-0">{{ notif.footer }}</p>
                    </div>

                    <form @submit.prevent="onSubmitSystem" name="site" class="row">

                        <div class="form-group col-md-4">
                            <label>Site Name</label>
                            <input name="siteName" type="text" class="form-control" placeholder="Enter site name" required>
                            <small class="form-text text-muted">This will be the web app name</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Company Name</label>
                            <input name="companyName" type="text" class="form-control" placeholder="Enter company name">
                        </div>

                        <div class="form-group col-md-4">
                            <label>Email</label>
                            <input name="email" type="email" class="form-control" placeholder="Enter email" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Language</label>
                            <select name="language" class="form-control">
                                <option value="en">English</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Theme</label>
                            <select name="theme" class="form-control">
                                <option value="default">Default</option>
                            </select>
                        </div>

                        <hr class="col-md-12">
                        <div class="form-group col-md-4">
                            <label>Calendar</label>
                            <select name="calendar" class="form-control">
                                <option value="1">Adminstrators</option>
                                <option value="2">Everyone</option>
                            </select>
                            <small class="form-text text-muted">Who can view calendar</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Date Format</label>
                            <select name="dateFormat" class="form-control">
                                <option value="1">DD/MM/YYYY</option>
                                <option value="2">MM/DD/YYYY</option>
                                <option value="3">DD-MM-YYYY</option>
                                <option value="4">MM-DD-YYYY</option>
                                <option value="5" selected>MMM DD, YYYY</option>
                                <option value="6">DD, MMM YYYY</option>
                            </select>
                            <small class="form-text text-muted">Example: Oct 24, 2018</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Time Format</label>
                            <select name="timeFormat" class="form-control">
                                <option value="1">HH:MM:SS</option>
                                <option value="2" selected>HH:MM Z</option>
                            </select>
                            <small class="form-text text-muted">Example: 03:36 PM</small>
                        </div>

                        <hr class="col-md-12">
                        <div class="form-group col-md-4">
                            <label>Pagination</label>
                            <select name="pagination" class="form-control">
                                <option value="1">Row Limit</option>
                                <option value="2">Ifinite Scroll</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Page Rows</label>
                            <input name="maxRows" type="number" class="form-control" value=20 min=20 placeholder="Enter rows per page" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Email Protocol</label>
                            <select name="emailProtocol" class="form-control">
                                <option value="1">PHP Mail Function</option>
                                <option value="2">Send Mail</option>
                                <option value="3">SMTP</option>
                            </select>
                            <small class="form-text text-muted">Not applicable for local setup</small>
                        </div>

                        <hr class="col-md-12">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-success">
                                <i class="fas fa-cog pr-2"></i>Save Preference
                            </button>
                        </div>

                    </form>
                </div>

            </div>
            <div class="tab-pane fade" id="accounting" role="tabpanel">

                <h2 class="font-weight-light mb-1">Accounting and Sales</h2>
                <p class="font-weight-light">
                    Update accounting and sales preferences.
                </p>
                <hr class="my-4">
                <div class="content text-left">
                    <!-- notification message -->
                    <div v-if="notif.target == 'system.accounting'" class="alert alert-info mb-4" role="alert">
                        <h4 class="alert-heading">{{ notif.heading }}</h4>
                        <p>{{ notif.message }}</p>
                        <hr>
                        <p class="mb-0">{{ notif.footer }}</p>
                    </div>

                    <form @submit.prevent="onSubmitSystem" name="accounting" class="row">

                        <div class="form-group col-md-4">
                            <label>Acounting Method</label>
                            <select name="accountingMethod" class="form-control">
                                <option value="1">Average Cost</option>
                                <option value="2">First In First Out</option>
                                <option value="3">Last In First Out</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Currency</label>
                            <select name="currency" class="form-control">
                                <option value="1">Philippine Peso</option>
                            </select>
                            <small class="form-text text-muted">System will use this currency</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Currency Symbol</label>
                            <input name="currencySymbol" type="text" class="form-control" value="PHP" placeholder="Enter currency symbol">
                        </div>

                        <div class="form-group col-md-4">
                            <label>Currency Format</label>
                            <select name="currencyFormat" class="form-control">
                                <option value="1">Before Money</option>
                                <option value="2">After Money</option>
                            </select>
                            <small class="form-text text-muted">Example: PHP 1,000</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Decimal Places</label>
                            <input name="decimalPlaces" type="text" class="form-control" value=2 min=0 max=6 placeholder="Enter currency symbol" required>
                        </div>

                        <hr class="col-md-12">
                        <div class="form-group col-md-4">
                            <label>Over Selling</label>
                            <select name="overSelling" class="form-control">
                                <option value="1">Enable Over Selling</option>
                                <option value="2" selected>Disable Over Selling</option>
                            </select>
                            <small class="form-text text-muted">Allow negative quantities</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Reference Format</label>
                            <select name="referenceFormat" class="form-control">
                                <option value="1">Prefix [ Sequence ]</option>
                                <option value="2">Prefix [ Random ]</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Cart Handling</label>
                            <select name="cartMethod" class="form-control">
                                <option value="1">Unique Entry</option>
                                <option value="2">Multiple Entry</option>
                            </select>
                            <small class="form-text text-muted">When adding items to cart</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Tax Method</label>
                            <select name="taxMethod" class="form-control">
                                <option value="1">Tax Free</option>
                            </select>
                        </div>

                        <hr class="col-md-12">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-success">
                                <i class="fas fa-cog pr-1"></i> Save Preference
                            </button>
                        </div>

                    </form>
                </div>

            </div>
            <div class="tab-pane fade" id="products" role="tabpanel">

                <h2 class="font-weight-light mb-1">Products</h2>
                <p class="font-weight-light">
                    Update product handling preferences.
                </p>
                <hr class="my-4">
                <div class="content text-left">
                    <!-- notification message -->
                    <div v-if="notif.target == 'system.product'" class="alert alert-info mb-4" role="alert">
                        <h4 class="alert-heading">{{ notif.heading }}</h4>
                        <p>{{ notif.message }}</p>
                        <hr>
                        <p class="mb-0">{{ notif.footer }}</p>
                    </div>

                    <form @submit.prevent="onSubmitSystem" name="product" class="row">

                        <div class="form-group col-md-4">
                            <label>Expiration</label>
                            <select name="expiration" class="form-control">
                                <option value="1">Enable Expiration</option>
                                <option value="2">Disable Expiration</option>
                            </select>
                            <small class="form-text text-muted">Allow expiration of products</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Remove Expired</label>
                            <select name="removeExpired" class="form-control">
                                <option value="1">Enable Auto-Remove</option>
                                <option value="2">Disable Auto-Remove</option>
                            </select>
                            <small class="form-text text-muted">Auto remove expired products</small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Unit Case Break</label>
                            <select name="unitCaseBreak" class="form-control">
                                <option value="1">Enable Case Break</option>
                                <option value="2">Disable Case Break</option>
                            </select>
                            <small class="form-text text-muted">Out of stock child products will get quantity to parent products</small>
                        </div>

                        <hr class="col-md-12">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-success">
                                <i class="fas fa-cog pr-1"></i> Save Preference
                            </button>
                        </div>

                    </form>
                </div>

            </div>
            <div class="tab-pane fade" id="prefix" role="tabpanel">

                <h2 class="font-weight-light mb-1">Prefix and Rewards</h2>
                <p class="font-weight-light">
                    Set prefixes and rewards calculation.
                </p>
                <hr class="my-4">
                <div class="content text-left">
                    <!-- notification message -->
                    <div v-if="notif.target == 'system.prefix'" class="alert alert-info mb-4" role="alert">
                        <h4 class="alert-heading">{{ notif.heading }}</h4>
                        <p>{{ notif.message }}</p>
                        <hr>
                        <p class="mb-0">{{ notif.footer }}</p>
                    </div>

                    <form @submit.prevent="onSubmitSystem" name="prefix" class="row">
                        
                        <div class="form-group col-md-4">
                            <label>Sales</label>
                            <input name="sales" type="text" class="form-control" value="SL" placeholder="Prefix Sales" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Sales Return</label>
                            <input name="salesReturn" type="text" class="form-control" value="SLR" placeholder="Prefix Sales Return" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Payment</label>
                            <input name="payment" type="text" class="form-control" value="PAY" placeholder="Prefix Payment" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Purchase</label>
                            <input name="purchase" type="text" class="form-control" value="PU" placeholder="Prefix Purchase" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Purchase Payment</label>
                            <input name="purchasePayment" type="text" class="form-control" value="PUP" placeholder="Prefix Purchase Payment" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Purchase Return</label>
                            <input name="purchaseReturn" type="text" class="form-control" value="PUR" placeholder="Prefix Purchase Return" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Delivery</label>
                            <input name="delivery" type="text" class="form-control" value="DL" placeholder="Prefix Delivery" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Quotation</label>
                            <input name="quotation" type="text" class="form-control" value="QU" placeholder="Prefix Quotation" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Transfer</label>
                            <input name="transfer" type="text" class="form-control" value="TF" placeholder="Prefix Transfer" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Expense</label>
                            <input name="expense" type="text" class="form-control" value="EX" placeholder="Prefix Expense" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Adjustment</label>
                            <input name="adjustment" type="text" class="form-control" value="ADJ" placeholder="Prefix Adjustment" required>
                        </div>

                        <hr class="col-md-12">
                        <div class="form-group col-md-4">
                            <label>Rewards</label>
                            <select name="allowRewards" class="form-control">
                                <option value="1">Enable Rewards</option>
                                <option value="2">Disable Rewards</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Employee Rewards</label>
                            <input name="employeeReward" type="text" class="form-control" placeholder="Enter rewards rate">
                            <small class="form-text text-muted">
                                Enter the sales amount first then the equivalent point separated by comma.
                                <br>
                                <br>Below example is 1 reward point every 1000 sales amount.
                                <br>
                                <br>Example: 1000, 1
                            </small>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Customer Rewards</label>
                            <input name="customerReward" type="text" class="form-control" placeholder="Enter rewards rate">
                            <small class="form-text text-muted">
                                Enter the spent amount first then the equivalent point separated by comma.
                                <br>
                                <br>Below example is 2 rewards points every 1000 amount spent.
                                <br>
                                <br>Example: 1000, 2
                            </small>
                        </div>

                        <hr class="col-md-12">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-success">
                                <i class="fas fa-cog pr-1"></i> Save Preference
                            </button>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>
</div>