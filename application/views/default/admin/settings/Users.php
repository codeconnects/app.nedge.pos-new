<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

            <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">

            </div>
        </div>

        <div class="row px-3 mb-3">
            <!-- search section -->
            <div class="col px-md-0 pb-2 pb-md-0">
                <div class="input-group">
                    <input v-model="search" v-on:input="populate()" type="text" class="form-control" placeholder="Search users">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" disabled>
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- action section -->
            <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                <div class="btn-group" role="group">
                    <a href="users/create" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Create User">
                        <i class="fas fa-user-plus"></i>
                    </a>

                    <button @click="gotoUpdate()" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Update User">
                        <i class="fas fa-user-edit"></i>
                    </button>

                    <button @click="show('delete')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Delete User">
                        <i class="fas fa-user-slash"></i>
                    </button>

                    <button @click="show('detail')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Detail">
                        <i class="fas fa-search"></i>
                    </button>
                </div>

                <div class="dropdown ml-2">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="-138,0">
                        <i class="fas fa-cog"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a href="../settings/permissions" class="dropdown-item">
                            <i class="fas fa-user-shield text-center mr-2"></i>
                            Manage Roles
                        </a>

                        <a @click="show('roles')" class="dropdown-item">
                            <i class="fas fa-user-cog text-center mr-2"></i>
                            Update Roles
                        </a>

                        <div class="dropdown-divider"></div>
                        <a href="users/import" class="dropdown-item">
                            <i class="fas fa-file-import text-center mr-2"></i>
                            Import from CSV
                        </a>

                        <a @click="show('export')" class="dropdown-item">
                            <i class="fas fa-file-export text-center mr-2"></i>
                            Export to CSV
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- table section -->
        <div class="table-responsive">
            <table class="table table-dark table-hover bg-transparent">
                <thead>
                    <tr>
                        <th scope="col">
                            <input @change="select($event)" type="checkbox">
                        </th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-if="users.length === 0">
                        <td v-if="search.length > 0" colspan="6" class="text-center">No records found for "{{ search }}".</td>
                        <td v-else colspan="6" class="text-center">No users found in database.</td>
                    </tr>
                    <tr v-else v-for="user in users" @click="select($event)">
                        <td><input v-model="selected" :value="user.id" type="checkbox"></td>
                        <td>{{ user.name }}</td>
                        <td class="text-capitalize">{{ user.gender }}</td>
                        <td>{{ user.email }}</td>
                        <td class="text-capitalize">{{ user.role }}</td>
                        <td>
                            <span class="badge text-uppercase px-3 py-1" :class="{ 'badge-success': user.is_active == 1, 'badge-danger': user.is_active != 1 }">
                                <div v-if="user.is_active == 1">enabled</div>
                                <div v-else>disabled</div>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- pagination -->
            <app-pagination v-if="pageinfo" :current="pageinfo.current" :margin="2" :total="pageinfo.maxpage" @click="navigate($event)"></app-pagination>
        </div>

        <!-- modal section -->
        <div v-if="isVisible" class="action-modal h-100 w-100">
            <div class="container h-100">
                <?php $this->load->view($theme.'/admin/settings/users/delete'); ?>
                <?php $this->load->view($theme.'/admin/settings/users/detail'); ?>
                <?php $this->load->view($theme.'/admin/settings/users/role'); ?>
                <?php $this->load->view($theme.'/admin/settings/users/export'); ?>
            </div>
        </div>

    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/settings/users.js'); ?>"></script>