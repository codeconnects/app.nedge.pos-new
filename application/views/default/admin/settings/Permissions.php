<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

            <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">

            </div>
        </div>

        <div class="row px-3 mb-3">

        </div>

    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/settings/permissions.js'); ?>"></script>