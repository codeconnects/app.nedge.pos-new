<div v-if="modal === 'detail'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-search border-right border-white px-2 mr-2"></i>
                    View Detail
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body py-0 mh-100">
                <div v-if="selected.length > 1" class="row align-items-center border-bottom border-secondary pl-3 py-3">
                    <div class="col-md-6">
                        Currently Viewing <strong class="lead text-warning pl-2">{{ current.view +1 }}</strong>
                        / <strong class="font-weight-light text-warning">{{ selected.length }}</strong>
                    </div>

                    <div class="col-md-6 text-right">
                        <div class="btn-group">
                            <button @click="prev()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-left"></i>
                            </button>
                            <button @click="next()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="row p-3 border-bottom border-secondary">
                    <div class="row col-md-6 align-items-center">
                        <div class="col-md-12">
                            <h3 class="font-weight-light m-0">Information</h3>
                            <p class="font-weight-light m-0">User Personal Information</p>
                        </div>
                    </div>

                    <div class="col-md-6 text-right">
                        <img src="https://res.cloudinary.com/teepublic/image/private/s--sc913Ajc--/t_Preview/b_rgb:ffffff,c_limit,f_jpg,h_630,q_90,w_630/v1494292560/production/designs/1585650_1.jpg" class="bg-warning" style="
                            width: 100px;
                            height: 100px;
                            border-radius: 100%;
                        "></img>
                    </div>
                </div>

                <div class="row p-3">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Full Name</label>
                        <p class="text-capitalize">{{ vuser.name }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Gender</label>
                        <p class="text-capitalize">{{ vuser.gender }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Role</label>
                        <p class="text-capitalize">{{ vuser.role }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Birthdate</label>
                        <p class="text-capitalize">{{ vuser.bday }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Email</label>
                        <p class="text-lowercase">{{ vuser.email }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Mobile</label>
                        <p>{{ vuser.mobile }}</p>
                    </div>

                    <!-- separator -->
                    <div class="col-md-12 py-3"></div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Created Date</label>
                        <p class="text-capitalize">{{ vuser.created_dt }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Created By</label>
                        <p class="text-capitalize">{{ vuser.created_by }}</p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>