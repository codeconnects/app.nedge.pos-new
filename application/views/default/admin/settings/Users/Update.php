<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

            <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">

            </div>
        </div>

        <form @submit.prevent="update()">
        <div class="card bg-dark mb-3">
            <h5 class="card-header font-weight-light">
                <i class="fas fa-user mr-2"></i>
                User Information
            </h5>

            <div class="card-body row rounded-bottom px-3 mx-0">
                <div class="form-group col-md-4">
                    <label>First Name</label>
                    <input v-model="user.firstname" type="text" class="form-control" placeholder="Given name" required>
                </div>

                <div class="form-group col-md-4">
                    <label>Last Name</label>
                    <input v-model="user.lastname" type="text" class="form-control" placeholder="Family name" required>
                </div>

                <div class="form-group col-md-4">
                    <label>Gender</label>
                    <select v-model="user.gender" class="custom-select">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label>Birthdate</label>
                    <input v-model="user.bday" type="date" class="form-control" required>
                </div>

                <div class="form-group col-md-4">
                    <label>Role</label>
                    <select v-model="user.role" class="custom-select text-capitalize">
                        <option v-for="role in roles" :value="role.id" class="text-capitalize">{{ role.name }}</option>
                    </select>
                </div>

                <div class="form-group col-md-8">
                    <label>Email</label>
                    <input v-model="user.email" type="email" class="form-control" placeholder="Email address" required>
                    <small class="form-text text-muted">You will be using this email address to login</small>
                </div>

                <div class="form-group col-md-4">
                    <label>Mobile</label>
                    <input v-model="user.mobile" type="text" class="form-control" placeholder="Phone number" required>
                </div>

                <div class="form-group col-md-4">
                    <label>Password</label>
                    <input v-model="user.pass" type="password" class="form-control" placeholder="Password">
                </div>

                <div class="form-group col-md-4">
                    <label>Retype Password</label>
                    <input v-model="user.repass" type="password" class="form-control" placeholder="Confirm">
                </div>

                <hr class="w-100 mt-0">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-success px-4" :disabled="isProcess">
                        <i v-if="!isProcess" class="fas fa-user-check mr-2"></i>
                        <i v-else class="fas fa-spinner mr-2"></i>
                        Update User
                    </button>
                </div>
            </div>
        </div>
        </form>

    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/settings/users/update.js'); ?>"></script>