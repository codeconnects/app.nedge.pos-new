<div v-if="modal === 'export'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-file-export border-right border-white px-2 pr-3 mr-2"></i>
                    Export to CSV
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">Limit of export data depends on your pagination configuration: <strong class="text-warning">paginated</strong> - depends on max rows set, <strong class="text-warning">infinite</strong> - depends on loaded data selected.</p>
                </div>

                <div class="table-responsive border-bottom border-secondary">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="user in selectedList">
                                <td>{{ user.name }}</td>
                                <td class="text-capitalize">{{ user.gender }}</td>
                                <td>{{ user.email }}</td>
                                <td class="text-capitalize">{{ user.role }}</td>
                                <td class="p-1">
                                    <button @click="restore(user.id)" type="button" class="btn btn-primary" :disabled="isProcess">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="d-flex pt-3">
                    <button @click="startExport()" type="button" class="btn btn-primary" :disabled="isProcess">
                        <i v-if="!isProcess" class="fas fa-download mr-2"></i>
                        <i v-else class="fas fa-spinner mr-2"></i>
                        Start Exporting
                    </button>
                    <div v-if="isProcess" class="d-inline-block px-3 py-2">
                        Exporting {{ current.process }} out of {{ selectedList.length }}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>