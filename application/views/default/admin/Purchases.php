<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

             <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle px-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ warehouse.name }}
                    </button>
                    <div class="dropdown-menu">
                        <div v-for="warehouse in warehouses">
                            <a @click="selectWH(warehouse.id)" class="dropdown-item">{{ warehouse.name }}</a>
                            <div v-if="warehouse.id == 1" class="dropdown-divider"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex mb-3">
            <!-- search section -->
            <div class="col px-md-0">
                <div class="input-group">
                    <input v-model="search" v-on:input="populate()" type="text" class="form-control" placeholder="Search purchases">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" disabled>
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- action section -->
            <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                <div class="btn-group" role="group">
                    <button onclick="window.location.href = 'purchases/create'" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Create Purchase">
                        <i class="fas fa-plus"></i>
                    </button>
                    <button @click="gotoUpdate()" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Update Purchase">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button @click="show('receive')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Receive Purchase">
                        <i class="fas fa-box-open"></i>
                    </button>
                    <button @click="show('detail')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Detail">
                        <i class="fas fa-search"></i>
                    </button>
                </div>

                <div class="dropdown ml-md-2">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="-138,0">
                        <i class="fas fa-cog"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a @click="show('payment')" class="dropdown-item">
                            <i class="fas fa-dollar-sign text-center mr-2"></i>
                            Payments
                        </a>
                        <a  @click="show('return')" class="dropdown-item">
                            <i class="fas fa-redo text-center mr-2"></i>
                            Return Purchase
                        </a>
                        <a @click="show('print')" class="dropdown-item">
                            <i class="fas fa-tasks text-center mr-2"></i>
                            <!-- <i class="fas fa-barcode text-center mr-2"></i> -->
                            Print Purchases
                        </a>
                        <div class="dropdown-divider"></div>
                        <a onclick="window.location.href = 'purchases/import'" class="dropdown-item">
                            <i class="fas fa-file-import text-center mr-2"></i>
                            Import from CSV
                        </a>
                        <a @click="show('export')" class="dropdown-item">
                            <i class="fas fa-file-export text-center mr-2"></i>
                            Export to CSV
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- table section -->
        <div class="table-responsive">
            <table class="table table-dark table-hover bg-transparent">
                <thead>
                    <tr>
                        <th scope="col">
                            <input @change="select($event)" type="checkbox">
                        </th>
                        <th scope="col">Reference</th>
                        <th scope="col">Date Time</th>
                        <th scope="col">Total Cost</th>
                        <th scope="col">Supplier</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-if="purchases.length === 0">
                        <td v-if="search.length > 0" colspan="6" class="text-center">No records found for "{{ search }}".</td>
                        <td v-else colspan="6" class="text-center">No purchase records found in database.</td>
                    </tr>
                    <tr v-else v-for="purchase in purchases" @click="select($event)">
                        <td><input v-model="selected" :value="purchase.id" type="checkbox"></td>
                        <td>{{ purchase.reference }}</td>
                        <td>{{ purchase.datetime }}</td>
                        <td>{{ settings.symbol }} {{ purchase.total }}</td>
                        <td>{{ purchase.supplier }}</td>
                        <td>
                            <span class="badge w-100 py-1" :class="{ 'badge-info': purchase.status === 'pending', 'badge-success': purchase.status === 'received', 'badge-danger': purchase.status === 'returned' }">
                                <div class="text-uppercase">{{ purchase.status }}</div>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- pagination -->
            <app-pagination v-if="pageinfo" :current="pageinfo.current" :margin="2" :total="pageinfo.maxpage" @click="navigate($event)"></app-pagination>
        </div>

        <!-- modal section -->
        <div v-if="isVisible" class="action-modal h-100 w-100">
            <div class="container h-100">
                <?php $this->load->view($theme.'/admin/purchases/receive'); ?>
                <?php $this->load->view($theme.'/admin/purchases/detail'); ?>
                <?php $this->load->view($theme.'/admin/purchases/delete'); ?>
                <?php $this->load->view($theme.'/admin/purchases/payment'); ?>
                <?php $this->load->view($theme.'/admin/purchases/print'); ?>
                <?php $this->load->view($theme.'/admin/purchases/export'); ?>
            </div>
        </div>
    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/purchases.js'); ?>"></script>