<div v-if="modal === 'suspended'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-redo border-right border-white px-2 pr-3 mr-2"></i>
                    Suspended Sales
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">You can load and process these orders by clicking the button at the last column of each row. Canceling the order will just <strong class="text-primary">clear the fields</strong> but not <strong class="text-warning">delete</strong> the suspended order.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Reference</th>
                                <th scope="col">Date Time</th>
                                <th scope="col">Customer</th>
                                <th scope="col">Cart</th>
                                <th scope="col">Total</th>
                                <th scope="col">Operator</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="suspends.length == 0">
                                <td colspan="7" class="text-center">No suspended sales to load.</td>
                            </tr>
                            <tr v-else v-for="order in suspends">
                                <td>{{ order.reference }}</td>
                                <td>{{ order.datetime }}</td>
                                <td>{{ order.customer_name }}</td>
                                <td>{{ order.cart }}</td>
                                <td>{{ settings.symbol }} {{ order.total }}</td>
                                <td>{{ order.operator_name }}</td>
                                <td class="p-1">
                                    <button @click="load(order.id)" type="button" class="btn btn-primary" :disabled="isProcess">
                                        <i class="fas fa-angle-right"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>