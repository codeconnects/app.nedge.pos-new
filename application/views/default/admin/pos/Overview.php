<div v-if="modal === 'overview'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-5 h-75">
        
        <div class="card bg-dark w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-exclamation-circle border-right border-white px-2 pr-3 mr-2"></i>
                    Order Summary
                </div>

                <i @click="cancel()" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>

            <div class="card-body">
                <div class="">
                    <div class="row py-1">
                        <label class="font-weight-light col-md-5 text-md-right m-0">Customer</label>
                        <p class="font-weight-light col m-0">{{ customer.name }}</p>
                    </div>

                    <div class="row py-1 pb-3">
                        <label class="font-weight-light col-md-5 text-md-right m-0">Method</label>
                        <p class="font-weight-light col m-0 text-capitalize">{{ method }}</p>
                    </div>

                    <div class="row py-1">
                        <label class="font-weight-light col-md-5 text-md-right m-0">Sub Total</label>
                        <p class="font-weight-light col m-0">{{ settings.symbol }} {{ stotal }}</p>
                    </div>

                    <div class="row py-1">
                        <label class="font-weight-light col-md-5 text-md-right m-0">Discount</label>
                        <p class="font-weight-light col m-0">{{ discount }} ( Percentage )</p>
                    </div>

                    <div class="row py-1">
                        <label class="font-weight-light col-md-5 text-md-right m-0">Amount Due</label>
                        <p class="font-weight-light col m-0">{{ settings.symbol }} {{ atotal }}</p>
                    </div>

                    <div class="row py-1">
                        <label class="font-weight-light col-md-5 text-md-right m-0">Paid</label>
                        <p class="font-weight-light col m-0">{{ settings.symbol }} {{ paid }}</p>
                    </div>

                    <hr class="w-100">
                    <div class="row">
                        <h2 class="col-md-12">
                            <p class="lead col-md-5 d-inline-block text-md-right mb-0">Change</p>
                            {{ settings.symbol }} {{ change }}
                        </h2>
                    </div>
                </div>
            </div>

            <div class="card-footer text-right">
                <button @click="cancel()" type="button" class="btn btn-success">Ok! Got it</button>
            </div>
        </div>

    </div>
</div>