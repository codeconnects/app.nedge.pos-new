<div v-if="modal === 'giftcard'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-5 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-gift border-right border-white px-2 pr-3 mr-2"></i>
                    Gift Card
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>

            <!-- apply gift card: information -->
            <div v-if="!gcCreate" class="card-body mh-100">
                <div class="input-group">
                    <input type="text" placeholder="Enter 'Gift Card' code here" class="form-control">
                    <div class="input-group-append">
                        <button @click="gcCreate = true" type="button" class="btn btn-secondary">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <hr class="col-md-12 mx-0 px-0">
                <div>
                    <div class="border-bottom border-secondary px-3">
                        <p class="lead m-0">Information</p>
                        <p class="font-weight-light">Gift Card information and details</p>
                    </div>

                    <div class="row mx-0 px-0">
                        <div class="col-md-12 mt-3">
                            <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Gift Card Code</label>
                            <p>11000224200</p>
                        </div>

                        <div class="col-md-12">
                            <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Gift Card Holder</label>
                            <p>Walk In Customer</p>
                        </div>

                        <div class="col-md-6">
                            <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Method</label>
                            <p>Discount / Cash</p>
                        </div>

                        <div class="col-md-6">
                            <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Value</label>
                            <p>PHP 1,500.00</p>
                        </div>

                        <div class="col-md-12">
                            <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Expiration Date</label>
                            <p>Nov 19 2020 09:52 PM</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- create giftcard: information -->
            <div v-if="gcCreate" class="card-body mh-100">
                <div class="row mx-0 px-0">
                    <div class="form-group col-md-12">
                        <label>Card Code</label>
                        <div class="input-group">
                            <input type="text" placeholder="Enter reference" required="required" class="form-control">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-secondary">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Customer Name</label>
                        <input type="text" class="form-control" placeholder="Enter customer name">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Method</label>
                        <select class="custom-select">
                            <option value="cash">Cash</option>
                            <option value="discount">Discount</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Value</label>
                        <input type="number" class="form-control" placeholder="Enter value" min=1>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Expiration</label>
                        <input type="datetime-local" class="form-control">
                    </div>
                </div>
            </div>

            <div class="card-footer text-right">
                <button v-if="!gcCreate" type="button" class="btn btn-success">Use Gift Card</button>

                <button v-if="gcCreate" type="button" class="btn btn-success">Create Gift Card</button>
                <button @click="gcCreate = false" v-if="gcCreate" type="button" class="btn btn-danger">
                    <i class="fas fa-ban"></i>
                </button>
            </div>
        </div>

    </div>
</div>