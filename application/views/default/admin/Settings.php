<!-- page specific style -->
<style>
    .card a i { transition: margin .2s; }
    .card a:hover i { margin-left: 10px !important; }
</style>

<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

            <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">

            </div>
        </div>

        <div class="row px-3">
            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-sitemap border-right border-secondary pr-3 mr-2"></i>
                        System
                    </p>

                    <div class="card-body rounded-bottom">
                        Set or update site information and processing preferences. 
                    </div>

                    <a href="settings/system" class="btn btn-primary text-white rounded-0 py-2">
                        Go to Settings
                        <i class="fas fa-arrow-right ml-1"></i>
                    </a>
                </div>
            </span>

            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-box-open border-right border-secondary pr-3 mr-2"></i>
                        POS
                    </p>

                    <div class="card-body rounded-bottom">
                        Set or update preferences for point of sales.
                    </div>

                    <a href="settings/pos" class="btn btn-primary text-white rounded-0 py-2">
                        Go to Settings
                        <i class="fas fa-arrow-right ml-1"></i>
                    </a>
                </div>
            </span>

            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-users-cog border-right border-secondary pr-3 mr-2"></i>
                        Users
                    </p>

                    <div class="card-body rounded-bottom">
                        Manage accounts of users and set or update roles.
                    </div>

                    <a href="settings/users" class="btn btn-primary text-white rounded-0 py-2">
                        Go to Settings
                        <i class="fas fa-arrow-right ml-1"></i>
                    </a>
                </div>
            </span>

            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-user-shield border-right border-secondary pr-3 mr-2"></i>
                        Permissions
                    </p>

                    <div class="card-body rounded-bottom">
                        Manage user roles and set or update permissions.
                    </div>

                    <a href="settings/permissions" class="btn btn-primary text-white rounded-0 py-2">
                        Go to Settings
                        <i class="fas fa-arrow-right ml-1"></i>
                    </a>
                </div>
            </span>
        </div>

    </div>
    <!-- end: page content -->
</div>