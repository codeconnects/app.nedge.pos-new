<div class="container pb-5">
    <div class="row">
        <!-- sidebar section -->
        <?php $this->load->view('default/extension/sidebar'); ?>

        <!-- page content -->
        <div class="page-content col">
            <!-- page header -->
            <div class="page-header d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
                <!-- dashboard / breadcrumb -->
                <?php $this->load->view('default/extension/dashboard', array('page_name' => array('dashboard' => 'Profile Info'))); ?>

                <!-- widget section -->
                <div id="appController" class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">
                    
                </div>
            </div>

            <!-- page body -->
            <div class="page-body">
                
            </div> <!-- page body end -->

        </div> <!-- page content end -->
    </div> <!-- row end -->
</div> <!-- container end -->