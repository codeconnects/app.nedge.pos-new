<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view('default/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>
        </div>

        <form @submit.prevent="create()">
            <!-- information section -->
            <div class="card bg-dark mb-3">
                <h5 class="card-header font-weight-light">
                    <i class="fas fa-box mr-2"></i>
                    Product Information
                </h5>

                <div class="card-body row rounded-bottom mx-0">
                    <!-- start: information fields -->
                    <div class="form-group col-md-4">
                        <label>Barcode</label>
                        <div class="input-group mb-3">
                            <input v-model="information.code" type="text" class="form-control" placeholder="Barcode" required>
                            <div class="input-group-append">
                                <button @click="generate()" class="btn btn-secondary" type="button" data-toggle="tooltip" data-placement="top" title="Generate Barcode">
                                    <i class="fas fa-barcode"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label>HSN Code</label>
                        <input v-model="information.code_hsn" type="text" class="form-control" placeholder="HSN Code">
                        <small class="form-text text-muted">International commodity code</small>
                    </div>

                    <div class="form-group col-md-8">
                        <label>Name</label>
                        <input v-model="information.name" @input="slug()" type="text" class="form-control" placeholder="Product Name" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Slug</label>
                        <input v-model="information.slug" type="text" class="form-control" placeholder="Slug" readonly>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Display Name</label>
                        <input v-model="information.name_display" type="text" class="form-control" placeholder="Display Name" required>
                        <small class="form-text text-muted">When displayed in tables</small>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Category</label>
                        <select v-model="information.category" class="custom-select">
                            <option v-for="category in dependencies.categories" :value="category.id">{{ category.name }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Sub Category</label>
                        <select v-model="information.category_sub" class="custom-select">
                            <option v-for="category in dependencies.subcategories" :value="category.id">{{ category.name }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Brand</label>
                        <select v-model="information.brand" class="custom-select">
                            <option v-for="brand in dependencies.brands" :value="brand.id">{{ brand.name }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Unit</label>
                        <select v-model="information.unit" class="custom-select">
                            <option v-for="unit in dependencies.units" :value="unit.id">{{ unit.name }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>CB: Parent</label>
                        <input v-model="parent" @input="search()" @keyup.escape="reset()" type="text" class="form-control" placeholder="None">
                        <div v-if="isSearch && parent.length > 0" class="autocomplete bg-dark rounded-bottom shadow w-100 py-3">
                            <span v-if="products.length === 0" class="d-block text-center px-3">
                                No records found for "{{ parent }}".
                            </span>
                            <span v-else v-for="product in products" @click="select(product)" class="items row align-items-center w-100 m-0 px-3 py-1">
                                <p class="col-md-auto text-md-center m-0">{{ product.code }}</p>
                                <p class="col font-weight-light border-left border-secondary m-0">{{ product.name_display }}</p>
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label>CB: Value</label>
                        <input v-model="information.cb_quantity" type="number" class="form-control" placeholder="Unit Quantity" min=1 required>
                        <small class="form-text text-muted">Break down quantity</small>
                    </div>

                    <!-- image and invoice section -->
                    <hr class="w-100 mx-3">
                    <div class="form-group col-md-8">
                        <label>Image</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input @input="information.image = $event.target.value" type="file" class="custom-file-input" accept="image/*">
                                <label class="custom-file-label text-secondary">{{ image }}</label>
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button">
                                    <i class="fas fa-folder"></i>
                                </button>
                            </div>
                        </div>
                        <textarea v-model="information.description" class="form-control" placeholder="Product description"></textarea>
                    </div>
                    <!-- end: information fields -->
                </div>
            </div>

            <div class="card bg-dark">
                <h5 class="card-header font-weight-light">
                    <i class="fas fa-cogs mr-2"></i>
                    Product Preference
                </h5>

                <div class="card-body row rounded-bottom mx-0">
                    <!-- start: preference fields -->
                    <div class="form-group col-md-4">
                        <label>Product Type</label>
                        <select v-model="information.product_type" class="custom-select">
                            <option value="standard">Standard</option>
                            <option value="combo">Combo</option>
                            <option value="digital">Digital</option>
                            <option value="service">Service</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Barcode Type</label>
                        <select v-model="information.barcode_type" class="custom-select">
                            <option value="code128">Code128</option>
                            <option value="ean13">EAN13</option>
                            <option value="upc-a">UPC-A</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Quantity Tracking</label>
                        <select v-model="information.is_tracked" class="custom-select">
                            <option value="0">Do Not Track</option>
                            <option value="1">Track Quantity</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Alert Trigger</label>
                        <input v-model="information.trigger" type="number" class="form-control" placeholder="Trigger Quantity" min=20 required>
                    </div>

                    <hr class="w-100 mx-3">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success" :disabled="isProcess">
                            <i v-if="!isProcess" class="fas fa-check mr-2"></i>
                            <i v-else class="fas fa-spinner mr-2"></i>
                            Create Product
                        </button>
                    </div>
                    <!-- end: preference fiels -->
                </div>
            </div>
        </form>

    </div>
    <!-- end: page content -->
</div> 

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/products/create.js') ?>"></script>