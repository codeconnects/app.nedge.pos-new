<div class="container pb-5">
    <div class="row">
        <!-- sidebar section -->
        <?php $this->load->view('default/extension/sidebar'); ?>

        <!-- page content -->
        <div class="page-content col">
            <!-- page header -->
            <div class="page-header d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
                <!-- dashboard / breadcrumb -->
                <?php $this->load->view('default/extension/dashboard'); ?>
            </div>

            <!-- page body -->
            <div class="page-body">
                <!-- page controls -->
                <div id="appActions" class="d-flex mb-3">
                    <!-- searchbox -->
                    <div class="col px-md-0">
                        <div class="input-group">
                            <input v-model="searchWord" @keyup.enter="searchProduct()" type="text" class="form-control" placeholder="Search products">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- action buttons -->
                    <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                        <div class="btn-group" role="group">
                            <button v-if="isCompleted" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top">
                                <i class="fas fa-check"></i>
                            </button>
                            
                            <button @click="importDemo()" type="button" class="btn btn-outline-primary" :class="{ disabled: isProcessing }" :disabled="isProcessing" data-toggle="tooltip" data-placement="top" title="Import Product">
                                <i v-if="!isProcessing" class="fas fa-file-import"></i>
                                <i v-if="isProcessing" class="fas fa-spinner"></i>
                            </button>
                            <button @click="showModal('print')" type="button" class="btn btn-outline-primary" :class="{ disabled: isProcessing }" :disabled="isProcessing" data-toggle="tooltip" data-placement="top" title="Print Barcode">
                                <i class="fas fa-barcode"></i>
                            </button>
                            <button @click="showModal('delete')" type="button" class="btn btn-outline-primary" :class="{ disabled: isProcessing }" :disabled="isProcessing" data-toggle="tooltip" data-placement="top" title="Delete Product">
                                <i class="fas fa-trash"></i>
                            </button>
                            <button @click="showModal('detail')" type="button" class="btn btn-outline-primary" :class="{ disabled: isProcessing }" :disabled="isProcessing" data-toggle="tooltip" data-placement="top" title="View Detail">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- page table -->
                <div id="appTable" class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <input @change="setSelected($event)" type="checkbox">
                                </th>
                                <th scope="col">Code</th>
                                <th scope="col">Name</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Category</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="products.length > 0" v-for="product in products" @click="setSelected($event)">
                                <td><input v-model="selectedProducts" :value="product.id" type="checkbox"></td>
                                <td>{{ product.code }}</td>
                                <td>{{ product.name_display }}</td>
                                <td>{{ product.brand }}</td>
                                <td>{{ product.category }}</td>
                                <td>
                                    <span class="badge px-3 py-1" :class="{ 'badge-success': product.is_hidden === 0, 'badge-danger': product.is_hidden === 1 }">
                                        <div v-if="product.is_hidden === 0">ACTIVE</div>
                                        <div v-else>INACTIVE</div>
                                    </span>
                                </td>
                            </tr>
                            <tr v-if="products.length === 0">
                                <td class="text-center" colspan="6">No products to show for now.</td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- pagination -->
                    <pagination :infinite="settings.infinite" :current="settings.current" :margin="2" :total="settings.total" @click="pageNavigate($event)"></pagination>
                </div>

            </div> <!-- page body end -->

            <!-- modal windows -->
            <div id="appModal" v-if="isVisible === true" class="action-modal h-100 w-100">
                <div class="container h-100">
                    <?php $this->load->view('default/admin/products/delete'); ?>
                    <?php $this->load->view('default/admin/products/detail'); ?>
                    <?php $this->load->view('default/admin/products/print'); ?>
                </div>
            </div>

        </div> <!-- page content end -->
    </div> <!-- row end -->
</div> <!-- container end -->

<!-- page javascript -->
<script src="<?= base_url('assets/js/default/admin/products/import.js') ?>"></script>