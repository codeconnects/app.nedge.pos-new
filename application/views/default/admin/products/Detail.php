<div v-if="modal === 'detail'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-search border-right border-white px-2 mr-2"></i>
                    View Detail
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div v-if="selected.length > 1" class="d-md-flex no-gutters align-items-center w-100 border-bottom border-secondary pb-3 mb-3">
                    <div class="col-md-6">
                        Currently Viewing <strong class="lead text-warning pl-2">{{ current.view +1 }}</strong>
                        / <strong class="font-weight-light text-warning">{{ selected.length }}</strong>
                    </div>

                    <div class="col-md-6 text-right">
                        <div class="btn-group">
                            <button @click="prev()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-left"></i>
                            </button>
                            <button @click="next()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <h3 class="font-weight-light mb-1">Information</h3>
                <p class="font-weight-light">Product base information</p>
                <hr class="my-3">
                <div class="row">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Barcode</label>
                        <p>{{ vproduct.code }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">HSN Code</label>
                        <p>{{ vproduct.code_hsn }}</p>
                    </div>

                    <div class="col-md-8">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Name</label>
                        <p>{{ vproduct.name }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Slug</label>
                        <p>{{ vproduct.slug }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Display Name</label>
                        <p>{{ vproduct.name_display }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Category</label>
                        <p>{{ vproduct.category }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Sub Category</label>
                        <p>{{ vproduct.category_sub }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Brand</label>
                        <p>{{ vproduct.brand }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Unit</label>
                        <p>{{ vproduct.unit }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">CB: Parent</label>
                        <p v-if="vproduct.cb_parent !== 0">{{ vproduct.cb_parent }}</p>
                        <p v-else>—</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">CB: Value</label>
                        <p v-if="vproduct.cb_quantity !== 0">{{ vproduct.cb_quantity }}</p>
                        <p v-else>—</p>
                    </div>

                    <div class="col-md-12"></div>
                    <div class="col-md-8">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Image</label>
                        <p>{{ vproduct.image }}</p>
                    </div>

                    <div class="col-md-8">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Description</label>
                        <p>{{ vproduct.description }}</p>
                    </div>
                </div>

                <h3 class="font-weight-light mt-5 mb-1">Preferences</h3>
                <p class="font-weight-light">Product preferences and behavior</p>
                <hr class="my-3">
                <div class="row">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Product Type</label>
                        <p class="text-capitalize">{{ vproduct.product_type }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Barcode Type</label>
                        <p class="text-uppercase">{{ vproduct.barcode_type }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Track Quantity</label>
                        <p v-if="vproduct.is_tracked == 1">Tracked</p>
                        <p v-else>Not Tracked</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Alert Trigger</label>
                        <p>{{ vproduct.trigger }}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>