<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

             <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle px-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ warehouse.name }}
                    </button>
                    <div class="dropdown-menu">
                        <div v-for="warehouse in warehouses">
                            <a @click="selectWH(warehouse.id)" class="dropdown-item">{{ warehouse.name }}</a>
                            <div v-if="warehouse.id == 1" class="dropdown-divider"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex mb-3">
            <!-- search section -->
            <div class="col px-md-0">
                <div class="input-group">
                    <input v-model="search" v-on:input="populate()" type="text" class="form-control" placeholder="Search stocks">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" disabled>
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- action section -->
            <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                <div class="btn-group" role="group">
                    <button onclick="window.location.href = 'purchases/create'" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Add Purchase">
                        <i class="fas fa-plus"></i>
                    </button>
                    <button @click="gotoUpdate()" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Update Product">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button @click="show('delete')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Expire Stock">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button @click="show('detail')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Detail">
                        <i class="fas fa-search"></i>
                    </button>
                </div>

                <div class="dropdown ml-md-2">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="-138,0">
                        <i class="fas fa-cog"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a @click="show('adjust')" class="dropdown-item">
                            <i class="fas fa-sync-alt text-center mr-2"></i>
                            Adjust Quantity
                        </a>
                        <a @click="show('price')" class="dropdown-item">
                            <i class="fas fa-dollar-sign text-center mr-2"></i>
                            Update Price
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="<?= base_url('admin/stocks/expire'); ?>" class="dropdown-item">
                            <i class="fas fa-history text-center mr-2"></i>
                            Expired Stocks
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- table section -->
        <div class="table-responsive">
            <table class="table table-dark table-hover bg-transparent">
                <thead>
                    <tr>
                        <th scope="col">
                            <input @change="select($event)" type="checkbox">
                        </th>
                        <th scope="col">Reference</th>
                        <th scope="col">Name</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Unit</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-if="stocks.length === 0">
                        <td v-if="search.length > 0" colspan="8" class="text-center">No records found for "{{ search }}".</td>
                        <td v-else colspan="8" class="text-center">No records found in database.</td>
                    </tr>
                    <tr v-else v-for="stock in stocks" @click="select($event)">
                        <td><input v-model="selected" :value="stock.id" type="checkbox"></td>
                        <td>{{ stock.reference }}</td>
                        <td>{{ stock.name_display }}</td>
                        <td>{{ stock.brand }}</td>
                        <td>{{ stock.category }}</td>
                        <td>{{ settings.symbol }} {{ stock.price }}</td>
                        <td>{{ stock.quantity }}</td>
                        <td>{{ stock.unit }}</td>
                    </tr>
                </tbody>
            </table>

            <!-- pagination -->
            <app-pagination v-if="pageinfo" :current="pageinfo.current" :margin="2" :total="pageinfo.maxpage" @click="navigate($event)"></app-pagination>
        </div>

        <!-- modal section -->
        <div v-if="isVisible" class="action-modal h-100 w-100">
            <div class="container h-100">
                <?php $this->load->view($theme.'/admin/stocks/delete'); ?>
                <?php $this->load->view($theme.'/admin/stocks/detail'); ?>
                <?php $this->load->view($theme.'/admin/stocks/adjust'); ?>
                <?php $this->load->view($theme.'/admin/stocks/price'); ?>
            </div>
        </div>
    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/stocks.js'); ?>"></script>