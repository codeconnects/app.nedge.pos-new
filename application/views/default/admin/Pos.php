<style>
    .grid {
        background: transparent;
        border: none;
        cursor: pointer;
        overflow: hidden;
    }
    .grid > .item {
        background-size: 100% 100%;
        height: 108px;
    }
    .grid > .item > div {
        position: relative;
        top: 100%;
        
        transition: top .2s;
    }
    .grid:focus > .item > div,
    .grid:hover > .item > div { top: 0%; }
</style>

<div id="approot" class="row container mx-auto px-0 pb-5">
    <!-- start: page content [main] -->
    <div class="col px-4 mx-2">
        <div class="row border-bottom border-dark mx-0 mb-3 pt-2 pb-3">
            <?php
                $temp = array('page_name' => array('dashboard' => 'Point of Sale'));
                $this->load->view($theme.'/extension/dashboard', $temp);
            ?>

            <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 mx-0">
                
            </div>
        </div>
            
        <div class="row p-0 mx-0 mb-3">
            <div class="col-md-5 px-0 mb-2 mb-md-0">
                <div class="input-group">
                    <input v-model="keywordp" @input="searchp()" @keydown.escape="keywordp = ''" type="text" class="form-control" placeholder="Search products" :disabled="isProcess" data-toggle="tooltip" data-placement="top" title="Search or scan barcode">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" disabled>
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>

                <!-- autocomplete section -->
                <div v-if="keywordp.length > 0 && !grid" class="autocomplete bg-dark rounded-bottom shadow w-100 py-3" style="width: 240% !important;">
                    <span v-if="products.length == 0" class="d-block text-center px-3">
                        No records found for "{{ keywordp }}".
                    </span>
                    <span v-else v-for="product in products" @click="addToCart(product.id)" class="items row align-items-center w-100 m-0 px-3 py-1">
                        <p class="col-md-3 text-md-center m-0">{{ product.code }}</p>
                        <p class="col-md-3 font-weight-light border-left border-secondary m-0">{{ product.name_display }}</p>
                        <p class="col-md-2 font-weight-light border-left border-secondary m-0">{{ product.brand }}</p>
                        <p class="col-md-2 font-weight-light border-left border-secondary m-0">{{ product.category }}</p>
                        <p class="col-md-2 font-weight-light border-left border-secondary m-0">{{ settings.symbol }} {{ product.price }}</p>
                    </span>
                </div>
                <!-- end autocompete -->
            </div>

            <div class="col text-right px-0">
                <div class="btn-group" role="group">
                    <button @click="toggle()" type="button" class="btn btn-outline-primary"  data-toggle="tooltip" data-placement="top" title="Show Item Grid">
                        <i class="fas fa-th"></i>
                    </button>

                    <button @click="show('giftcard')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Gift Card" :disabled="isProcess">
                        <i class="fas fa-gift"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="">
            <!-- grid section -->
            <div v-if="grid" class="bg-yellow w-100 h-100" style="position: relative;">
                <div class="row mx-0 mb-3">
                    <button v-for="n in 28" class="grid col-md-2 p-1 pb-0 mb-1">
                        <div class="item" style="background-image: url('https://images-na.ssl-images-amazon.com/images/I/51K7YxVuk2L._SY355_.jpg');">
                            <div class="row align-items-center text-center h-100 mx-0" style="background-color: #1997c6;">
                                <p class="w-100 text-white text-center p-2 mb-0">
                                   [ Sample ] Rubiks Cube #{{ n }}
                                </p>
                            </div>
                        </div>

                        <!-- <div class="card">
                            <img src="http://www.klslinternational.com/storage/merchant_store_product/331/2235/5_1428585373_sample-img.png" class="card-img-top" alt="Sample Product">
                            <div class="card-body bg-secondary p-0">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div> -->

                    </button>
                </div>

                <!-- pagination -->
                <app-pagination :current="1" :margin="2" :total="5"></app-pagination>
            </div>

            <div class="table-responsive" style="overflow-y: auto; max-height: 358px;">
                <table class="table table-dark table-hover bg-transparent">
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Unit</th>
                            <th scope="col">Total</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-if="orders.length === 0">
                            <td colspan="7" class="text-center">No products added to cart.</td>
                        </tr>
                        <tr v-else v-for="product in orders">
                            <td>{{ product.code }}</td>
                            <td>{{ product.name_display }}</td>
                            <td>{{ settings.symbol }} {{ product.price }}</td>
                            <td class="py-1">
                                <input type="number" class="form-control" v-model="product.quantity" style="width: 80px;" min=1 :disabled="isProcess">
                            </td>
                            <td>{{ product.unit }}</td>
                            <td>{{ settings.symbol }} {{ total(product.quantity, product.price) }}</td>
                            <td class="py-1">
                                <button @click="removeFromCart(product.id)" type="button" class="btn btn-danger" :disabled="isProcess">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!-- end: page content [main] -->

    <!-- start: page content [info] -->
    <div class="col-md-4">
        <div class="sticky-top" style="top: 62px; z-index: 1;">
            <div class="card bg-dark">
                <div class="card-body rounded">
                    <div class="input-group">
                        <input v-model="keywordc" @input="searchc()" @keydown.escape="keywordc = ''" type="text" placeholder="Search customer" class="form-control" :disabled="isProcess">
                        <div class="input-group-append">
                            <button @click="show('customer')" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="New Customer" :disabled="isProcess">
                                <i class="fas fa-user-plus"></i>
                            </button>
                        </div>

                        <!-- autocomplete section -->
                        <div v-if="keywordc.length > 0" class="autocomplete bg-dark rounded-bottom shadow w-100 py-3">
                            <span v-if="customers.length == 0" class="d-block text-center px-3">
                                No records found for "{{ keywordc }}".
                            </span>
                            <span v-else v-for="customer in customers" @click="setCustomer(customer.id)" class="items row align-items-center w-100 m-0 px-3 py-1">
                                <p class="col-md-12 m-0">{{ customer.lastname }}, {{ customer.firstname }}</p>
                            </span>
                        </div>
                        <!-- end autocomplete -->
                    </div>
                </div>
            </div>

            <div class="card bg-dark mt-2">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item card-body">
                        <div class="row">
                            <label class="lead font-weight-normal col-md-5 text-md-right m-0">Cart Items</label>
                            <p class="lead font-weight-light col m-0">{{ cart.base }} ({{ cart.full }})</p>
                        </div>
                    </li>

                    <li class="list-group-item card-body py-2">
                        <div class="row">
                            <label class="font-weight-light col-md-5 text-md-right m-0">Customer</label>
                            <p class="font-weight-light col m-0">{{ customer.name }}</p>
                        </div>
                    </li>

                    <li class="list-group-item card-body py-2">
                        <div class="row">
                            <label class="font-weight-light col-md-5 text-md-right m-0">Discount</label>
                            <p class="font-weight-light col m-0">{{ discount }} ( Percentage )</p>
                        </div>
                    </li>

                    <li class="list-group-item card-body py-2">
                        <div class="row">
                            <label class="font-weight-light col-md-5 text-md-right m-0">Sub Total</label>
                            <p class="font-weight-light col m-0">{{ settings.symbol }} {{ stotal }}</p>
                        </div>
                    </li>

                    <li class="list-group-item card-body py-2">
                        <div class="row">
                            <label class="font-weight-light col-md-5 m-0 text-md-right">Amount Due</label>
                            <p class="font-weight-light col m-0">{{ settings.symbol }} {{ atotal }}</p>
                        </div>
                    </li>
                </ul>

                <div class="card-body rounded-bottom">
                    <div class="row p-0 m-0">
                        <label class="font-weight-light col-md-5 text-md-right m-0 py-2">Method</label>
                        <select v-model="method" class="custom-select col-md-6" :disabled="isProcess">
                            <option value="cash">Cash</option>
                            <option value="debit">Debit</option>
                            <!-- <option value="gift-card">Gift Card</option> -->
                        </select>
                    </div>

                    <div class="row p-0 m-0 mt-2">
                        <label class="font-weight-light col-md-5 text-md-right m-0 py-2">Paid</label>
                        <div class="input-group col-md-6 px-0">
                            <input v-model="paid" @keydown.enter="payment()" type="number" class="form-control" value=0 min=0 :disabled="isProcess">
                            <div class="input-group-append">
                                <button @click="payment()" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Process Payment" :disabled="isProcess">
                                    <i v-if="!isProcess" class="fas fa-dollar-sign px-1"></i>
                                    <i v-else class="fas fa-spinner"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card bg-dark mt-2">
                <div class="card-body rounded">

                    <div class="btn-group col-md-12 row mx-0 px-0" role="group">
                        <button @click="suspend()" type="button" class="btn btn-outline-warning col-md-6" :disabled="isProcess">
                            <i class="fas fa-pause mr-1"></i>
                            Suspend Sale
                        </button>

                        <button @click="show('suspended')" type="button" class="btn btn-outline-primary col-md-2" data-toggle="tooltip" data-placement="top" title="Suspended Sales" :disabled="isProcess">
                            <i class="fas fa-th-list"></i>
                        </button>

                        <button @click="setDiscount()" type="button" class="btn btn-outline-primary col-md-2" data-toggle="tooltip" data-placement="top" title="Discount" :disabled="isProcess">
                            <i class="fas fa-percent"></i>
                        </button>

                        <button @click="cancel()" type="button" class="btn btn-danger col-md-2" data-toggle="tooltip" data-placement="top" title="Cancel Sale" :disabled="isProcess">
                            <i class="fas fa-ban"></i>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end: page content [info] -->

    <!-- modal section -->
    <div v-if="isVisible" class="action-modal h-100 w-100">
        <div class="container h-100">
            <?php $this->load->view($theme.'/admin/pos/customer'); ?>
            <?php $this->load->view($theme.'/admin/pos/giftcard'); ?>
            <?php $this->load->view($theme.'/admin/pos/suspended'); ?>
            <?php $this->load->view($theme.'/admin/pos/overview'); ?>
        </div>
    </div>

</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/pos.js') ?>"></script>