<div v-if="modal === 'price'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-10 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-dollar-sign border-right border-white px-2 pr-3 mr-2"></i>
                    Update Price
                </div>

                <i @click="close()" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">Updating prices here will <strong class="text-warning">not affect the price of stocks on purchase</strong>. After saving, new prices will <strong class="text-primary">used by all process on the POS</strong> for the updated stocks. Updates will not be saved unless you save them.</p>
                </div>

                <div class="table-responsive border-bottom border-secondary">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Reference</th>
                                <th scope="col">Name</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="stock in selectedList">
                                <td>{{ stock.reference }}</td>
                                <td>{{ stock.name_display }}</td>
                                <td>{{ stock.brand }}</td>
                                <td>{{ stock.category }}</td>
                                <td class="py-1">
                                    <input v-model="stock.price" type="number" class="form-control" style="max-width: 100px;" min=0 :readonly="isProcess">
                                </td>
                                <td>{{ stock.quantity }}</td>
                                <td class="p-1">
                                    <button @click="restore(stock.id)" type="button" class="btn btn-primary" :disabled="isProcess">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pt-3">
                    <button @click="update()" type="button" class="btn btn-success" :disabled="isProcess">
                        <i v-if="!isProcess" class="fas fa-check mr-2"></i>
                        <i v-else class="fas fa-spinner mr-2"></i>
                        Update Prices
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>