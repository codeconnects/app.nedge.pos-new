<!-- <?php $this->load->view('default/extension/loading'); ?> -->
<div class="container pb-5">
    <div class="row">
        <!-- sidebar section -->
        <?php $this->load->view('default/extension/sidebar'); ?>

        <!-- page content -->
        <div class="page-content col">
            <!-- page header -->
            <div class="page-header d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
                <!-- dashboard / breadcrumb -->
                <?php $this->load->view('default/extension/dashboard'); ?>

                <!-- widget section -->
                <div id="appController" class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle px-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ selectedWarehouse.name }}
                        </button>
                        <div class="dropdown-menu">
                            <div v-for="warehouse in warehouses">
                                <a @click="setSelected(warehouse.id)" class="dropdown-item">{{ warehouse.name }}</a>
                                <div v-if="warehouse.id === 1" class="dropdown-divider"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- page body -->
            <div class="page-body">
                <!-- page controls -->
                <div id="appActions" class="row mx-0 mb-3">
                    <!-- searchbox -->
                    <div class="col-md-4 px-md-0">
                        <div class="input-group">
                            <input v-model="searchWord" @input="searchStock()" type="text" class="form-control" placeholder="Search stocks">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button" disabled>
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- action buttons -->
                    <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                        <div class="btn-group" role="group">
                            <button @click="showModal('restore')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Restore Stock">
                                <i class="fas fa-redo-alt"></i>
                            </button>
                            <button @click="showModal('detail')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Detail">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- page table -->
                <div id="appTable" class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <input @change="setSelected($event)" type="checkbox">
                                </th>
                                <th scope="col">Reference</th>
                                <th scope="col">Name</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Unit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="stocks.length === 0">
                                <td v-if="searchWord.length > 0" colspan="8" class="text-center">No records found for "{{ searchWord }}".</td>
                                <td v-else colspan="8" class="text-center">No stocks found to show.</td>
                            </tr>
                            <tr v-else v-for="stock in stocks" @click="setSelected($event)">
                                <td><input v-model="selectedStocks" :value="stock.id" type="checkbox"></td>
                                <td>{{ stock.reference }}</td>
                                <td>{{ stock.name_display }}</td>
                                <td>{{ stock.brand }}</td>
                                <td>{{ stock.category }}</td>
                                <td>{{ settings.accounting.currency_symbol }} {{ stock.price }}</td>
                                <td>{{ stock.quantity }}</td>
                                <td>{{ stock.unit }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- pagination -->
                    <pagination v-if="paginate" :current="paginate.current" :margin="2" :total="paginate.maxpage" @click="pageNavigate($event)"></pagination>
                </div>

            </div> <!-- page body end -->

            <!-- modal windows -->
            <div id="appModal" v-if="isVisible === true" class="action-modal h-100 w-100">
                <div class="container h-100">
                    <?php $this->load->view('default/admin/stocks/restore'); ?>
                    <?php $this->load->view('default/admin/stocks/detail'); ?>
                </div>
            </div>

        </div> <!-- page content end -->
    </div> <!-- row end -->
</div> <!-- container end -->

<!-- page javascript -->
<script src="<?= base_url('assets/js/default/admin/stocks/expire.js'); ?>"></script>