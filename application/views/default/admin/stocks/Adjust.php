<div v-if="modal === 'adjust'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-10 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-sync-alt border-right border-white px-2 pr-3 mr-2"></i>
                    Adjust Quantity
                </div>

                <i @click="close()" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">
                        Adjusting stock quantity will also <strong class="text-warning">adjust the purchase data</strong> and will re-calculate the purchase total cost. Adjustment history will be visible on a purchase if there's any quantity adjustment made to the stocks under that purchase.
                    </p>
                </div>

                <div class="table-responsive border-bottom border-secondary">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Reference</th>
                                <th scope="col">Name</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="stock in selectedList">
                                <td>{{ stock.reference }}</td>
                                <td>{{ stock.name_display }}</td>
                                <td>{{ stock.brand }}</td>
                                <td>{{ stock.category }}</td>
                                <td>
                                    {{ settings.symbol }} {{ stock.price }}
                                </td>
                                <td class="py-1">
                                    <input v-model="stock.quantity" type="number" class="form-control" style="max-width: 80px;" min=0 :readonly="isProcess">
                                </td>
                                <td class="p-1">
                                    <button @click="restore(stock.id)" type="button" class="btn btn-primary" :disabled="isProcess">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="text-center text-md-left pt-3">
                    <button @click="adjust()" type="button" class="btn btn-success" :disabled="isProcess">
                        <i v-if="!isProcess" class="fas fa-check mr-2"></i>
                        <i v-else class="fas fa-spinner mr-2"></i>
                        Adjust Quantities
                    </button>

                    <label class="btn m-0">
                        <input v-model="isConfirm" class="mr-1" type="checkbox"> I confirm the adjustment
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>