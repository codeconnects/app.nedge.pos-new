<div v-if="modal === 'detail'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-search border-right border-white px-2 mr-2"></i>
                    View Detail
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                
                <div v-if="selected.length > 1" class="d-md-flex no-gutters align-items-center w-100 border-bottom border-secondary pb-3 mb-3">
                    <div class="col-md-6">
                        Currently Viewing <strong class="lead text-warning pl-2">{{ current.view +1 }}</strong>
                        / <strong class="font-weight-light text-warning">{{ selected.length }}</strong>
                    </div>

                    <div class="col-md-6 text-right">
                        <div class="btn-group">
                            <button @click="prev()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-left"></i>
                            </button>
                            <button @click="next()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <h3 class="font-weight-light mb-1">Product Info</h3>
                <p class="font-weight-light">Information about this product</p>
                <hr class="my-3">
                <div class="row">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Barcode</label>
                        <p>{{ vstock.code }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Product Name</label>
                        <p>{{ vstock.name_display }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Category</label>
                        <p>{{ vstock.category }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Brand</label>
                        <p>{{ vstock.brand }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Unit</label>
                        <p>{{ vstock.unit }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Parent</label>
                        <p>{{ vstock.parent }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Price</label>
                        <p>{{ settings.symbol }} {{ vstock.price }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Quantity</label>
                        <p>{{ vstock.quantity }}</p>
                    </div>
                </div>

                <hr class="mt-5 mb-3">
                <h3 class="font-weight-light mb-1">Purchase Info</h3>
                <p class="font-weight-light">Information about purchase</p>
                <hr class="my-3">
                <div class="row">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Date Time</label>
                        <p>{{ vstock.datetime }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Reference Code</label>
                        <p>{{ vstock.reference }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Warehouse</label>
                        <p>{{ vstock.warehouse }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Supplier</label>
                        <p>{{ vstock.supplier }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Tax Method</label>
                        <p>{{ vstock.tax }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Discount</label>
                        <p>{{ vstock.discount }}</p>
                    </div>

                    <div class="col-md-8">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Purchase Note</label>
                        <p>{{ vstock.note }}</p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>