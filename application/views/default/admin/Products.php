<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>
        </div>

        <div class="d-flex mb-3">
            <!-- search section -->
            <div class="col px-md-0">
                <div class="input-group">
                    <input v-model="search" v-on:input="populate()" type="text" class="form-control" placeholder="Search products">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" disabled>
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- action section -->
            <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                <div class="btn-group" role="group">
                    <button onclick="window.location.href = 'products/create'" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Create Product">
                        <i class="fas fa-plus"></i>
                    </button>
                    <button @click="gotoUpdate()" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Update Product">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button @click="show('delete')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Delete Product">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button @click="show('detail')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Detail">
                        <i class="fas fa-search"></i>
                    </button>
                </div>

                <div class="dropdown ml-md-2">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="-138,0">
                        <i class="fas fa-cog"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a @click="show('print')" class="dropdown-item">
                            <i class="fas fa-barcode text-center mr-2"></i>
                            Print Barcode
                        </a>
                        <div class="dropdown-divider"></div>
                        <a onclick="window.location.href = 'products/import'" class="dropdown-item">
                            <i class="fas fa-file-import text-center mr-2"></i>
                            Import from CSV
                        </a>
                        <a @click="show('export')" class="dropdown-item">
                            <i class="fas fa-file-export text-center mr-2"></i>
                            Export to CSV
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- table section -->
        <div class="table-responsive">
            <table class="table table-dark table-hover bg-transparent">
                <thead>
                    <tr>
                        <th scope="col">
                            <input @change="select($event)" type="checkbox">
                        </th>
                        <th scope="col">Code</th>
                        <th scope="col">Name</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Category</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-if="products.length === 0">
                        <td v-if="search.length > 0" colspan="6" class="text-center">No records found for "{{ search }}".</td>
                        <td v-else colspan="6" class="text-center">No product records found in database.</td>
                    </tr>
                    <tr v-else v-for="product in products" @click="select($event)">
                        <td><input v-model="selected" :value="product.id" type="checkbox"></td>
                        <td>{{ product.code }}</td>
                        <td>{{ product.name_display }}</td>
                        <td>{{ product.brand }}</td>
                        <td>{{ product.category }}</td>
                        <td>
                            <span class="badge w-75 py-1" :class="{ 'badge-success': product.is_hidden == 0, 'badge-danger': product.is_hidden != 0 }">
                                <div v-if="product.is_hidden == 0">ENABLED</div>
                                <div v-else>DISABLED</div>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- pagination -->
            <app-pagination v-if="pageinfo" :current="pageinfo.current" :margin="2" :total="pageinfo.maxpage" @click="navigate($event)"></app-pagination>
        </div>

        <!-- modal section -->
        <div v-if="isVisible" class="action-modal h-100 w-100">
            <div class="container h-100">
                <?php $this->load->view($theme.'/admin/products/delete'); ?>
                <?php $this->load->view($theme.'/admin/products/detail'); ?>
                <?php $this->load->view($theme.'/admin/products/print'); ?>
                <?php $this->load->view($theme.'/admin/products/export'); ?>
            </div>
        </div>
    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/products.js'); ?>"></script>