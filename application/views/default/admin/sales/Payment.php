<div v-if="modal === 'payment'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-10 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-dollar-sign border-right border-white px-2 pr-3 mr-2"></i>
                    Sales Payment
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">

                <div class="row px-3">
                    <div class="col-md-5 mx-0 pr-5 py-5">
                        <span class="row">
                            <label class="col-md-6 text-right">Sub Total</label>
                            <p class="m-0">{{ settings.symbol }} {{ payments.total }}</p>
                        </span>

                        <span class="row">
                            <label class="col-md-6 text-right">Discount</label>
                            <p class="m-0">{{ payments.discount }} ( Percentage )</p>
                        </span>

                        <span class="row">
                            <label class="col-md-6 text-right">Amount Due</label>
                            <p class="m-0">{{ settings.symbol }} {{ totalA }}</p>
                        </span>

                        <span class="row mt-2">
                            <label class="lead col-md-6 text-right">Balance</label>
                            <p class="lead m-0">{{ settings.symbol }} {{ balance }}</p>
                        </span>

                        <hr class="w-100">
                        <span class="row">
                            <label class="col-md-6 text-right m-0 py-2">Cash Tendered</label>
                            <div class="input-group col pl-0">
                                <input v-model="payments.cash" @keyup.enter="payment()" type="number" class="form-control" placeholder="Cash" min="0" >
                                <div class="input-group-append">
                                    <button @click="payment()" type="button" class="btn btn-success">
                                        <i class="fas fa-dollar-sign px-1"></i>
                                    </button>
                                </div>
                            </div>
                        </span>

                        <hr class="w-100">
                        <span class="row">
                            <label class="lead col-md-6 text-right">Change</label>
                            <p class="lead m-0">{{ settings.symbol }} {{ change }}</p>
                        </span>
                    </div>

                    <div class="col-md-7 table-responsive bg-dark rounded py-3">
                        <div v-if="change >= 0" class="bg-success text-center rounded py-2">
                            No more payments to process for this sale.
                        </div>

                        <table class="table table-dark table-hover bg-transparent">
                            <thead>
                                <tr>
                                    <th scope="col">Date Time</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Biller</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-if="payments.history.length === 0">
                                    <td colspan="3" class="text-center">
                                        No payment records found for this sale.
                                    </td>
                                </tr>
                                <tr v-else v-for="payment in payments.history">
                                    <td>{{ payment.datetime }}</td>
                                    <td>{{ settings.symbol }} {{ payment.amount }}</td>
                                    <td>{{ payment.operator }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>