<div v-if="modal === 'detail'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-search border-right border-white px-2 mr-2"></i>
                    View Detail
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div v-if="selected.length > 1" class="d-md-flex no-gutters align-items-center w-100 border-bottom border-secondary pb-3 mb-3">
                    <div class="col-md-6">
                        Currently Viewing <strong class="lead text-warning pl-2">{{ current.view +1 }}</strong>
                        / <strong class="font-weight-light text-warning">{{ selected.length }}</strong>
                    </div>

                    <div class="col-md-6 text-right">
                        <div class="btn-group">
                            <button @click="prev()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-left"></i>
                            </button>
                            <button @click="next()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <h3 class="font-weight-light mb-1">Information</h3>
                <p class="font-weight-light">Sale information</p>
                <hr class="my-3">
                <div class="row">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Date Time</label>
                        <p>{{ detail.info.datetime }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Reference Code</label>
                        <p>{{ detail.info.reference }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Warehouse</label>
                        <p>{{ detail.info.warehouse }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Customer</label>
                        <p>{{ detail.info.customer }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Amount Due</label>
                        <p>{{ settings.symbol }} {{ detail.info.total }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Discount</label>
                        <p>{{ detail.info.discount }} ( Percentage )</p>
                    </div>


                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Paid</label>
                        <p>{{ settings.symbol }} {{ detail.info.paid }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Method</label>
                        <p class="text-capitalize">{{ detail.info.method }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Biller</label>
                        <p>{{ detail.info.biller }}</p>
                    </div>
                </div>

                <!-- current products section -->
                <h3 class="font-weight-light mt-5 mb-1">Sales Products</h3>
                <p class="font-weight-light">Products under this sale</p>
                <hr class="mt-3 mb-0">
                <div class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Code</th>
                                <th scope="col">Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Unit</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="product in detail.cart">
                                <td>{{ product.code }}</td>
                                <td>{{ product.name_display }}</td>
                                <td>{{ settings.symbol }} {{ product.price }}</td>
                                <td>{{ product.quantity }}</td>
                                <td>{{ product.unit }}</td>
                                <td>{{ settings.symbol }} {{ total(product.quantity, product.price) }}</td>
                            </tr>
                            <tr v-if="detail.cart.length === 0">
                                <td class="text-center" colspan="6"> No products found under this sale.</td> 
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row border-top border-secondary mx-0 pt-3">
                    <h2 class="col-md text-white text-right px-md-0 m-md-0 mb-sm-2">
                        <p class="lead d-inline-block mb-0 mr-2">Total Cost</p>
                        {{ settings.symbol }} {{ totalDS }}
                    </h2>
                </div>

            </div>
        </div>

    </div>
</div>