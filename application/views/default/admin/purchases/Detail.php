<div v-if="modal === 'detail'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-search border-right border-white px-2 mr-2"></i>
                    View Detail
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div v-if="selected.length > 1" class="d-md-flex no-gutters align-items-center w-100 border-bottom border-secondary pb-3 mb-3">
                    <div class="col-md-6">
                        Currently Viewing <strong class="lead text-warning pl-2">{{ current.view +1 }}</strong>
                        / <strong class="font-weight-light text-warning">{{ selected.length }}</strong>
                    </div>

                    <div class="col-md-6 text-right">
                        <div class="btn-group">
                            <button @click="prev()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-left"></i>
                            </button>
                            <button @click="next()" type="button" class="btn btn-outline-primary">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <h3 class="font-weight-light mb-1">Information</h3>
                <p class="font-weight-light">Purchase information</p>
                <hr class="my-3">
                <div class="row">
                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Date Time</label>
                        <p>{{ detail.purchase.datetime }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Reference Code</label>
                        <p>{{ detail.purchase.reference }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Warehouse</label>
                        <p>{{ detail.purchase.warehouse }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Supplier</label>
                        <p>{{ detail.purchase.supplier }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Tax Method</label>
                        <p>{{ detail.purchase.tax }}</p>
                    </div>

                    <div class="col-md-4">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Discount</label>
                        <p>{{ detail.purchase.discount }}</p>
                    </div>

                    <div class="col-md-8">
                        <label class="text-info font-weight-bold border-bottom border-info pb-2 w-50">Purchase Note</label>
                        <p>{{ detail.purchase.note }}</p>
                    </div>
                </div>

                <!-- current products section -->
                <h3 class="font-weight-light mt-5 mb-1">Purchase Products</h3>
                <p class="font-weight-light">Products under this purchase</p>
                <hr class="mt-3 mb-0">
                <div class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Code</th>
                                <th scope="col">Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="product in detail.products">
                                <td>{{ product.code }}</td>
                                <td>{{ product.name_display }}</td>
                                <td>{{ product.category }}</td>
                                <td>{{ settings.symbol }} {{ product.price }}</td>
                                <td>{{ product.quantity }}</td>
                            </tr>
                            <tr v-if="detail.products.length === 0">
                                <td class="text-center" colspan="6"> No products found under this purchase.</td> 
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row border-top border-secondary mx-0 pt-3">
                    <h2 class="col-md text-white text-right px-md-0 m-md-0 mb-sm-2">
                        <p class="lead d-inline-block mb-0 mr-2">Total Cost</p>
                        {{ settings.symbol }} {{ total }}
                    </h2>
                </div>

                <!-- quantity adjustment history -->
                <div v-if="detail.adjustments.length !== 0" class="rounded bg-dark px-4 mt-4" style="transition: height .2; overflow: hidden;">
                    <h3 class="font-weight-light mt-3 mb-1">Adjustment History</h3>
                    <p class="font-weight-light">Quantity adjustments for this purchase</p>
                    <hr class="mt-3 mb-0">
                    <div class="table-responsive">
                        <table class="table table-dark table-hover bg-transparent">
                            <thead>
                                <tr>
                                    <th scope="col">Date Time</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Processed By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="adjustment in detail.adjustments">
                                    <td>{{ adjustment.datetime }}</td>
                                    <td>{{ adjustment.name_display }}</td>
                                    <td>{{ adjustment.category }}</td>
                                    <td>{{ adjustment.quantity }}</td>
                                    <td>James Malatabon</td>
                                </tr>
                                <tr v-if="detail.adjustments.length === 0">
                                    <td class="text-center" colspan="6"> No adjustments found under this purchase.</td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- receive history section -->
                <div v-if="detail.purchase.status == 'returned' && detail.history.length !== 0">
                    <hr class="mt-5 mb-2">
                    <h3 class="font-weight-light mb-1">Receive History</h3>
                    <p class="font-weight-light">Products when received</p>
                    <hr class="mt-3 mb-0">
                    <div class="table-responsive">
                        <table class="table table-dark table-hover bg-transparent">
                            <thead>
                                <tr>
                                    <th scope="col">Code</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="product in detail.history">
                                    <td>{{ product.code }}</td>
                                    <td>{{ product.name_display }}</td>
                                    <td>{{ product.category }}</td>
                                    <td>{{ settings.symbol }} {{ product.price }}</td>
                                    <td>{{ product.quantity }}</td>
                                </tr>
                                <tr v-if="detail.history.length === 0">
                                    <td class="text-center" colspan="6"> No products found under this purchase.</td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="row border-top border-secondary mx-0 pt-3">
                        <h2 class="col-md text-white text-right px-md-0 m-md-0 mb-sm-2">
                            <p class="lead d-inline-block mb-0 mr-2">Total Cost</p>
                            {{ settings.symbol }} {{ totalH }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>