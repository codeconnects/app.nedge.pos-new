<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>
        </div>

        <form @submit.prevent="purchase()">
            <!-- order section -->
            <div class="card bg-dark mb-3">
                <h5 class="card-header font-weight-light">
                    <i class="fas fa-shopping-cart mr-2"></i></i>
                    Purchase Products
                </h5>

                <div class="card-body rounded-bottom">
                    <div class="row border-bottom border-secondary mx-0 pb-3">
                        <div class="col-md-5">
                            <div class="input-group">
                                <input v-model="search" @input="searchp()" @keydown.escape="search = ''" type="text" class="form-control" placeholder="Search products" :readonly="isProcess">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="button" disabled>
                                        <i class="fas fa-cart-plus"></i>
                                    </button>
                                </div>
                            </div>

                            <div v-if="search.length > 0" class="autocomplete bg-dark rounded-bottom shadow w-100 py-3">
                                <span v-if="products.length === 0" class="d-block text-center px-3">
                                    No records found for "{{ search }}".
                                </span>
                                <span v-else v-for="product in products" @click="addToCart(product.id)" class="items row align-items-center w-100 m-0 px-3 py-1">
                                    <p class="col-md-auto text-md-center m-0">{{ product.code }}</p>
                                    <p class="col font-weight-light border-left border-secondary m-0">{{ product.name_display }}</p>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-dark table-hover bg-transparent">
                            <thead>
                                <tr>
                                    <th scope="col">Code</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="product in orders">
                                    <td>{{ product.code }}</td>
                                    <td>{{ product.name_display }}</td>
                                    <td>{{ product.category }}</td>
                                    <td class="py-1">
                                        <input v-model="product.price" class="form-control" style="width: 100px;" type="number" min=1 :readonly="isProcess" required>
                                    </td>
                                    <td class="py-1">
                                        <input v-model="product.quantity" class="form-control" style="width: 70px;" type="number" min=1 :readonly="isProcess" required>
                                    </td>
                                    <td class="py-1">
                                        <button @click="removeFromCart(product.id)" type="button" class="btn btn-danger" :disabled="isProcess">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr v-if="orders.length === 0">
                                    <td class="text-center" colspan="6"> No products added to orders yet.</td> 
                                </tr>
                            </tbody>
                        </table>

                        <div v-if="orders.length !== 0" class="row w-100 border-top border-secondary mx-0 pt-3">
                            <h2 class="col-md-auto text-white">
                                <p class="lead d-inline-block mb-0 mr-2">Total Cost</p>
                                {{ settings.symbol }} {{ total }}
                            </h2>
                            <div class="col text-right">
                                <button type="submit" class="btn btn-success" :disabled="isProcess">
                                    <i v-if="!isProcess" class="fas fa-check mr-1"></i>
                                    <i v-if="isProcess" class="fas fa-spinner mr-1"></i>
                                    Create Purchase
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- information section -->
            <div class="card bg-dark mb-3">
                <h5 class="card-header font-weight-light">
                    <i class="fas fa-pen-alt mr-2"></i>
                    Purchase Information
                </h5>

                <div class="card-body row rounded-bottom mx-0">
                    <div class="form-group col-md-4">
                        <label>Date Time</label>
                        <input v-model="information.datetime" type="datetime-local" class="form-control">
                        <small class="form-text text-muted">Fill this for late entries</small>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Reference</label>
                        <div class="input-group">
                            <input v-model="information.reference" type="text" class="form-control" placeholder="Enter reference" required>
                            <div class="input-group-append">
                                <button @click="generate()" class="btn btn-secondary" type="button" data-toggle="tooltip" data-placement="top" title="Generate Reference">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Warehouse</label>
                        <select v-model="information.warehouse" class="custom-select">
                            <option v-for="warehouse in dependencies.warehouses" :value="warehouse.id">{{ warehouse.name }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Supplier</label>
                        <div class="input-group">
                            <select v-model="information.supplier" class="custom-select">
                                <option v-for="supplier in dependencies.suppliers" :value="supplier.id">{{ supplier.lastname }}, {{ supplier.firstname }}</option>
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button" data-toggle="tooltip" data-placement="top" title="Create Supplier">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <small class="form-text text-muted">Create supplier if not on the list</small>
                    </div>

                     <div class="form-group col-md-4">
                        <label>Tax Method</label>
                        <select v-model="information.tax" class="custom-select">
                            <option v-for="tax in dependencies.taxes" :value="tax.id">{{ tax.name }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Discount</label>
                        <input v-model="information.discount" class="form-control" type="number" placeholder="Enter discount" min=0 max="100">
                    </div>

                    <hr class="w-100">
                    <div class="form-group col-md-8">
                        <label>Note</label>
                        <textarea v-model="information.note" class="form-control" placeholder="Purchase note"></textarea>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/purchases/create.js') ?>"></script>