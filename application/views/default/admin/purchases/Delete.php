<div v-if="modal === 'return'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-redo border-right border-white px-2 pr-3 mr-2"></i>
                    Return Purchases
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">This process will move the <strong class="text-warning">remaining stocks</strong> under this purchase into <strong class="text-primary">returned stocks</strong> and set this purchase' status to returned. Stocks under this purchase that are already sold are ignored.</p>
                </div>

                <div class="table-responsive border-bottom border-secondary">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Reference</th>
                                <th scope="col">Date Time</th>
                                <th scope="col">Total Cost</th>
                                <th scope="col">Supplier</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="purchase in selectedList">
                                <td>{{ purchase.reference }}</td>
                                <td>{{ purchase.datetime }}</td>
                                <td>{{ settings.symbol }} {{ purchase.total }}</td>
                                <td>{{ purchase.supplier }}</td>
                                <td class="p-1">
                                    <button @click="restore(purchase.id)" type="button" class="btn btn-primary" :disabled="isProcess">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="pt-3">
                    <button @click="startReturn()" type="button" class="btn btn-danger" :disabled="isProcess">
                        <i v-if="!isProcess" class="fas fa-check mr-2"></i>
                        <i v-else class="fas fa-spinner mr-2"></i>
                        Return Purchases
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>