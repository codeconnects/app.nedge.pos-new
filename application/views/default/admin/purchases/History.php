<div v-if="modalName === 'history'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-history border-right border-white px-2 pr-3 mr-2"></i>
                    Receive History
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">These datas shown are the original datas when you created and received this purchase. Products, quantities, and prices of purchases are kept for future reference in dealing with <strong class="text-warning">Return Purchases</strong>.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Code</th>
                                <th scope="col">Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="product in products">
                                <td>{{ product.code }}</td>
                                <td>{{ product.name_display }}</td>
                                <td>{{ product.category }}</td>
                                <td>{{ settings.accounting.currency_symbol }} {{ product.price }}</td>
                                <td>{{ product.quantity }}</td>
                            </tr>
                            <tr v-if="products.length === 0">
                                <td class="text-center" colspan="6"> No products found under this purchase.</td> 
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row border-top border-secondary mx-0 pt-3">
                    <h2 class="col-md text-white text-right px-md-0 m-md-0 mb-sm-2">
                        <p class="lead d-inline-block mb-0 mr-2">Total Cost</p>
                        {{ settings.accounting.currency_symbol }} {{ calculatedTotal }}
                    </h2>
                </div>

            </div>
        </div>

    </div>
</div>