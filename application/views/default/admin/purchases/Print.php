<div v-if="modal === 'print'" class="row justify-content-center align-items-center h-100">
    <div class="col-md-9 h-75">
        
        <div class="card bg-dark h-100 w-100">
            <h5 class="card-header d-flex font-weight-light h-auto">
                <div class="mr-auto">
                    <i class="fas fa-tasks border-right border-white px-2 pr-3 mr-2"></i>
                    <!-- <i class="fas fa-barcode border-right border-white px-2 pr-3 mr-2"></i> -->
                    Print Purchases
                </div>

                <i @click="isVisible = false" class="fas fa-times" style="cursor:pointer;"></i>
            </h5>
            <div class="card-body mh-100">
                <div class="notice border-bottom border-secondary px-3">
                    <h5>Understanding the process</h5>
                    <p class="mt-3 mb-4">This will print the selected purchases with their stocks. Double check the list below before printing to avoid printing of unnecessary purchases (<strong class="text-warning">note that purchases with stocks are a lot of data</strong>).</p>
                </div>

                <div class="table-responsive border-bottom border-secondary">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">Reference</th>
                                <th scope="col">Date Time</th>
                                <th scope="col">Total Cost</th>
                                <th scope="col">Supplier</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="purchase in selectedList">
                                <td>{{ purchase.reference }}</td>
                                <td>{{ purchase.datetime }}</td>
                                <td>{{ settings.symbol }} {{ purchase.total }}</td>
                                <td>{{ purchase.supplier }}</td>
                                <td class="p-1">
                                    <button @click="restore(purchase.id)" type="button" class="btn btn-primary" :disabled="isProcess">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="d-flex pt-3">
                    <button @click="startPrint()" type="button" class="btn btn-primary" :disabled="isProcess">
                        <i v-if="!isProcess" class="fas fa-print mr-2"></i>
                        <i v-else class="fas fa-spinner mr-2"></i>
                        Start Printing
                    </button>
                    <div v-if="isProcess" class="d-inline-block px-3 py-2">
                        Printing {{ current.process }} out of {{ selected.length }}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>