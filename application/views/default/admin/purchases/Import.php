<div class="container pb-5">
    <div class="row">
        <!-- sidebar section -->
        <?php $this->load->view('default/extension/sidebar'); ?>

        <!-- page content -->
        <div class="page-content col">
            <!-- page header -->
            <div class="page-header d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
                <!-- dashboard / breadcrumb -->
                <?php $this->load->view('default/extension/dashboard'); ?>
            </div>

            <!-- page body -->
            <div class="page-body">
                <!-- page controls -->
                <div id="appActions" class="d-flex mb-3">
                    <!-- searchbox -->
                    <div class="col px-md-0">
                        <div class="input-group">
                            <input v-model="searchWord" @keyup.enter="searchProduct()" type="text" class="form-control" placeholder="Search purchases">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- action buttons -->
                    <div class="row col-md-8 justify-content-end align-items-center px-md-0 m-0">
                        <div class="btn-group" role="group">
                            <button v-if="isCompleted" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top">
                                <i class="fas fa-check"></i>
                            </button>
                            
                            <button @click="importDemo()" type="button" class="btn btn-outline-primary" v-bind:class="{ disabled: isProcessing }" :disabled="isProcessing" data-toggle="tooltip" data-placement="top" title="Import Product">
                                <i v-if="!isProcessing" class="fas fa-file-import"></i>
                                <i v-if="isProcessing" class="fas fa-spinner"></i>
                            </button>
                            <button @click="showModal('receive')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Receive Purchase">
                                <i class="fas fa-box-open"></i>
                            </button>
                            <button @click="showModal('delete')" type="button" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Return Purchase">
                                <i class="fas fa-redo"></i>
                            </button>
                            <button @click="showModal('detail')" type="button" class="btn btn-outline-primary" v-bind:class="{ disabled: isProcessing }" :disabled="isProcessing" data-toggle="tooltip" data-placement="top" title="View Detail">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- page table -->
                <div id="appTable" class="table-responsive">
                    <table class="table table-dark table-hover bg-transparent">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <input @change="setSelected($event)" type="checkbox">
                                </th>
                                <th scope="col">Reference</th>
                                <th scope="col">Date Time</th>
                                <th scope="col">Total Cost</th>
                                <th scope="col">Supplier</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="purchase in purchases" @click="setSelected($event)">
                                <td><input v-model="selectedPurchases" :value="purchase.id" type="checkbox"></td>
                                <td>{{ purchase.reference }}</td>
                                <td>{{ purchase.datetime }}</td>
                                <td>{{ settings.currency_symbol }} {{ purchase.total }}</td>
                                <td>{{ purchase.supplier }}</td>
                                <td>
                                    <span class="badge px-3 py-1" v-bind:class="{ 'badge-info': purchase.status === 'pending', 'badge-success': purchase.status === 'received', 'badge-danger': purchase.status === 'returned' }">
                                        <div class="text-uppercase">{{ purchase.status }}</div>
                                    </span>
                                </td>
                            </tr>
                            <tr v-if="purchases.length === 0">
                                <td class="text-center" colspan="6">No purchases to show for now.</td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- pagination -->
                    <pagination :infinite="settings.infinite" :current="pagination.current" :margin="2" :total="pagination.total" @click="pageNavigate($event)"></pagination>
                </div>

            </div> <!-- page body end -->

            <!-- modal windows -->
            <div id="appModal" v-if="isVisible === true" class="action-modal h-100 w-100">
                <div class="container h-100">
                    <?php $this->load->view('default/admin/purchases/receive'); ?>
                    <?php $this->load->view('default/admin/purchases/detail'); ?>
                    <?php $this->load->view('default/admin/purchases/delete'); ?>
                </div>
            </div>

        </div> <!-- page content end -->
    </div> <!-- row end -->
</div> <!-- container end -->

<!-- page javascript -->
<script src="<?= base_url('assets/js/default/admin/purchases/import.js') ?>"></script>