<div class="row container m-auto px-0 pb-5">
    <?php $this->load->view($theme.'/extension/sidebar'); ?>

    <!-- start: page content -->
    <div id="approot" class="col">
        <div class="d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
            <?php $this->load->view($theme.'/extension/dashboard'); ?>

            <!-- widget section -->
            <div class="row col-md-6 justify-content-end align-items-center px-md-0 m-0">

            </div>
        </div>

        <div class="row px-3">
            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-dollar-sign border-right border-secondary pr-3 mr-2"></i>
                        Sales
                    </p>

                    <div class="card-body rounded-bottom">
                        Generate daily, weekly, monthly or annual sales report.
                    </div>

                    <button type="button" class="btn btn-primary rounded-0 py-2">
                        <i class="fas fa-cog mr-1"></i>
                        <!-- <i class="fas fa-spinner mr-1"></i> -->
                        Generate Report
                    </button>
                </div>
            </span>

            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-box-open border-right border-secondary pr-3 mr-2"></i>
                        Purchases
                    </p>

                    <div class="card-body rounded-bottom">
                        Generate reports for returned and received stock purchases.
                    </div>

                    <button type="button" class="btn btn-primary rounded-0 py-2">
                        <i class="fas fa-cog mr-1"></i>
                        <!-- <i class="fas fa-spinner mr-1"></i> -->
                        Generate Report
                    </button>
                </div>
            </span>

            <span class="col-md-4 px-1 mb-3">
                <div class="card bg-dark">
                    <p class="card-header lead">
                        <i class="fas fa-box border-right border-secondary pr-3 mr-2"></i>
                        Inventory
                    </p>

                    <div class="card-body rounded-bottom">
                        Generate daily, weekly, monthly and annual inventory report.
                    </div>

                    <button type="button" class="btn btn-primary rounded-0 py-2">
                        <i class="fas fa-cog mr-1"></i>
                        <!-- <i class="fas fa-spinner mr-1"></i> -->
                        Generate Report
                    </button>
                </div>
            </span>
        </div>

    </div>
    <!-- end: page content -->
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/admin/reports.js'); ?>"></script>