<div class="page-content container pb-5">
    <div class="row">
        <?php $this->load->view('default/extension/sidebar'); ?>

        <div class="col">

            <div class="page-header d-md-flex border-bottom border-dark pt-2 pb-3 mb-3">
                <div class="col px-md-0">
                    <span class="nav-link text-muted p-0">DASHBOARD</span>
                    <h2 class="font-weight-light py-0 my-0"><?= $page_name['dashboard']; ?></h2>
                </div>
            </div>

            <div>
                File not found.
            </div>

        </div>
    </div>
</div>