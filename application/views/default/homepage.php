<style>
	.tagline {
		background-repeat: no-repeat;
		background-size: 110%;
		background-position: center;
	}
</style>

<div id="approot" class="row justify-content-center align-items-center w-100 m-auto" style="height: 100vh;">
	<div class="row col-md-8 h-50 rounded px-0" style="overflow: hidden;">
		<div class="col-md-7 px-0">
			<div class="tagline h-100 p-5" style="background-image: url(<?= base_url('assets/img/system/login-background.jpg'); ?>);">
				<h4 class="font-weight-light py-2 mt-3">Welcome to NEDGE POS</h4>
				<p class="font-weight-light mt-3">
					We give you the best services in terms of POS and Inventory management. Be inline with technology and innovation, plugin-based features and more!
				</p>

				<p class="font-weight-light">
					Interested? Email us at <a class="text-white" href="#">support@app.nedge.pos.com</a>
				</p>
			</div>
		</div>

		<div class="col pt-3 bg-white">
			<p class="lead text-secondary text-center mb-0">Member Login</p>
			<form @submit.prevent="login()" class="border-top mt-3 pt-4 px-5">
				<div class="input-group my-2">
					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="fas fa-envelope"></i>
						</span>
					</div>
					<input v-model="user.email" type="email" class="form-control" placeholder="Enter email address" required autofocus>
				</div>

				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="fas fa-asterisk"></i>
						</span>
					</div>
					<input v-model="user.pass" type="password" class="form-control" placeholder="Enter password" required autofocus>
				</div>

				<button type="submit" class="btn btn-primary btn-block mb-2" :disabled="isProcess">
					<span v-if="!isProcess">Sign In</span>
					<i v-else class="fas fa-spinner"></i>
				</button>

				<a class="d-block text-secondary text-right" href="<?= base_url('forgot.php'); ?>">Forgot Password?</a>
			</form>
		</div>
	</div>
</div>

<!-- import javascript -->
<script src="<?= base_url('assets/js/'.$theme.'/homepage.js'); ?>"></script>