<section class="top-navbar bg-dark sticky-top">
    <div class="container pr-md-0">

    <nav class="navbar navbar-expand-lg navbar-dark pr-md-0">
        <a class="navbar-brand mr-md-4" href="<?= base_url('admin/overview'); ?>">
            <img class="mr-1"src="https://getbootstrap.com/docs/4.1/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
            <?= $site_name; ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <div class="navbar-nav mr-auto">
                <a class="nav-link mx-md-1 my-md-1" href="<?= base_url('admin/pos'); ?>">Point of Sale</a>
                <a class="nav-link mx-md-1 my-md-1" href="<?= base_url('admin/registers'); ?>">Registers</a>
                <a class="nav-link mx-md-1 my-md-1" href="<?= base_url('admin/profit'); ?>">Profit</a>

                <div class="dropdown mx-md-1 my-md-1">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tools
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Calculator</a>
                        <a class="dropdown-item" href="#">Calendar</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Clear Local Data</a>
                    </div>
                </div>

                <?php if($quantity_alert > 0) { ?>
                <button type="button" class="btn btn-outline-danger ml-md-4 my-1">
                    Products <span class="badge badge-danger ml-1"><?= $quantity_alert; ?></span>
                </button>
                <?php } ?>
            </div>

            <div class="dropdown col-md-auto px-0 my-1">
                <button class="btn btn-dark dropdown-toggle w-100" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $user_fullname; ?>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= base_url('admin/profile'); ?>">Profile Info</a>
                    <a class="dropdown-item" href="<?= base_url('admin/password'); ?>">Change Password</a>
                    <a class="dropdown-item" href="<?= base_url('admin/settings'); ?>">Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= base_url('admin/account/logout'); ?>">Logout</a>
                </div>
            </div>
        </div>
    </nav>

    </div>
</section>