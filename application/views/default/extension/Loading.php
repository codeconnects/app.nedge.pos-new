<div class="loading-overlay">
	<div class="row justify-content-center align-items-center">
		<p class="lead">
			<i class="fas fa-spinner"></i> Loading Components
		</p>
	</div>
</div>