<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="<?= $meta_description; ?>">
        <meta name="keywords" content="<?= $meta_keywords; ?>">
        <meta name="author" content="<?= $meta_author; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?= $site_name .' | '. $page_name['title']; ?></title>

        <!-- import css -->
        <link rel="stylesheet" href="<?= base_url('assets/css/lib/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/lib/animate.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/lib/fontawesome.all.css'); ?>">

        <!-- theme css -->
        <link rel="stylesheet" href="<?= base_url('assets/css/default/main.css'); ?>">

        <!-- import js -->
        <script src="<?= base_url('assets/js/lib/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/lib/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/lib/sweetalert.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/lib/vue.js'); ?>"></script>

        <script src="<?= base_url('assets/js/default/main.js'); ?>"></script>
        <script src="<?= base_url('assets/js/default/components.js'); ?>"></script>
    </head>
    <body class="bg-dark">
