<div class="sidebar col-md-3 pr-md-5 mb-4 mb-md-0">
    <nav class="nav flex-column sticky-top" style="top: 62px; z-index: 1;">
        <span class="nav-link text-muted">DASHBOARDS</span>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'overview'? 'active': ''; ?>" href="<?= base_url('admin/overview'); ?>">
            <i class="fas fa-tachometer-alt"></i>Overview
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'products'? 'active': ''; ?>" href="<?= base_url('admin/products'); ?>">
            <i class="fas fa-box"></i>Products
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'purchases'? 'active': ''; ?>" href="<?= base_url('admin/purchases'); ?>">
            <i class="fas fa-box-open"></i>Purchases
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'stocks'? 'active': ''; ?>" href="<?= base_url('admin/stocks'); ?>">
            <i class="fas fa-boxes"></i>Stocks
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'sales'? 'active': ''; ?>" href="<?= base_url('admin/sales'); ?>">
            <i class="fas fa-dollar-sign"></i>Sales
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'quotations'? 'active': ''; ?>" href="<?= base_url('admin/quotations'); ?>">
            <i class="fas fa-chart-line"></i>Quotations
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'transfers'? 'active': ''; ?>" href="<?= base_url('admin/transfers'); ?>">
            <i class="fas fa-exchange-alt"></i>Transfers
        </a>

        <span class="nav-link text-muted mt-3">MORE</span>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'people'? 'active': ''; ?>" href="<?= base_url('admin/people'); ?>">
            <i class="fas fa-users"></i>People
        </a>
        <a class="nav-link rounded <?= strtolower($page_name['title']) == 'reports'? 'active': ''; ?>" href="<?= base_url('admin/reports'); ?>">
            <i class="far fa-chart-bar"></i>Reports
        </a>
    </nav>
</div>