<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct() {
        parent::__construct();

        // check if logged in user exist
        if($this->session->has_userdata('id'))
            header('Location: '. base_url('admin/overview'));
    }

    public function _remap($method, $param = array()) {
        if(method_exists($this, $method))
            $this->$method($param);
    }

	private function index() {
        // build header info
        $header['meta_description'] = 'pos inventory';
        $header['meta_keywords'] = 'pos, inventory, stock manager';
        $header['meta_author'] = 'james malatabon';
        $header['site_name'] = $this->config()['system']['site'];

        // set page name
        $header['page_name'] = array(
            'title' => 'Home',
            'dashboard' => 'Login'
        );

        // set param theme
        $param['theme'] = $this->config()['system']['theme'];

        $this->load->view($this->config()['system']['theme'].'/extension/header', $header);
        $this->load->view($this->config()['system']['theme'].'/homepage', $param);
        $this->load->view($this->config()['system']['theme'].'/extension/footer');
	}
}
