<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Misc extends MY_Controller {

	public function __construct() {
		parent::__construct();

		// load dependencies
		$this->load->model('category_model', 'category');
		$this->load->model('brand_model', 'brand');
		$this->load->model('unit_model', 'unit');

		$this->load->model('warehouse_model', 'warehouse');
		$this->load->model('people_model', 'people');
		$this->load->model('tax_model', 'tax');

		$this->load->model('role_model', 'role');

		// check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
	}

	public function load_product_dependencies() {
		$query['categories'] = $this->category->load();
		$query['subcategories'] = $this->category->loadSub();
		$query['brands'] = $this->brand->load();
		$query['units'] = $this->unit->load();

		// validate values
		foreach ($query as $item) {
			if(sizeof($item) === 0) {
				$item = array('id' => 1, 'name' => 'General');
			}
		}

		$data['success'] = true;
		$data['content'] = $query;

		echo json_encode($data);
	}

	public function load_purchase_dependencies() {
		$query['warehouses'] = $this->warehouse->load();
		$query['suppliers'] = $this->people->load();
		$query['taxes'] = $this->tax->load();

		// validate resulting values
		if(sizeof($query['warehouses']) === 0)
			$query['warehouses'] = array('id' => 1, 'name' => 'General Warehouse');

		if(sizeof($query['suppliers']) === 0)
			$query['suppliers'] = array('id' => 2, 'name' => 'General Supplier');

		if(sizeof($query['taxes']) === 0)
			$query['taxes'] = array('id' => 1, 'name' => 'VAT Free');

		$data['success'] = true;
		$data['content'] = $query;
		$data['setting'] = $this->config('external');

		echo json_encode($data);
	}

	public function load_sale_dependencies() {
		$query['warehouses'] = $this->warehouse->load();
		$query['customers'] = $this->people->load('customer');
		$query['billers'] = $this->people->load('biller');

		// validate resulting values
		if(sizeof($query['warehouses']) === 0)
			$query['warehouses'] = array('id' => 1, 'name' => 'General Warehouse');

		if(sizeof($query['customers']) === 0)
			$query['customers'] = array('id' => 2, 'name' => 'Walk In Customer');

		if(sizeof($query['billers']) === 0)
			$query['billers'] = array('id' => 1, 'name' => 'General Biller');

		$data['success'] = true;
		$data['content'] = $query;
		$data['setting'] = $this->config('external');

		echo json_encode($data);
	}

	public function load_user_dependencies() {
		$query = $this->role->load();

		$data['success'] = sizeof($query) !== 0;
		$data['content'] = $query;
		$data['setting'] = $this->config('external');

		echo json_encode($data);
	}

	// miscellaneous
	public function load_settings() {
		echo json_encode($this->config('external'));
	}

}

?>