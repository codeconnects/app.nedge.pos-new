<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouses extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('warehouse_model', 'warehouse');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        if(method_exists($this, $method))
            $this->$method($params);
        else
            $this->error_404();
    }

	private function index() {
        $this->template('warehouses');
    }

    private function request_load() {
        $query = $this->warehouse->load();

        $data['success'] = true;
        $data['content'] = $query;

        echo json_encode($data);
    }

    // load handler
    private function request_populate() {
        // get post data
        $post['keyword'] = $this->input->post('keyword');
        $post['pagenum'] = $this->input->post('pagenum');

        // setup query parameters
        $param['search'] = $post['keyword'];
        $param['maxrow'] = $this->config('internal')['system']['maxrow'];
        $param['offset'] = ($post['pagenum'] -1) * $param['maxrow'];

        $query = $this->warehouse->populate($param);
        $count = ceil($query['count'] / $param['maxrow']);

        $data['success'] = true;
        $data['content'] = $query['data'];
        $data['setting'] = $this->config('external');
        $data['pageset']['current'] = intval($post['pagenum']);
        $data['pageset']['maxpage'] = $count;

        echo json_encode($data);
    }
}
