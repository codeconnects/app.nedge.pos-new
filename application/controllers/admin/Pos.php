<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends MY_Controller {

	public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('pos_model', 'pos');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        if(method_exists($this, $method))
            $this->$method($params);
        else
            $this->error_404();
    }

	private function index() {
        $this->template('pos');
    }

    private function request_search() {
    	$param['search'] = $this->input->post('keyword');
    	$param['warehouse'] = $this->input->post('warehouse');

    	if(!$this->input->post('source')) {
            $param['method'] = $this->config()['account']['method'] === 'fifo' ? 'DESC' : 'ASC';
            $param['oversell'] = $this->config()['account']['oversell'];
            
            $query = $this->pos->load($param);
        }else
            $query = $this->pos->loadCustomers($param);

    	$data['success'] = true;
    	$data['content'] = $query;

        // for debugging
        $data['debug'] = $param;

    	echo json_encode($data);
    }

    private function request_suspended() {
    	$query = $this->pos->suspended();

    	$data['success'] = true;
    	$data['content'] = $query;

    	echo json_encode($data); 
    }

    private function request_suspend() {
    	$post['info'] = $this->input->post('info');
    	$post['products'] = $this->input->post('products');

    	$param = $post['info'];
        $param['id'] = $this->pos->generate_id();
    	$param['detail'] = $post['products'];
    	$param['operator'] = 1; // this should be the current user ID

    	$query = $this->pos->suspend($param);
    	$data['success'] = $query;

    	// for debugging
    	// $data['content'] = $param;

    	echo json_encode($data);
    }

    private function request_detail() {
    	$oid = $this->input->post('order');
    	$query = $this->pos->detail($oid);

    	$data['success'] = sizeof($query) !== 0;
    	$data['content'] = $query;

    	echo json_encode($data);
    }

    private function request_payment() {
        // get post data
        $post['info'] = $this->input->post('info');
        $post['orders'] = $this->input->post('orders');
        $post['order'] = $this->input->post('order');

        // build query parameter
        $param['info'] = $post['info'];
        $param['orders'] = $post['orders'];
        $param['order'] = $post['order'];
        $param['info']['id'] = $this->pos->generate_id('sales');
        $param['info']['operator'] = 1; // this should be current user

        $query = $this->pos->payment($param);
        $data['success'] = $query;

        // for debugging
        // $data['debug'] = $param;

        echo json_encode($data);
    }
}
