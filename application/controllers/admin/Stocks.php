<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stocks extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('stock_model', 'stock');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        $address = array(
            'index' => 'stocks',
            'expire' => 'stocks/expire'
        );

        if(method_exists($this, $method)) {
            $this->$method();
        }else {
            $location = isset($address[$method]) ? $address[$method] : null;
            if($location)
                $this->template($location);
            else
                $this->error_404();
        }
    }

    private function request_load() {
        // get post data
        $post['keyword'] = $this->input->post('keyword');
        $post['storage'] = $this->input->post('warehouse');
        $post['pagenum'] = $this->input->post('pagenum');
        $post['source'] = $this->input->post('source');

        // setup query parameters
        $param['search'] = $post['keyword'];
        $param['whouse'] = $post['storage'];
        $param['expire'] = $post['source'] ? 1 : 0;
        $param['oversell'] = $this->config()['account']['oversell'];
        $param['maxrow'] = $this->config()['system']['maxrow'];
        $param['offset'] = ($post['pagenum'] -1) * $param['maxrow'];

        $query = $this->stock->populate($param);
        $count = ceil($query['count'] / $param['maxrow']);

        $data['success'] = true;
        $data['content'] = $query['data'];
        $data['setting'] = $this->config('external');
        $data['pageset']['current'] = intval($post['pagenum']);
        $data['pageset']['maxpage'] = $count;

        // for debugging
        // $data['debug'] = $query;

        echo json_encode($data);
    }

    private function request_view() {
        $sid = $this->input->post('sid');
        $query = $this->stock->view($sid);

        $data['success'] = sizeof($query) !== 0;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function request_update() {
        $adjustments = $this->input->post('adjustments');

        $counter = 0;
        foreach ($adjustments as $item) {
            $query = $this->stock->update($item);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($adjustments);
        echo json_encode($data);
    }

    private function request_adjust() {
        $adjustments = $this->input->post('adjustments');

        $counter = 0;
        foreach ($adjustments as $item) {
            $query = $this->stock->adjust($item);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($adjustments);
        echo json_encode($data);
    }

    private function request_expire() {
        $stocks = $this->input->post('stocks');

        $counter = 0;
        foreach ($stocks as $sid) {
            $query = $this->stock->expire($sid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($stocks);
        echo json_encode($data);
    }

    private function request_restore() {
        $stocks = $this->input->post('stocks');

        $counter = 0;
        foreach ($stocks as $sid) {
            $query = $this->stock->restore($sid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($stocks);
        echo json_encode($data);
    }
}