<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

	public function __construct() {
		parent::__construct();

		// load dependencies
		$this->load->model('user_model', 'user');
		$this->load->model('role_model', 'role');
	}

	public function login() {
		$param = $this->input->post('user');
		$query = $this->user->login($param['email']);

		// set default data
		$data['success'] = false;
		$data['error'] = 0;

		if($query) {
			if(password_verify($param['pass'], $query['pass']) && $query['is_active'] == 1) {
				$this->session->set_userdata($query);
				$data['success'] = true;
			}else
				$data['error'] = $query['is_active'] != 1? 2: 1;
		}

		echo json_encode($data);
	}

	public function logout() {
		// destroy session
		$this->session->sess_destroy();

		// redirect to homepage
		header('Location: '. base_url());
		exit();
	}

}

?>