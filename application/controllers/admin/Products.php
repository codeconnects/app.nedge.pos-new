<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {
    public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('product_model', 'product');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $param = array()) {
        $address = array(
            'index' => 'products',
            'create' => 'products/create',
            'update' => 'products/update',
            'import' => 'products/import'
        );

        if(method_exists($this, $method)) {
            $this->$method();
        }else {
            $location = isset($address[$method]) ? $address[$method] : null;
            if($location)
                $this->template($location);
            else
                $this->error_404();
        }
    }

    private function request_load() {
        // get post data
        $post['keyword'] = $this->input->post('keyword');
        $post['pagenum'] = $this->input->post('pagenum');
        $post['source'] = $this->input->post('source');

        // setup query parameters
        $param['search'] = $post['keyword'];
        $param['source'] = $post['source'];
        $param['maxrow'] = $this->config()['system']['maxrow'];
        $param['offset'] = ($post['pagenum'] -1) * $param['maxrow'];

        $query = $this->product->populate($param);
        $count = ceil($query['count'] / $param['maxrow']);

        $data['success'] = true;
        $data['content'] = $query['data'];
        $data['setting'] = $this->config('external');
        $data['pageset']['current'] = intval($post['pagenum']);
        $data['pageset']['maxpage'] = $count;

        // for debugging
        $data['debug'] = $param;

        echo json_encode($data);
    }

    private function request_view() {
        $pid = $this->input->post('pid');
        $query = $this->product->view($pid);

        $data['success'] = sizeof($query) !== 0;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function request_detail() {
        $pid = $this->input->post('pid');
        $query = $this->product->load($pid);

        $data['success'] = sizeof($query) !== 0;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function request_create() {
        $param['data'] = $this->input->post('data');
        $param['data']['id'] = $this->product->generate_id();

        // remove empty values
        if(strlen(str_replace(' ', '', $param['data']['code_hsn'])) === 0)
            unset($param['data']['code_hsn']);

        if(strlen(str_replace(' ', '', $param['data']['image'])) === 0)
            unset($param['data']['image']);

        if(strlen(str_replace(' ', '', $param['data']['description'])) === 0)
            unset($param['data']['description']);

        $query = $this->product->create($param);
        $data['success'] = $query;

        // TC: success testing
        // $data['success'] = true;
        // $data['content'] = $param;

        echo json_encode($data);
    }

    private function request_update() {
        $post = $this->input->post('data');

        // setup parameter
        $param['pid'] = $post['id'];
        $param['data'] = $post;

        unset($param['data']['id']);
        unset($param['data']['parent']);

        // remove empty values
        if(strlen(trim($param['data']['code_hsn'])) === 0)
            unset($param['data']['code_hsn']);

        if(strlen(trim($param['data']['image'])) === 0)
            unset($param['data']['image']);

        if(strlen(trim($param['data']['description'])) === 0)
            unset($param['data']['description']);

        $query = $this->product->update($param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function request_delete() {
        $products = $this->input->post('products');

        $counter = 0;
        foreach ($products as $pid) {
            $query = $this->product->delete($pid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($products);
        echo json_encode($data);
    }
}

?>