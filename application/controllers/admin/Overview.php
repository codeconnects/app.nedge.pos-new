<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends MY_Controller {

	public function __construct() {
        parent::__construct();

        // load dependencies
        

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        if(method_exists($this, $method))
            $this->$method($params);
        else
            $this->error_404();
    }

	private function index() {
        $this->template();
    }
}
