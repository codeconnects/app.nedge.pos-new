<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('user_model', 'user');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        $address = array(
            'index' => 'settings',
            'system' => 'settings/system',
            'pos' => 'settings/pos',
            'users' => 'settings/users',
            'permissions' => 'settings/permissions',

            // users sub-address
            'users/create' => 'settings/users/create',
            'users/update' => 'settings/users/update',
            'users/import' => 'settings/users/import'
        );

        if(method_exists($this, $method)) {
            $this->$method();
        }else {
            if(count($params) > 0)
                $method = $method."/".$params[0];
            
            // set location
            $location = isset($address[$method]) ? $address[$method] : null;
            
            if($location)
                $this->template($location);
            else
                $this->error_404();
        }
    }

    // functions: users
    private function user_load() {
        // get post data
        $post['keyword'] = $this->input->post('keyword');
        $post['pagenum'] = $this->input->post('pagenum');

        // setup query parameters
        $param['search'] = $post['keyword'];
        $param['oversell'] = $this->config()['account']['oversell'];
        $param['maxrow'] = $this->config()['system']['maxrow'];
        $param['offset'] = ($post['pagenum'] -1) * $param['maxrow'];

        $query = $this->user->populate($param);
        $count = ceil($query['count'] / $param['maxrow']);

        $data['success'] = true;
        $data['content'] = $query['data'];
        $data['roles'] = $query['roles'];

        $data['setting'] = $this->config('external');
        $data['pageset']['current'] = intval($post['pagenum']);
        $data['pageset']['maxpage'] = $count;

        // for debugging
        $data['debug'] = $query;

        echo json_encode($data);
    }

    private function user_create() {
        $param = $this->input->post('user');

        // add operator id
        $param['created_by'] = 1; // should be current user

        // format password
        $param['pass'] = password_hash($param['pass'], PASSWORD_BCRYPT);

        // remove unnecessary fields
        unset($param['repass']);

        $query = $this->user->create($param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function user_update() {
        $param = $this->input->post('user');
        $uid = $param['id'];

        // format password
        if(array_key_exists('pass', $param)) {
            $param['pass'] = password_hash($param['pass'], PASSWORD_BCRYPT);
            unset($param['repass']);
        }

        // remove unnecessary fields
        unset($param['id']);

        $query = $this->user->update($uid, $param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function user_detail() {
        $uid = $this->input->post('uid');
        $query = $this->user->detail($uid);

        // remove unnecessary fields
        unset($query['pass']);
        unset($query['is_active']);
        unset($query['created_dt']);
        unset($query['created_by']);

        // format datetime
        $temp = explode(' ', $query['bday']);
        $query['bday'] = $temp[0];

        $data['success'] = sizeof($query) !== 0;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function user_view() {
        $uid = $this->input->post('uid');
        $query = $this->user->view($uid);

        $data['success'] = sizeof($query) !== 0;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function user_delete() {
        $users = $this->input->post('users');

        $counter = 0;
        foreach ($users as $uid) {
            $query = $this->user->delete($uid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($users);
        echo json_encode($data);
    }

    private function user_role() {
        $users = $this->input->post('users');

        $counter = 0;
        foreach ($users as $user) {
            $param['uid'] = $user['id'];
            $param['role'] = $user['role_id'];

            $query = $this->user->role($param);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($users);
        echo json_encode($data);
    } 
}
