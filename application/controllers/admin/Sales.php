<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('sale_model', 'sale');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        $address = array(
            'index' => 'sales',
            'create' => 'sales/create',
            'update' => 'sales/update',
            'import' => 'sales/import'
        );

        if(method_exists($this, $method)) {
            $this->$method();
        }else {
            $location = isset($address[$method]) ? $address[$method] : null;
            if($location)
                $this->template($location);
            else
                $this->error_404();
        }
    }

    private function request_load() {
        // get post data
        $post['keyword'] = $this->input->post('keyword');
        $post['storage'] = $this->input->post('warehouse');
        $post['pagenum'] = $this->input->post('pagenum');

        // setup query parameters
        $param['search'] = $post['keyword'];
        $param['whouse'] = $post['storage'];
        $param['maxrow'] = $this->config()['system']['maxrow'];
        $param['offset'] = ($post['pagenum'] -1) * $param['maxrow'];

        $query = $this->sale->populate($param);
        $count = ceil($query['count'] / $param['maxrow']);

        $data['success'] = true;
        $data['content'] = $query['data'];
        $data['setting'] = $this->config('external');
        $data['pageset']['current'] = intval($post['pagenum']);
        $data['pageset']['maxpage'] = $count;

        // for debugging
        // $data['debug'] = $query;

        echo json_encode($data);
    }

    private function request_payment() {
        $sid = $this->input->post('sid');
        $query = $this->sale->payments($sid);

        $data['success'] = true;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function request_create() {
        $post['sale'] = $this->input->post('info');
        $post['cart'] = $this->input->post('cart');
        $post['sale']['id'] = $this->sale->generate_id();
        
        // arrange product fields
        $temp = array();
        foreach ($post['cart'] as $item) {
            array_push($temp, array(
                'sale' => $post['sale']['id'],
                'purchase' => $item['purchase'],
                'product' => $item['id'],
                'price' => $item['price'],
                'quantity' => $item['quantity']
            ));
        }

        // build parameters
        $param['sale'] = $post['sale'];
        $param['cart'] = $temp;

        // remove empty values
        if(strlen($param['sale']['datetime']) === 0)
            unset($param['sale']['datetime']);

        // TC: success testing
        // $data['success'] = true;
        // $data['content'] = $param;

        $query = $this->sale->create($param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function request_update() {
        $post['sale'] = $this->input->post('info');
        $post['products'] = $this->input->post('cart');
        
        // arrange product fields
        $temp = array();
        foreach ($post['products'] as $product) {
            array_push($temp, array(
                'sale' => $post['sale']['id'],
                'product' => $product['id'],
                'price' => $product['price'],
                'quantity' => $product['quantity']
            ));
        }

        // build parameters
        $param['sale'] = $post['sale'];
        $param['products'] = $temp;

        // remove empty values
        if(strlen($param['sale']['datetime']) === 0)
            unset($param['sale']['datetime']);

        if(strlen(str_replace(' ', '', $param['sale']['note'])) === 0)
            unset($param['sale']['note']);

        // TC: success testing
        // $data['success'] = true;
        // $data['content'] = $param;

        $query = $this->sale->update($param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function request_detail() {
        $sid = $this->input->post('sid');
        $query = $this->sale->detail($sid);

        // format datetime
        $temp = explode(':', $query['sale']['datetime']);
        unset($temp[count($temp) -1]);
        $query['sale']['datetime'] = str_replace(' ', 'T', implode(':', $temp));

        $data['success'] = sizeof($query['sale']) !== 0;
        $data['info'] = $query['sale'];
        $data['cart'] = $query['products'];
        $data['setting'] = $this->config('external');

        echo json_encode($data);
    }

    private function request_view() {
        $sid = $this->input->post('sid');
        $query = $this->sale->view($sid);

        $data['success'] = $query['info'] != false;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function request_return() {
        $sales = $this->input->post('sales');

        $counter = 0;
        foreach ($sales as $sid) {
            $query = $this->sale->return($sid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($sales);
        echo json_encode($data);
    }

    private function request_pay() {
        $post['sale'] = $this->input->post('sale');
        $post['amount'] = $this->input->post('amount');

        $param['sale'] = $post['sale'];
        $param['amount'] = $post['amount'];
        $param['operator'] = 2; // this should be current user

        $query = $this->sale->pay($param);
        $data['success'] = $query;
        // $data['success'] = true;

        // for debugging
        // $data['debug'] = $param;

        echo json_encode($data);
    }
}
