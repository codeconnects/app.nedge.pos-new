<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotations extends MY_Controller {

    public function _remap($method, $params = array()) {
        if(method_exists($this, $method))
            $this->$method($params);
        else
            $this->error_404();
    }

	private function index() {
        $this->template('quotations');
    }
}
