<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // load dependencies
        $this->load->model('purchase_model', 'purchase');

        // check if logged in user exist
        if(!$this->session->has_userdata('id'))
            header('Location: '. base_url());
    }

    public function _remap($method, $params = array()) {
        $address = array(
            'index' => 'purchases',
            'create' => 'purchases/create',
            'update' => 'purchases/update',
            'import' => 'purchases/import'
        );

        if(method_exists($this, $method)) {
            $this->$method();
        }else {
            $location = isset($address[$method]) ? $address[$method] : null;
            if($location)
                $this->template($location);
            else
                $this->error_404();
        }
    }

    private function request_load() {
        // get post data
        $post['keyword'] = $this->input->post('keyword');
        $post['storage'] = $this->input->post('warehouse');
        $post['pagenum'] = $this->input->post('pagenum');

        // setup query parameters
        $param['search'] = $post['keyword'];
        $param['whouse'] = $post['storage'];
        $param['maxrow'] = $this->config()['system']['maxrow'];
        $param['offset'] = ($post['pagenum'] -1) * $param['maxrow'];

        $query = $this->purchase->populate($param);
        $count = ceil($query['count'] / $param['maxrow']);

        $data['success'] = true;
        $data['content'] = $query['data'];
        $data['setting'] = $this->config('external');
        $data['pageset']['current'] = intval($post['pagenum']);
        $data['pageset']['maxpage'] = $count;

        // for debugging
        // $data['debug'] = $query;

        echo json_encode($data);
    }

    private function request_payment() {
        $pid = $this->input->post('pid');
        $query = $this->purchase->payments($pid);

        $data['success'] = true;
        $data['content'] = $query;

        echo json_encode($data);
    }

    private function request_create() {
        $post['purchase'] = $this->input->post('info');
        $post['products'] = $this->input->post('cart');
        $post['purchase']['id'] = $this->purchase->generate_id();
        
        // arrange product fields
        $temp = array();
        foreach ($post['products'] as $product) {
            array_push($temp, array(
                'purchase' => $post['purchase']['id'],
                'product' => $product['id'],
                'price' => $product['price'],
                'quantity' => $product['quantity']
            ));
        }

        // build parameters
        $param['purchase'] = $post['purchase'];
        $param['products'] = $temp;

        // remove empty values
        if(strlen($param['purchase']['datetime']) === 0)
            unset($param['purchase']['datetime']);

        if(strlen(str_replace(' ', '', $param['purchase']['note'])) === 0)
            unset($param['purchase']['note']);

        // TC: success testing
        // $data['success'] = true;
        // $data['content'] = $param;

        $query = $this->purchase->create($param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function request_update() {
        $post['purchase'] = $this->input->post('info');
        $post['products'] = $this->input->post('cart');
        
        // arrange product fields
        $temp = array();
        foreach ($post['products'] as $product) {
            array_push($temp, array(
                'purchase' => $post['purchase']['id'],
                'product' => $product['id'],
                'price' => $product['price'],
                'quantity' => $product['quantity']
            ));
        }

        // build parameters
        $param['purchase'] = $post['purchase'];
        $param['products'] = $temp;

        // remove empty values
        if(strlen($param['purchase']['datetime']) === 0)
            unset($param['purchase']['datetime']);

        if(strlen(str_replace(' ', '', $param['purchase']['note'])) === 0)
            unset($param['purchase']['note']);

        // TC: success testing
        // $data['success'] = true;
        // $data['content'] = $param;

        $query = $this->purchase->update($param);
        $data['success'] = $query;

        echo json_encode($data);
    }

    private function request_detail() {
        $pid = $this->input->post('pid');
        $query = $this->purchase->detail($pid);

        // format datetime
        $temp = explode(':', $query['purchase']['datetime']);
        unset($temp[count($temp) -1]);
        $query['purchase']['datetime'] = str_replace(' ', 'T', implode(':', $temp));

        $data['success'] = sizeof($query['purchase']) !== 0;
        $data['info'] = $query['purchase'];
        $data['cart'] = $query['products'];
        $data['setting'] = $this->config('external');

        echo json_encode($data);
    }

    private function request_view() {
        $pid = $this->input->post('pid');
        
        $query = $this->purchase->view($pid);
        $query['history'] = $this->purchase->history($pid);
        $query['adjustment'] = $this->purchase->adjustment($pid);

        $data['success'] = sizeof($query['purchase']) !== 0;
        $data['content']['purchase'] = $query['purchase'];
        $data['content']['products'] = $query['products'];
        $data['content']['adjustments'] = $query['adjustment'];
        $data['content']['history'] = $query['history'];

        echo json_encode($data);
    }

    private function request_history() {
        $pid = $this->input->post('pid');
        $query = $this->purchase->historyPurchase($pid);

        $data['success'] = sizeof($query) !== 0;
        $data['content'] = $query;
        $data['setting'] = array(
            'accounting' => $this->setting('accounting')
        );

        echo json_encode($data);
    }

    private function request_receive() {
        $purchases = $this->input->post('purchases');

        $counter = 0;
        foreach ($purchases as $pid) {
            $query = $this->purchase->receive($pid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($purchases);
        echo json_encode($data);
    }

    private function request_return() {
        $purchases = $this->input->post('purchases');

        $counter = 0;
        foreach ($purchases as $pid) {
            $query = $this->purchase->return($pid);
            if($query) $counter++;
        }

        $data['success'] = $counter === sizeof($purchases);
        echo json_encode($data);
    }

    private function request_pay() {
        $post['purchase'] = $this->input->post('purchase');
        $post['amount'] = $this->input->post('amount');

        $param['purchase'] = $post['purchase'];
        $param['amount'] = $post['amount'];
        $param['operator'] = 2; // this should be current user

        $query = $this->purchase->pay($param);
        $data['success'] = $query;
        // $data['success'] = true;

        // for debugging
        // $data['debug'] = $param;

        echo json_encode($data);
    }
}
