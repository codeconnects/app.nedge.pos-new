<?php

class People_model extends CI_Model {

	public function load($group = 'supplier') {
		$gid = array(
			'biller' => 1,
			'supplier' => 2,
			'customer' => 3
		);

		$this->db->select('id, lastname, firstname');
		$this->db->from('people');
		$this->db->where('group', $gid[$group]);
		$this->db->order_by('lastname', 'ASC');

		return $this->db->get()->result_array();
	}

	public function populate($param) {
		$this->db->from('people');
		// do not add if search is empty
		if($param['search'] != '') {
			$this->db->group_start();
			$this->db->like('people.firstname', $param['search'], 'after');
			$this->db->or_like('people.lastname', $param['search'], 'after');
			$this->db->group_end();

			$param['limit']['maxrow'] = 20;
			$param['limit']['offset'] = 0;
		}

		$this->db->where('people.group', 2);
		if($param) $this->db->limit($param['limit']['maxrow'], $param['limit']['offset']);

		$query['data'] = $this->db->get()->result_array();
		$query['count'] = $this->db->count_all();

		return $query;
	}
}

?>