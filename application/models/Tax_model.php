<?php

class Tax_model extends CI_Model {

	public function load() {
		$this->db->select('id, name');
		$this->db->from('tax_rates');

		return $this->db->get()->result_array();
	}

	public function populate($param) {
		$this->db->from('tax_rates');
		// do not add if search is empty
		if($param['search'] != '') {
			$this->db->like('tax_rates.name', $param['search'], 'after');
			$this->db->limit(20);

			$param['limit']['maxrow'] = 20;
			$param['limit']['offset'] = 0;
		}

		if($param) $this->db->limit($param['limit']['maxrow'], $param['limit']['offset']);

		$query['data'] = $this->db->get()->result_array();
		$query['count'] = $this->db->count_all('tax_rates');

		return $query;
	}
}

?>