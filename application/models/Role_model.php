<?php

class Role_model extends CI_Model {

	public function load() {
		$this->db->select('id, name');
		$this->db->from('user_role');
		$this->db->where('id !=', 1);
		$this->db->order_by('name', 'ASC');

		return $this->db->get()->result_array();
	}

	public function populate($param) {
		
	}

	public function create() {

	}

	public function update() {

	}

	public function detail($uid) {

	}

	public function view($uid) {

	}

	public function delete($uid) {

	}
	
}

?>