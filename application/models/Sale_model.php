<?php

class Sale_model extends CI_Model {

	// sale functions
	public function populate($param) {
		$this->db->select('
			sales.id,
			sales.reference,
			DATE_FORMAT(sales.datetime, "%b %d %Y %I:%i %p") AS datetime,
			CONCAT(pc.firstname, " ", pc.lastname) AS customer,
			FORMAT(
				(SELECT SUM(sales_detail.price * sales_detail.quantity)
				FROM sales_detail
					WHERE sales_detail.sale = sales.id
				) - (
				(SELECT SUM(sales_detail.price * sales_detail.quantity)
				FROM sales_detail
					WHERE sales_detail.sale = sales.id
				) * (sales.discount / 100)), 2) AS total,
			CONCAT(pb.firstname, " ", pb.lastname) AS biller,
			sales.status');

		$this->db->from('sales');
		$this->db->join('people AS pc', 'pc.id = sales.customer');
		$this->db->join('people AS pb', 'pb.id = sales.operator');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('sales.reference', $param['search'], 'after');
			$this->db->or_like('pc.firstname', $param['search'], 'after');
			$this->db->or_like('pc.lastname', $param['search'], 'after');
			$this->db->or_like('pc.email', $param['search'], 'after');
			$this->db->or_like('pc.company', $param['search'], 'after');
			$this->db->or_like('pb.firstname', $param['search'], 'after');
			$this->db->or_like('pb.lastname', $param['search'], 'after');
			$this->db->or_like('pb.email', $param['search'], 'after');
			$this->db->or_like('pb.company', $param['search'], 'after');
			$this->db->group_end();

			// set limit
			$param['maxrow'] = 20;
			$param['offset'] = 0;
		}

		$this->db->where('sales.warehouse', $param['whouse']);
		$query['count'] = $param['search'] != "" ? 0 : $this->db->count_all_results('', FALSE);

		$this->db->limit($param['maxrow'], $param['offset']);
		$query['data'] = $this->db->get()->result_array();

		return $query;
	}

	public function create($param) {
		$query = $this->db->insert('sales', $param['sale']);
		if($query) {
			foreach ($param['cart'] as $item) {
				$this->db->insert('sales_detail', $item);

				// update stock quantity
				$this->db->set('quantity', 'quantity-'.$item['quantity'], FALSE);
				$this->db->where('purchase', $item['purchase']);
				$this->db->where('product', $item['product']);
				$this->db->update('stocks');
			}
		}

		return $query;
	}

	public function update($param) {
		$sid = $param['sales']['id'];
		$status = $param['sales']['status'];

		unset($param['sales']['id']);
		unset($param['sales']['status']);

		// update sales info
		$this->db->where('id', $sid);
		$query = $this->db->update('sales', $param['sales']);

		if($status == 'pending') {
			// remove existing record
			$this->db->where('sales', $sid);
			$this->db->delete('sales_detail');

			foreach ($param['products'] as $product)
				$this->db->insert('sales_detail', $product);
		}

		return $query;
	}

	public function detail($sid) {
		// get sales info
		$query = $this->db->get_where('sales', array('id' => $sid));
		$result['sales'] = $query->result_array()[0];

		// get products by sales id [$sid]
		$table = $result['sales']['status'] == 'returned' ? 'sales_return' : 'sales_detail';

		$this->db->select('
			product.id,
			product.code,
			product.name_display,
			category.name AS category,
			'.$table.'.price,
			'.$table.'.quantity'
		);

		$this->db->from($table);
		$this->db->join('product', 'product.id = '. $table .'.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->where('sales', $sid);

		$result['products'] = $this->db->get()->result_array();

		return $result;
	}

	public function view($sid) {
		// get sales info
		$this->db->select('
			DATE_FORMAT(sales.datetime, "%b %d %Y %I:%i %p") AS datetime,
			sales.reference,
			warehouse.name AS warehouse,
			CONCAT(pc.firstname, " ", pc.lastname) AS customer,
			FORMAT(
				(SELECT SUM(sales_detail.price * sales_detail.quantity)
				FROM sales_detail
					WHERE sales_detail.sale = sales.id
				) - (
				(SELECT SUM(sales_detail.price * sales_detail.quantity)
				FROM sales_detail
					WHERE sales_detail.sale = sales.id
				) * (sales.discount / 100)), 2) AS total,
			sales.discount,
			sales.paid,
			sales.method,
			CONCAT(pb.firstname, " ", pb.lastname) AS biller'
		);

		$this->db->from('sales');
		$this->db->join('warehouse', 'warehouse.id = sales.warehouse');
		$this->db->join('people AS pc', 'pc.id = sales.customer');
		$this->db->join('people AS pb', 'pb.id = sales.operator');
		$this->db->where('sales.id', $sid);

		$query = $this->db->get();
		$result['info'] = $query->result_array()[0];

		$this->db->select('
			product.code,
			product.name_display,
			sales_detail.price,
			sales_detail.quantity,
			units.name AS unit'
		);

		$this->db->from('sales_detail');
		$this->db->join('product', 'product.id = sales_detail.product');
		$this->db->join('units', 'units.id = product.unit');
		$this->db->where('sales_detail.sale', $sid);

		$result['cart'] = $this->db->get()->result_array();

		return $result;
	}

	public function return($sid) {
		$this->db->set('status', 'returned');
		$this->db->where('id', $sid);
		$query = $this->db->update('sales');
		
		if($query) {
			// restore stock quantities
			$products = $this->db->get_where('sales_detail', array('sale' => $sid));
			foreach ($products->result_array() as $product) {
				$this->db->set('quantity', 'quantity+'.$product['quantity'], FALSE);
				$this->db->where('purchase', $product['purchase']);
				$this->db->where('product', $product['product']);

				$query = $this->db->update('stocks');
			}
		}

		return $query;
	}

	// payment functions
	public function payments($sid) {
		$this->db->select('
			sales.id,
			(SELECT SUM(price * quantity)
				FROM sales_detail
					WHERE sale = sales.id
				) AS total,
			sales.discount,
			((SELECT IFNULL(SUM(amount), 0)
				FROM sales_payment
					WHERE sale = sales.id
				) + sales.paid) AS paid');

		$this->db->from('sales');
		$this->db->where('sales.id', $sid);

		$query = $this->db->get()->result_array()[0];

		if($query) {
			$this->db->select('
				DATE_FORMAT(sales_payment.datetime, "%b %d %Y %I:%i %p") AS datetime,
				sales_payment.amount,
				CONCAT(people.firstname, " ", people.lastname) AS operator');

			$this->db->from('sales_payment');
			$this->db->join('people', 'people.id = sales_payment.operator');
			$this->db->where('sales_payment.sale', $sid);

			$query['history'] = $this->db->get()->result_array();
		}

		return $query;
	}

	public function pay($param) {
		$query = $this->db->insert('sales_payment', $param);
		return $query;
	}

	// miscellaneous
	public function generate_id() {
		$this->db->select('id');
		$this->db->from('sales');
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);

		$query = $this->db->get()->result_array();
		return  $query ? intval($query[0]['id']) +1 : 1;
	}
}

?>