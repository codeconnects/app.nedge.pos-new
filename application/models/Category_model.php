<?php

class Category_model extends CI_Model {

	public function load() {
		$this->db->select('id, name');
		$this->db->from('category');
		$this->db->where('is_active', 1);

		return $this->db->get()->result_array();
	}

	public function delete($cId) {
		$this->db->set('is_active', 0);
		$this->db->where('id', $cId);

		return $this->db->update('category');
	}

	public function loadSub() {
		$this->db->select('id, name');
		$this->db->from('category_sub');
		$this->db->where('is_active', 1);

		return $this->db->get()->result_array();
	}

	public function deleteSub($cId) {
		$this->db->set('is_active', 0);
		$this->db->where('id', $cId);

		return $this->db->update('category_sub');
	}
	
}

?>