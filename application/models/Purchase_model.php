<?php

class Purchase_model extends CI_Model {

	public function populate($param) {
		$this->db->select('
			purchase.id,
			purchase.reference,
			DATE_FORMAT(purchase.datetime, "%b %d %Y %I:%i %p") AS datetime,
			CONCAT(people.firstname, " ", people.lastname) AS supplier,
			purchase.status,
			FORMAT((
				SELECT SUM(purchase_detail.price * purchase_detail.quantity)
				FROM purchase_detail
					WHERE purchase_detail.purchase = purchase.id
				), 2) AS total');

		$this->db->from('purchase');
		$this->db->join('people', 'people.id = purchase.supplier');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('purchase.reference', $param['search'], 'after');
			$this->db->group_end();

			// set limit
			$param['maxrow'] = 20;
			$param['offset'] = 0;
		}

		$this->db->where('purchase.warehouse', $param['whouse']);
		$query['count'] = $param['search'] != "" ? 0 : $this->db->count_all_results('', FALSE);

		$this->db->limit($param['maxrow'], $param['offset']);
		$query['data'] = $this->db->get()->result_array();

		return $query;
	}

	public function create($param) {
		$query = $this->db->insert('purchase', $param['purchase']);
		if($query) {
			foreach ($param['products'] as $product)
				$this->db->insert('purchase_detail', $product);
		}

		return $query;
	}

	public function update($param) {
		$pid = $param['purchase']['id'];
		$status = $param['purchase']['status'];

		unset($param['purchase']['id']);
		unset($param['purchase']['status']);

		// update purchase info
		$this->db->where('id', $pid);
		$query = $this->db->update('purchase', $param['purchase']);

		if($status == 'pending') {
			// remove existing record
			$this->db->where('purchase', $pid);
			$this->db->delete('purchase_detail');

			foreach ($param['products'] as $product)
				$this->db->insert('purchase_detail', $product);
		}

		return $query;
	}

	public function detail($pid) {
		// get purchase info
		$query = $this->db->get_where('purchase', array('id' => $pid));
		$result['purchase'] = $query->result_array()[0];

		// get products by purchase id [$pid]
		$table = $result['purchase']['status'] == 'returned' ? 'purchase_return' : 'purchase_detail';

		$this->db->select('
			product.id,
			product.code,
			product.name_display,
			category.name AS category,
			'.$table.'.price,
			'.$table.'.quantity'
		);

		$this->db->from($table);
		$this->db->join('product', 'product.id = '. $table .'.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->where('purchase', $pid);

		$result['products'] = $this->db->get()->result_array();

		return $result;
	}

	public function view($pid) {
		// get purchase info
		$this->db->select('
			purchase.reference,
			DATE_FORMAT(purchase.datetime, "%b %d %Y %I:%i %p") AS datetime,
			warehouse.name AS warehouse,
			CONCAT(people.firstname, " ", people.lastname) AS supplier,
			tax_rates.name AS tax,
			purchase.discount,
			purchase.note,
			purchase.status'
		);

		$this->db->from('purchase');
		$this->db->join('warehouse', 'warehouse.id = purchase.warehouse');
		$this->db->join('people', 'people.id = purchase.supplier');
		$this->db->join('tax_rates', 'tax_rates.id = purchase.tax');
		$this->db->where('purchase.id', $pid);

		$query = $this->db->get();
		$result['purchase'] = $query->result_array()[0];

		// get products by purchase id [$pid]
		$table = $result['purchase']['status'] == 'returned' ? 'purchase_return' : 'purchase_detail';

		$this->db->select('
			product.id,
			product.code,
			product.name_display,
			category.name AS category,
			'. $table .'.price,
			'. $table .'.quantity'
		);

		$this->db->from($table);
		$this->db->join('product', 'product.id = '. $table .'.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->where('purchase', $pid);

		$result['products'] = $this->db->get()->result_array();

		return $result;
	}

	public function adjustment($pid) {
		$this->db->select('
			DATE_FORMAT(purchase_adjust.datetime, "%b %d %Y %I:%i %p") AS datetime,
			product.name_display,
			category.name AS category,
			purchase_adjust.quantity'
		);

		$this->db->from('purchase_adjust');
		$this->db->join('product', 'product.id = purchase_adjust.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->where('purchase', $pid);
		$this->db->order_by('purchase_adjust.datetime', 'DESC');

		$query = $this->db->get();
		return $query->result_array();
	}

	public function history($pid) {
		$this->db->select('
			product.id,
			product.code,
			product.name_display,
			category.name AS category,
			purchase_detail.price,
			purchase_detail.quantity'
		);

		$this->db->from('purchase_detail');
		$this->db->join('product', 'product.id = purchase_detail.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->where('purchase', $pid);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function receive($pid) {
		// update purchase status
		$this->db->set('status', 'received');
		$this->db->where('id', $pid);
		$query = $this->db->update('purchase');
		
		if($query) {
			// add products to stocks
			$products = $this->db->get_where('purchase_detail', array('purchase' => $pid));
			foreach ($products->result_array() as $product) {
				unset($product['id']);
				$this->db->insert('stocks', $product);
			}
		}
		
		return $query;
	}

	public function return($pid) {
		// update purchase status
		$this->db->set('status', 'returned');
		$this->db->where('id', $pid);
		$query = $this->db->update('purchase');

		if($query) {
			$status = $this->db->get_where('purchase', array('id' => $pid));
			$status = $status->result_array()[0]['status'];
			$table = $status == 'pending' ? 'purchase_detail' : 'stocks';

			// add products to stocks
			$products = $this->db->get_where($table, array('purchase' => $pid));
			foreach ($products->result_array() as $product) {
				unset($product['id']);
				if($table == 'stocks')
					unset($product['is_expired']);
				
				$this->db->insert('purchase_return', $product);
			}

			// delete products from stocks
			if($table == 'stocks') {
				$this->db->where('purchase', $pid);
				$this->db->update('stocks', array('quantity' => 0));
			}
		}
		
		return $query;
	}

	// payment functions
	public function payments($pid) {
		$this->db->select('
			purchase.id,
			(SELECT SUM(price * quantity)
				FROM purchase_detail
					WHERE purchase = purchase.id
				) AS total,
			purchase.discount,
			(SELECT IFNULL(SUM(amount), 0)
				FROM purchase_payment
					WHERE purchase = purchase.id
				) AS paid');

		$this->db->from('purchase');
		$this->db->where('purchase.id', $pid);

		$query = $this->db->get()->result_array()[0];

		if($query) {
			$this->db->select('
				DATE_FORMAT(purchase_payment.datetime, "%b %d %Y %I:%i %p") AS datetime,
				purchase_payment.amount,
				CONCAT(people.firstname, " ", people.lastname) AS operator');

			$this->db->from('purchase_payment');
			$this->db->join('people', 'people.id = purchase_payment.operator');
			$this->db->where('purchase_payment.purchase', $pid);

			$query['history'] = $this->db->get()->result_array();
		}

		return $query;
	}

	public function pay($param) {
		$query = $this->db->insert('purchase_payment', $param);
		return $query;
	}

	// miscellaneous
	public function generate_id() {
		$this->db->select('id');
		$this->db->from('purchase');
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);

		$query = $this->db->get()->result_array();
		return  $query ? intval($query[0]['id']) +1 : 1;
	}
}

?>