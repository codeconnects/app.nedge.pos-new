<?php

class Stock_model extends CI_Model {

	public function populate($param) {
		$this->db->select('
			stocks.id,
			stocks.product,
			stocks.purchase,
			purchase.reference,
			product.name_display,
			category.name AS category,
			brands.name AS brand,
			units.name AS unit,
			stocks.quantity,
			stocks.price
		');

		$this->db->from('stocks');
		$this->db->join('product', 'product.id = stocks.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->join('category_sub', 'category_sub.id = product.category_sub');
		$this->db->join('brands', 'brands.id = product.brand');
		$this->db->join('units', 'units.id = product.unit');
		$this->db->join('purchase', 'purchase.id = stocks.purchase');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('purchase.reference', $param['search'], 'after');
			$this->db->or_like('product.code', $param['search'], 'after');
			$this->db->or_like('product.name', $param['search'], 'after');
			$this->db->or_like('product.name_display', $param['search'], 'after');
			$this->db->or_like('category.name', $param['search'], 'after');
			$this->db->or_like('brands.name', $param['search'], 'after');
			$this->db->group_end();

			// set limit
			$param['maxrow'] = 20;
			$param['offset'] = 0;
		}

		if($param['oversell'] !== 2)
			$this->db->where('stocks.quantity >', 0);

		$this->db->where('is_expired', $param['expire']);
		$this->db->where('purchase.warehouse', $param['whouse']);
		$this->db->where('purchase.status', 'received');
		$query['count'] = $param['search'] != "" ? 0 : $this->db->count_all_results('', FALSE);

		$this->db->limit($param['maxrow'], $param['offset']);
		$query['data'] = $this->db->get()->result_array();

		return $query;
	}

	public function view($sid) {
		$this->db->select('
			product.code,
			product.name_display,
			category.name AS category,
			brands.name AS brand,
			units.name AS unit,
			product.cb_parent,
			IFNULL((SELECT temp.name_display
				FROM product AS temp
				WHERE temp.id = product.cb_parent LIMIT 1), "No Parent") AS parent,
			stocks.price,
			stocks.quantity,
			purchase.reference,
			DATE_FORMAT(purchase.datetime, "%b %d %Y %I:%i %p") AS datetime,
			warehouse.name AS warehouse,
			CONCAT(people.firstname, " ", people.lastname) AS supplier,
			tax_rates.name AS tax,
			purchase.discount,
			purchase.note'
		);

		$this->db->from('stocks');
		$this->db->join('product', 'product.id = stocks.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->join('brands', 'brands.id = product.brand');
		$this->db->join('units', 'units.id = product.unit');
		$this->db->join('purchase', 'purchase.id = stocks.purchase');
		$this->db->join('warehouse', 'warehouse.id = purchase.warehouse');
		$this->db->join('people', 'people.id = purchase.supplier');
		$this->db->join('tax_rates', 'tax_rates.id = purchase.tax');

		$this->db->where('stocks.id', $sid);
		$query = $this->db->get();

		return $query->result_array()[0];
	}

	public function update($param) {
		$this->db->where('id', $param['sid']);
		$query = $this->db->update('stocks', array('price' => $param['price']));

		return $query;
	}

	public function adjust($param) {
		// get original product info
		$this->db->where('purchase', $param['purchase']);
		$this->db->where('product', $param['product']);
		$info = $this->db->get('purchase_detail')->result_array()[0];

		if(count($info) > 0) {
			// update product quantity
			$this->db->where('purchase', $param['purchase']);
			$this->db->where('product', $param['product']);
			$temp = $this->db->update('purchase_detail', array('quantity' => $param['quantity']));

			if($temp) {
				// update stock quantity
				$this->db->where('purchase', $param['purchase']);
				$this->db->where('product', $param['product']);
				$temp = $this->db->update('stocks', array('quantity' => $param['quantity']));
			}

			if($temp) {
				// insert info to purchase_adjust
				unset($info['price']);
				$query = $this->db->insert('purchase_adjust', $info);
			}
		}

		return $query;
	}

	public function expire($sid) {
		$this->db->where('id', $sid);
		$query = $this->db->update('stocks', array('is_expired' => 1));

		return $query;
	}

	public function restore($sid) {
		$this->db->where('id', $sid);
		$query = $this->db->update('stocks', array('is_expired' => 0));

		return $query;
	}
	
}

?>