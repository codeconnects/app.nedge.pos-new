<?php

class Warehouse_model extends CI_Model {

	public function load() {
		$this->db->select('id, name');
		$this->db->from('warehouse');
		$this->db->order_by('name', 'ASC');

		return $this->db->get()->result_array();
	}

	public function populate($param) {
		$this->db->from('warehouse');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('warehouse.code', $param['search'], 'after');
			$this->db->group_end();
			
			// set limit
			$param['maxrow'] = 20;
			$param['offset'] = 0;
		}

		$this->db->limit($param['maxrow'], $param['offset']);

		$query['data'] = $this->db->get()->result_array();
		$query['count'] = $param['search'] != "" ? 0 : $this->db->count_all('warehouse');

		return $query;
	}
	
}

?>