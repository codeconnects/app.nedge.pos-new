<?php

class Pos_model extends CI_Model {

	public function load($param) {
		$this->db->select('
			stocks.id,
			stocks.purchase,
			stocks.product,
			product.code,
			product.name_display,
			category.name AS category,
			brands.name AS brand,
			units.name AS unit,
			stocks.price,
			stocks.quantity');

		$this->db->from('stocks');
		$this->db->join('purchase', 'purchase.id = stocks.purchase');
		$this->db->join('product', 'product.id = stocks.product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->join('brands', 'brands.id = product.brand');
		$this->db->join('units', 'units.id = product.unit');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('purchase.reference', $param['search'], 'after');
			$this->db->or_like('product.code', $param['search'], 'after');
			$this->db->or_like('product.name', $param['search'], 'after');
			$this->db->or_like('product.name_display', $param['search'], 'after');
			$this->db->or_like('category.name', $param['search'], 'after');
			$this->db->or_like('brands.name', $param['search'], 'after');
			$this->db->group_end();
		}

		if($param['oversell'] !== 2)
			$this->db->where('stocks.quantity >', 0);

		$this->db->where('stocks.is_expired', 0);
		$this->db->where('purchase.warehouse', $param['warehouse']);
		$this->db->where('purchase.status', 'received');

		$this->db->order_by('purchase.datetime', $param['method']);
		$this->db->limit(20, 0);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function loadCustomers($param) {
		$this->db->select('id, firstname, lastname');
		$this->db->from('people');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('firstname', $param['search'], 'after');
			$this->db->or_like('lastname', $param['search'], 'after');
			$this->db->or_like('city', $param['search'], 'after');
			$this->db->or_like('state', $param['search'], 'after');
			$this->db->or_like('email', $param['search'], 'after');
			$this->db->or_like('company', $param['search'], 'after');
			$this->db->group_end();
		}

		$this->db->where('group', 3);
		$this->db->where('is_active', 1);

		$this->db->order_by('lastname', 'ASC');
		$this->db->limit(20, 0);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function suspended() {
		$this->db->select('
			suspended.id,
			suspended.reference,
			DATE_FORMAT(
				suspended.datetime,
				"%b %d %Y %I:%i %p"
			) AS datetime,
			CONCAT(pc.firstname, " ", pc.lastname) AS customer_name,
			CONCAT(po.firstname, " ", po.lastname) AS operator_name,
			(SELECT 
				CONCAT(COUNT(csd.stock) ," (", SUM(csd.quantity), ")")
			FROM
				suspended_detail AS csd 
			WHERE csd.suspended = suspended.id) AS cart,
			(SELECT 
				SUM(tsd.quantity * stocks.price) 
			FROM
				suspended_detail AS tsd 
			LEFT JOIN stocks 
			 	ON stocks.id = tsd.stock WHERE tsd.suspended = suspended.id) AS total');

		$this->db->from('suspended');
		$this->db->join('people AS pc', 'pc.id = suspended.customer');
		$this->db->join('people AS po', 'po.id = suspended.operator');
		$this->db->where('suspended.is_completed', 0);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function suspend($param) {
		// get cart items
		$detail = $param['detail'];
		unset($param['detail']);

		$query = $this->db->insert('suspended', $param);
		if($query) {
			foreach ($detail as $product) {
				$product['suspended'] = $param['id'];
				$this->db->insert('suspended_detail', $product);
			}
		}

		return $query;
	}

	public function detail($oid) {
		$this->db->select('
			suspended.id,
			suspended.discount,
			suspended.method,
			suspended.paid,
			suspended.customer AS cid,
			CONCAT(people.firstname, " ", people.lastname) AS cname');

		$this->db->from('suspended');
		$this->db->join('people', 'people.id = suspended.customer');
		$this->db->where('suspended.id', $oid);

		$query = $this->db->get()->result_array()[0];
		if(sizeof($query) !== 0) {
			$this->db->select('
				suspended_detail.stock,
				stocks.purchase,
				stocks.product,
				product.code,
				product.name_display,
				units.name AS unit,
				stocks.price,
				suspended_detail.quantity');

			$this->db->from('suspended_detail');
			$this->db->join('stocks', 'stocks.id = suspended_detail.stock');
			$this->db->join('product', 'product.id = stocks.product');
			$this->db->join('units', 'units.id = product.unit');
			$this->db->where('suspended_detail.suspended', $oid);

			$query['products'] = $this->db->get()->result_array();
		}

		return $query;
	}

	public function payment($param) {
		$query = $this->db->insert('sales', $param['info']);
		if($query) {
			foreach ($param['orders'] as $item) {
				$temp = array(
					'sale' => $param['info']['id'],
					'purchase' => $item['purchase'],
					'product' => $item['product'],
					'price' => $item['price'],
					'quantity' => $item['quantity']
				);

				$this->db->insert('sales_detail', $temp);

				// update stock quantity
				$this->db->set('quantity', 'quantity-'.$item['quantity'], FALSE);
				$this->db->where('purchase', $item['purchase']);
				$this->db->where('product', $item['product']);
				$this->db->update('stocks');
			}

			if($param['order']) {
				$this->db->where('id', $param['order']);
				$this->db->update('suspended', array('is_completed' => 1));
			}
		}

		return $query;
	}

	public function generate_id($table = 'suspended') {
		$this->db->select('id');
		$this->db->from($table);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);

		$query = $this->db->get()->result_array();
		return  $query ? intval($query[0]['id']) +1 : 1;
	}

}

?>