<?php

class Product_model extends CI_Model {

	public function populate($param) {
		$this->db->select('
			product.id,
			product.code,
			product.name_display,
			category.name AS category,
			brands.name AS brand,
			product.is_hidden
		');

		$this->db->from('product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->join('category_sub', 'category_sub.id = product.category_sub');
		$this->db->join('brands', 'brands.id = product.brand');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('product.code', $param['search'], 'after');
			$this->db->or_like('product.name', $param['search'], 'after');
			$this->db->or_like('product.name_display', $param['search'], 'after');
			$this->db->or_like('category.name', $param['search'], 'after');
			$this->db->or_like('brands.name', $param['search'], 'after');
			$this->db->group_end();
			
			// if request not from products
			if($param['source'])
				$this->db->where('product.is_hidden', 0);

			// set limit
			$param['maxrow'] = 20;
			$param['offset'] = 0;
		}

		$this->db->limit($param['maxrow'], $param['offset']);

		$query['data'] = $this->db->get()->result_array();
		$query['count'] = $param['search'] != "" ? 0 : $this->db->count_all('product');

		return $query;
	}

	public function load($pid) {
		$this->db->select('
			product.*,
			IFNULL((
				SELECT name_display
					FROM product AS cbp
					WHERE cbp.id = product.cb_parent), "") AS parent');

		$query = $this->db->get_where('product', array('id' => $pid));
		$query = $query->result_array();

		return sizeof($query) > 0 ? $query[0] : [];
	}

	public function create($param) {
		$query = $this->db->insert('product', $param['data']);
		return $query;
	}

	public function update($param) {
		$this->db->where('id', $param['pid']);
		$query = $this->db->update('product', $param['data']);

		return $query;
	}

	public function view($pid) {
		$this->db->select('
			product.code,
			product.code_hsn,
			product.name,
			product.name_display,
			product.slug,
			category.name AS category,
			category_sub.name AS category_sub,
			brands.name AS brand,
			units.name AS unit,
			IFNULL(
				(SELECT name_display
					FROM product AS cbp
					WHERE cbp.id = product.cb_parent), "None") AS cb_parent,
			product.cb_quantity,
			product.image,
			product.description,
			product.product_type,
			product.barcode_type,
			product.is_tracked,
			product.trigger');

		$this->db->from('product');
		$this->db->join('category', 'category.id = product.category');
		$this->db->join('category_sub', 'category_sub.id = product.category_sub');
		$this->db->join('brands', 'brands.id = product.brand');
		$this->db->join('units', 'units.id = product.unit');
		$this->db->where('product.id', $pid);

		$query = $this->db->get();
		return $query->result_array()[0];
	}

	public function delete($pid) {
		$this->db->set('is_hidden', 1);
		$this->db->where('id', $pid);
		$query = $this->db->update('product');
		
		return $query;
	}

	public function generate_id() {
		$this->db->select('id');
		$this->db->from('product');
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);

		$query = $this->db->get()->result_array();
		return  $query ? intval($query[0]['id']) +1 : 1;
	}
}

?>