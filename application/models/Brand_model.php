<?php

class Brand_model extends CI_Model {

	public function load() {
		$this->db->select('id, name');
		$this->db->from('brands');
		$this->db->where('is_active', 1);

		return $this->db->get()->result_array();
	}

	public function delete($bId) {
		$this->db->set('is_active', 0);
		$this->db->where('id', $bId);

		return $this->db->update('brands');
	}
	
}

?>