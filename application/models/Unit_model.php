<?php

class Unit_model extends CI_Model {

	public function load() {
		$this->db->select('id, name');
		$this->db->from('units');
		$this->db->where('is_active', 1);

		return $this->db->get()->result_array();
	}

	public function delete($uId) {
		$this->db->set('is_active', 0);
		$this->db->where('id', $uId);

		return $this->db->update('units');
	}
	
}

?>