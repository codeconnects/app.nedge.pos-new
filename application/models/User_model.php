<?php

class User_model extends CI_Model {

	public function populate($param) {
		$this->db->select('
			user.id,
			CONCAT(user.lastname, ", ", user.firstname) AS name,
			user.email,
			user.gender,
			user.role AS role_id,
			user_role.name AS role,
			user.is_active
		');

		$this->db->from('user');
		$this->db->join('user_role', 'user_role.id = user.role');

		if($param['search'] != "") {
			$this->db->group_start();
			$this->db->like('user.id', $param['search'], 'after');
			$this->db->or_like('user.firstname', $param['search'], 'after');
			$this->db->or_like('user.lastname', $param['search'], 'after');
			$this->db->or_like('user.gender', $param['search'], 'after');
			$this->db->or_like('user.email', $param['search'], 'after');
			$this->db->or_like('user.mobile', $param['search'], 'after');
			$this->db->or_like('user_role.name', $param['search'], 'after');
			$this->db->group_end();

			// set limit
			$param['maxrow'] = 20;
			$param['offset'] = 0;
		}

		$this->db->where('user.role !=', 1);
		$query['count'] = $param['search'] != "" ? 0 : $this->db->count_all_results('', FALSE);

		$this->db->limit($param['maxrow'], $param['offset']);
		$this->db->order_by('user.lastname', 'ASC');

		$query['data'] = $this->db->get()->result_array();

		// query for roles
		$this->db->select('id, name');
		$this->db->from('user_role');
		$this->db->where('user_role.id !=', 1);

		$query['roles'] = $this->db->get()->result_array();

		return $query;
	}

	public function create($param) {
		$query = $this->db->insert('user', $param);
		return $query;
	}

	public function update($uid, $param) {
		$this->db->set($param);
		$this->db->where('id', $uid);

		$query = $this->db->update('user');
		return $query;
	}

	public function detail($uid) {
		$this->db->from('user');
		$this->db->where('user.id', $uid);

		$query = $this->db->get();
		return $query->result_array()[0];
	}

	public function view($uid) {
		$this->db->select('
			user_a.id,
			CONCAT(user_a.firstname, " ", user_a.lastname) AS name,
			user_a.gender,
			DATE_FORMAT(user_a.bday, "%M %d %Y") AS bday,
			user_a.email,
			user_a.mobile,
			user_role.name AS role,
			DATE_FORMAT(user_a.created_dt, "%b %d %Y %I:%i %p") AS created_dt,
			CONCAT(user_b.firstname, " ", user_b.lastname) AS created_by,
			user_a.is_active
		');

		$this->db->from('user AS user_a');
		$this->db->join('user_role', 'user_role.id = user_a.role');
		$this->db->join('user AS user_b', 'user_b.id = user_a.created_by');
		$this->db->where('user_a.id', $uid);

		$query = $this->db->get();
		return $query->result_array()[0];
	}

	public function delete($uid) {
		$this->db->set('is_active', 0);
		$this->db->where('id', $uid);
		$query = $this->db->update('user');
		
		return $query;
	}

	public function role($param) {
		$this->db->set('role', $param['role']);
		$this->db->where('id', $param['uid']);
		$query = $this->db->update('user');
		
		return $query;
	}

	// miscellaneous
	public function login($email) {
		$this->db->select('
			user.id,
			CONCAT(user.firstname, " ", user.lastname) AS name,
			user.gender,
			user.email,
			user.pass,
			user.role,
			user.is_active
		');

		$this->db->from('user');
		$this->db->where('user.email', $email);

		$query = $this->db->get()->result_array();
		return sizeof($query) !== 0? $query[0]: false;
	}
	
}

?>