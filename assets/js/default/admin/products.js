var approot = new Vue({
    el: '#approot',
    data: {
        // appaction variables
        search: '',

        // apptable variables
        products: [],
        pageinfo: {
            current: 1,
            maxpage: 0
        },

        // appmodal
        modal: '',
        vproduct: {},

        // miscellaneous
        settings: {},
        selected: [],
        current: {
            view: 0,
            process: 1
        },

        // process toggler
        isVisible: false,
        isProcess: false
    },
    methods: {
        // appaction methods
        gotoUpdate() {
            if(this.selected.length > 0) {
                let pid = this.selected[0];
                let href = window.location.href;

                window.location.href = href +'/update/'+ pid;
            }else
                swal('Hold on!', 'Select one item from the table.', 'warning');
        },

        // apptable methods
        populate() {
            let postdata = {
                keyword: this.search,
                pagenum: this.pageinfo.current
            };

            $.post('products/request_load', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.products = data.content;
                    this.settings = data.setting;
                    this.pageinfo = data.pageset;
                }
            });
        },

        select(event) {
            let target = event.target;
            let type = event.type;

            if(type === 'change') {
                this.selected = [];
                if(target.checked) {
                    this.products.forEach((product) => {
                        let pid = product.id;
                        this.selected.push(pid);
                    });
                }
            }else {
                if(target.localName !== 'input') {
                    let parent = target.parentElement;
                    let pid = parent.firstChild.children[0].value;

                    if(!parent.firstChild.children[0].checked)
                        this.selected.push(pid);
                    else {
                        let index = this.selected.findIndex(product => product === pid);
                        this.selected.splice(index, 1);
                    }
                }
            }
        },

        navigate(pagenum) {
            this.pageinfo.current = pagenum;
            this.selected = [];
            
            this.populate();
        },

        // appmodal methods
        startDelete() {
            this.isProcess = true;
            let postdata = { products: this.selected };
            $.post('products/request_delete', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.selected.forEach((pid) => {
                        let index = this.products.findIndex(product => product.id === pid);
                        this.products[index].is_hidden = 1;
                    });
                }else
                    swal('Wait a second...', 'Some products were not updated.', 'warning');

                this.selected = [];
                this.isVisible = false;
                this.isProcess = false;
            });
        },

        startPrint() {
            this.isProcess = true;
            let timer = setInterval(() => {
                this.current.process++;
                if(this.current.process > this.selected.length) {
                    clearInterval(timer);

                    this.current.process = 1;
                    this.selected = [];
                    this.isVisible = false;
                    this.isProcess = false;
                }
            }, 500);
        },

        startExport() {
            this.isProcess = true;
            let timer = setInterval(() => {
                this.current.process++;
                if(this.current.process > this.selected.length) {
                    clearInterval(timer);

                    this.current.process = 1;
                    this.selected = [];
                    this.isVisible = false;
                    this.isProcess = false;
                }
            }, 500);
        },

        restore(pid) {
            let index = this.selected.findIndex(sid => sid === pid);
            this.selected.splice(index, 1);

            if(this.selected.length === 0) {
                this.isVisible = false;
                this.isProcess = false;
            }
        },

        load() {
            let index = this.current.view;
            let pid = this.selected[index];

            $.post('products/request_view', { pid: pid }, (response) => {
                let data = JSON.parse(response);
                if(data.success) approot.vproduct = data.content;
            });
        },

        next() {
            if((this.current.view +1) < this.selected.length) {
                this.current.view++;
                this.load();
            }
        },

        prev() {
            if((this.current.view -1) >= 0) {
                this.current.view--;
                this.load();
            }
        },

        // miscealleous
        show(name) {
            if(this.selected.length > 0) {
                // dont load if processing
                if(!this.isProcess) {
                    this.modal = name;
                    if(name === 'detail') {
                        this.current.view = 0;
                        this.load();
                    }
                }

                // reset process mode
                this.isVisible = true;
            }else
                swal('Hold on!', 'Select at least one item from the table.', 'warning');
        }
    },
    components: {
        'app-pagination': pagination
    },
    computed: {
        selectedList() {
            let temp = [];
            this.selected.forEach((pid) => {
                let product = this.products.find(product => product.id === pid);
                temp.push(product);
            });

            return temp;
        }
    },
    created() {
        this.populate();
    }
});