var approot = new Vue({
    el: '#approot',
    data: {
        // appcontroller variables
        warehouses: [],
        warehouse: {
            id: 1,
            name: 'General Warehouse'
        },

        // appaction variables
        search: '',

        // apptable variables
        sales: [],
        pageinfo: {
            current: 1,
            maxpage: 0
        },

        // appmodal
        modal: '',
        detail: {
            info: {},
            cart: []
        },
        payments: {},

        // miscellaneous
        settings: {},
        selected: [],
        current: {
            view: 0,
            process: 1
        },

        // process toggler
        isVisible: false,
        isProcess: false
    },
    methods: {
        // appcontroller methods
        populateWH() {
            $.post('warehouses/request_load', (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.warehouses = data.content;
                    approot.warehouse = data.content[0];
                }
            });
        },

        selectWH(wid) {
            let index = this.warehouses.findIndex(warehouse => warehouse.id === wid);

            this.warehouse = this.warehouses[index];
            this.pageinfo.current = 1;
            this.selected = [];

            this.populate();
        },

        // appaction methods
        gotoUpdate() {
            if(this.selected.length > 0) {
                let sid = this.selected[0];
                let href = window.location.href;

                window.location.href = href +'/update/'+ sid;
            }else
                swal('Hold on!', 'Select one item from the table.', 'warning');
        },

        // apptable methods
        populate() {
            let postdata = {
                keyword: this.search,
                warehouse: this.warehouse.id,
                pagenum: this.pageinfo.current
            };

            $.post('sales/request_load', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.sales = data.content;
                    this.settings = data.setting;
                    this.pageinfo = data.pageset;
                }
            });
        },

        select(event) {
            let target = event.target;
            let type = event.type;

            if(type === 'change') {
                this.selected = [];
                if(target.checked) {
                    this.sales.forEach((sale) => {
                        let sid = sale.id;
                        this.selected.push(sid);
                    });
                }
            }else {
                if(target.localName !== 'input') {
                    let parent = target.parentElement;
                    let sid = parent.firstChild.children[0].value;

                    if(!parent.firstChild.children[0].checked)
                        this.selected.push(sid);
                    else {
                        let index = this.selected.findIndex(sale => sale === sid);
                        this.selected.splice(index, 1);
                    }
                }
            }
        },

        navigate(pagenum) {
            this.pageinfo.current = pagenum;
            this.selected = [];
            
            this.populate();
        },

        // appmodal methods
        loadPayment() {
            let sid = this.selected[0];
            $.post('sales/request_payment', { sid: sid }, (response) => {
                let data = JSON.parse(response);
                if(data.success)
                    this.payments = data.content;
            });
        },

        payment() {
            if(!isNaN(this.payments.cash) && this.change <= 0) {
                let cash = parseFloat(this.payments.cash);
                if(cash > 0) {
                    let postdata = {
                        sale: this.payments.id,
                        amount: this.payments.cash
                    };

                    $.post('sales/request_pay', postdata, (response) => {
                        let data = JSON.parse(response);
                        if(data.success) {
                            this.loadPayment();
                            this.payments.cash = '';
                        }else
                            swal('Oh noes!', 'Something went wrong while processing.', 'error');
                    });
                }else
                    swal('Hold on!', 'Cash tendered should be greater than 0.', 'warning');
            }
        },

        startReturn() {
            this.isProcess = true;

            let postdata = { sales: this.selected };
            $.post('sales/request_return', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.selected.forEach((sid) => {
                        let index = approot.sales.findIndex(sale => sale.id === sid);
                        approot.sales[index].status = 'returned';
                    });

                    // reset selected
                    approot.selected = [];
                }else
                    swal('Wait a second...', 'Some sales were not updated.', 'warning');

                this.selected = [];
                this.isVisible = false;
                this.isProcess = false;
            });
        },

        startPrint() {
            this.isProcess = true;
            let timer = setInterval(() => {
                this.current.process++;
                if(this.current.process > this.selected.length) {
                    clearInterval(timer);

                    this.current.process = 1;
                    this.selected = [];
                    this.isVisible = false;
                    this.isProcess = false;
                }
            }, 500);
        },

        startExport() {
            this.isProcess = true;
            let timer = setInterval(() => {
                this.current.process++;
                if(this.current.process > this.selected.length) {
                    clearInterval(timer);

                    this.current.process = 1;
                    this.selected = [];
                    this.isVisible = false;
                    this.isProcess = false;
                }
            }, 500);
        },

        restore(sid) {
            let index = this.selected.findIndex(sale => sale === sid);
            this.selected.splice(index, 1);

            if(this.selected.length === 0) {
                this.isVisible = false;
                this.isProcess = false;
            }
        },

        load() {
            let index = this.current.view;
            let sid = this.selected[index];

            $.post('sales/request_view', { sid: sid }, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.detail.info = data.content.info;
                    this.detail.cart = data.content.cart;
                }
            });
        },

        next() {
            if((this.current.view +1) < this.selected.length) {
                this.current.view++;
                this.load();
            }
        },

        prev() {
            if((this.current.view -1) >= 0) {
                this.current.view--;
                this.load();
            }
        },

        // miscealleous
        show(name) {
            if(name === 'return') {
                let temp = [];
                this.selected.forEach((sid) => {
                    let index = this.sales.findIndex(sale => sale.id == sid);
                    let status = this.sales[index].status;

                    if(status !== 'returned')
                        temp.push(sid);
                });

                // set new selected values
                this.selected = temp;
            }

            if(name === 'payment') {
                let temp = [];
                this.selected.forEach((sid) => {
                    let index = this.sales.findIndex(sale => sale.id == sid);
                    let status = this.sales[index].status;

                    if(status !== 'returned')
                        temp.push(sid);
                });

                // set new selected values
                this.selected = temp;
                this.loadPayment();
            }

            if(name === 'detail') {
                this.current.view = 0;
                this.load();
            }

            if(this.selected.length > 0) {
                // dont load if processing
                if(!this.isProcess)
                    this.modal = name;
                    
                // reset process mode
                this.isVisible = true;
            }else
                swal('Hold on!', 'Select at least one item from the table.', 'warning');
        },

        default() {
            this.payments = {
                total: parseFloat(0).toFixed(2),
                discount: 0,
                paid: 0,
                cash: 0,
                history: []
            }
        },

        total(quantity, price) {
            // verify quantity value
            quantity = quantity ? quantity : 0;

            let total = parseInt(quantity) * parseFloat(price);
            return total.toFixed(2);
        }
    },
    components: {
        'app-pagination': pagination
    },
    computed: {
        selectedList() {
            let temp = [];
            this.selected.forEach((sid) => {
                let sale = this.sales.find(sale => sale.id === sid);
                temp.push(sale);
            });

            return temp;
        },

        totalDS() {
            let total = 0;
            this.detail.cart.forEach((product) => {
                total +=  (product.price * product.quantity);
            });

            return total.toFixed(2);
        },

        totalA() {
            let discount = parseInt(this.payments.discount) / 100;
            let discount_val = discount * parseFloat(this.payments.total);
            let total = parseFloat(this.payments.total) - discount_val;

            return total.toFixed(2);
        },

        balance() {
            let balance = this.totalA - parseFloat(this.payments.paid);
            return balance.toFixed(2);
        },

        change() {
            let temp = this.payments.cash ? this.payments.cash : 0;
            let change = parseFloat(temp) - this.balance;

            return change.toFixed(2);
        }
    },
    created() {
        this.populateWH();
        this.populate();
        this.default();
    }
});