var approot = new Vue({
	el: '#approot',
	data: {
		user: {},
		roles: [],
		settings: {},

		// process toggler
		isProcess: false
	},
	methods: {
		populate() {
			$.post('../../../misc/load_user_dependencies', (response) => {
				let data = JSON.parse(response);
				if(data.success) {
					approot.roles = data.content;
					approot.settings = data.setting;
				}else
					approot.default('dependencies');
			});
		},

		update() {
			if(typeof this.user.pass != 'undefined' && this.user.pass !== this.user.repass)
				swal('Hold on!', 'Password confirmation did not matched.', 'warning');
			else {
				this.isProcess = true;

	        	let postdata = { user: this.user };
	        	$.post('../../user_update', postdata, (response) => {
	        		let data = JSON.parse(response);
	        		if(data.success) {
	        			swal('Good job!', 'User information has been updated.', 'success').then(() => {
	        				approot.gotoRoot();
	        			});
	        		}else
	        			swal('Oh noes!', 'Something went wrong while trying to update.', 'error');

	        		// reset process mode
	        		approot.isProcess = false;
	        	});
			}
		},

		// miscellaneous
		default(param) {
			// get user info
			let href = window.location.href.split('/');
			let uid = href[href.length -1];

			let postdata = { uid: uid };
			$.post('../../user_detail', postdata, (response) => {
				let data = JSON.parse(response);
				if(data.success)
					approot.user = data.content;
				else {
					swal('Oh noes!', 'Could not load this user.', 'error').then(() => {
						approot.gotoRoot();
					});
				}
			});

			if(param) {
				this.role = [
					{ id: 2, name: 'admin' },
					{ id: 3, name: 'cashier' },
					{ id: 4, name: 'manager' }
				];
			}
		},

		gotoRoot() {
			let href = window.location.href;
			let temp = href.split('/');

			temp.pop();
			temp.pop();

			window.location.href = temp.join('/');
		}
	},
	created() {
		this.populate();
		this.default();
	}
});