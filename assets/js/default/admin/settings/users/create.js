var approot = new Vue({
	el: '#approot',
	data: {
		user: {},
		roles: [],
		settings: {},

		// process toggler
		isProcess: false
	},
	methods: {
		populate() {
			$.post('../../misc/load_user_dependencies', (response) => {
				let data = JSON.parse(response);
				if(data.success) {
					approot.roles = data.content;
					approot.settings = data.setting;
				}else
					approot.default('dependencies');
			});
		},

		create() {
			if(this.user.pass === this.user.repass) {
				this.isProcess = true;

	        	let postdata = { user: this.user };
	        	$.post('../user_create', postdata, (response) => {
	        		let data = JSON.parse(response);
	        		if(data.success) {
	        			swal('Good job!', 'User has been created. Create more?', 'success', {
	        				buttons: ['No', 'Yes']
	        			}).then((confirm) => {
	        				if(confirm)
	        					this.default();
	        				else {
	        					let href = window.location.href.split('/');
	        					href.pop();

	        					window.location.href = href.join('/');
	        				}
						});
	        		}else
	        			swal('Oh noes!', 'Something went wrong while trying to save.', 'error');

	        		// reset process mode
	        		approot.isProcess = false;
	        	});
			}else
				swal('Hold on!', 'Password confirmation did not matched.', 'warning');
			
		},

		// miscellaneous
		default(param) {
			this.user = {
				firstname: '',
				lastname: '',
				gender: 'male',
				bday: null,
				role: 3,
				email: '',
				mobile: '',
				pass: '',
				repass: ''
			};

			if(param) {
				this.role = [
					{ id: 2, name: 'admin' },
					{ id: 3, name: 'cashier' },
					{ id: 4, name: 'manager' }
				];
			}
		}
	},
	created() {
		this.populate();
		this.default();
	}
});