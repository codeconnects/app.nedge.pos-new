var approot = new Vue({
    el: '#approot',
    data: {
        // appcontroller variables
        warehouses: [],
        warehouse: {
            id: 1,
            name: 'General Warehouse'
        },

        // appaction variables
        search: '',

        // apptable variables
        purchases: [],
        pageinfo: {
            current: 1,
            maxpage: 0
        },

        // appmodal
        modal: '',
        detail: {
            purchase: {},
            products: [],
            adjustments: [],
            history: []
        },
        payments: {
            history: []
        },

        // miscellaneous
        settings: {},
        selected: [],
        current: {
            view: 0,
            process: 1
        },

        // process toggler
        isVisible: false,
        isProcess: false
    },
    methods: {
        // appcontroller methods
        populateWH() {
            $.post('warehouses/request_load', (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.warehouses = data.content;
                    approot.warehouse = data.content[0];
                }
            });
        },

        selectWH(wid) {
            let index = this.warehouses.findIndex(warehouse => warehouse.id === wid);

            this.warehouse = this.warehouses[index];
            this.pageinfo.current = 1;
            this.selected = [];

            this.populate();
        },

        // appaction methods
        gotoUpdate() {
            if(this.selected.length > 0) {
                let pid = this.selected[0];
                let href = window.location.href;

                window.location.href = href +'/update/'+ pid;
            }else
                swal('Hold on!', 'Select one item from the table.', 'warning');
        },

        // apptable methods
        populate() {
            let postdata = {
                keyword: this.search,
                warehouse: this.warehouse.id,
                pagenum: this.pageinfo.current
            };

            $.post('purchases/request_load', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.purchases = data.content;
                    this.settings = data.setting;
                    this.pageinfo = data.pageset;
                }
            });
        },

        select(event) {
            let target = event.target;
            let type = event.type;

            if(type === 'change') {
                this.selected = [];
                if(target.checked) {
                    this.purchases.forEach((purchase) => {
                        let pid = purchase.id;
                        this.selected.push(pid);
                    });
                }
            }else {
                if(target.localName !== 'input') {
                    let parent = target.parentElement;
                    let pid = parent.firstChild.children[0].value;

                    if(!parent.firstChild.children[0].checked)
                        this.selected.push(pid);
                    else {
                        let index = this.selected.findIndex(purchase => purchase === pid);
                        this.selected.splice(index, 1);
                    }
                }
            }
        },

        navigate(pagenum) {
            this.pageinfo.current = pagenum;
            this.selected = [];
            
            this.populate();
        },

        // appmodal methods
        loadPayment() {
            let pid = this.selected[0];
            $.post('purchases/request_payment', { pid: pid }, (response) => {
                let data = JSON.parse(response);
                if(data.success)
                    this.payments = data.content;
            });
        },

        payment() {
            if(!isNaN(this.payments.cash) && this.change <= 0) {
                let cash = parseFloat(this.payments.cash);
                if(cash > 0) {
                    let postdata = {
                        purchase: this.payments.id,
                        amount: this.payments.cash
                    };

                    $.post('purchases/request_pay', postdata, (response) => {
                        let data = JSON.parse(response);
                        if(data.success) {
                            this.loadPayment();
                            this.payments.cash = '';
                        }else
                            swal('Oh noes!', 'Something went wrong while processing.', 'error');
                    });
                }else
                    swal('Hold on!', 'Cash tendered should be greater than 0.', 'warning');
            }
        },
        startReceive() {
            this.isProcess = true;

            let postdata = { purchases: this.selected };
            $.post('purchases/request_receive', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.selected.forEach((pid) => {
                        let index = approot.purchases.findIndex(purchase => purchase.id === pid);
                        approot.purchases[index].status = 'received';
                    });

                    // reset selected
                    approot.selected = [];
                }else
                    swal('Wait a second...', 'Some purchases were not updated.', 'warning');

                this.selected = [];
                this.isVisible = false;
                this.isProcess = false;
            });
        },

        startReturn() {
            this.isProcess = true;

            let postdata = { purchases: this.selected };
            $.post('purchases/request_return', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.selected.forEach((pid) => {
                        let index = approot.purchases.findIndex(purchase => purchase.id === pid);
                        approot.purchases[index].status = 'returned';
                    });

                    // reset selected
                    approot.selected = [];
                }else
                    swal('Wait a second...', 'Some purchases were not updated.', 'warning');

                this.selected = [];
                this.isVisible = false;
                this.isProcess = false;
            });
        },

        startPrint() {
            this.isProcess = true;
            let timer = setInterval(() => {
                this.current.process++;
                if(this.current.process > this.selected.length) {
                    clearInterval(timer);

                    this.current.process = 1;
                    this.selected = [];
                    this.isVisible = false;
                    this.isProcess = false;
                }
            }, 500);
        },

        startExport() {
            this.isProcess = true;
            let timer = setInterval(() => {
                this.current.process++;
                if(this.current.process > this.selected.length) {
                    clearInterval(timer);

                    this.current.process = 1;
                    this.selected = [];
                    this.isVisible = false;
                    this.isProcess = false;
                }
            }, 500);
        },

        restore(pid) {
            let index = this.selected.findIndex(purchase => purchase === pid);
            this.selected.splice(index, 1);

            if(this.selected.length === 0) {
                this.isVisible = false;
                this.isProcess = false;
            }
        },

        load() {
            let index = this.current.view;
            let pid = this.selected[index];

            $.post('purchases/request_view', { pid: pid }, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.detail.purchase = data.content.purchase;
                    approot.detail.products = data.content.products;
                    approot.detail.adjustments = data.content.adjustments;
                    approot.detail.history = data.content.history;
                }
            });
        },

        next() {
            if((this.current.view +1) < this.selected.length) {
                this.current.view++;
                this.load();
            }
        },

        prev() {
            if((this.current.view -1) >= 0) {
                this.current.view--;
                this.load();
            }
        },

        // miscealleous
        show(name) {
            if(name === 'receive') {
                let temp = [];
                this.selected.forEach((pid) => {
                    let index = this.purchases.findIndex(purchase => purchase.id == pid);
                    let status = this.purchases[index].status;

                    if(status === 'pending')
                        temp.push(pid);
                });

                // set new selected values
                this.selected = temp;
            }

            if(name === 'return') {
                let temp = [];
                this.selected.forEach((pid) => {
                    let index = this.purchases.findIndex(purchase => purchase.id == pid);
                    let status = this.purchases[index].status;

                    if(status !== 'returned')
                        temp.push(pid);
                });

                // set new selected values
                this.selected = temp;
            }

            if(name === 'detail') {
                this.current.view = 0;
                this.load();
            }

            if(name === 'payment') {
                let temp = [];
                this.selected.forEach((pid) => {
                    let index = this.purchases.findIndex(purchase => purchase.id == pid);
                    let status = this.purchases[index].status;

                    if(status !== 'returned')
                        temp.push(pid);
                });

                // set new selected values
                this.selected = temp;
                this.loadPayment();
            }

            if(this.selected.length > 0) {
                // dont load if processing
                if(!this.isProcess)
                    this.modal = name;
                    
                // reset process mode
                this.isVisible = true;
            }else
                swal('Hold on!', 'Select at least one item from the table.', 'warning');
        }
    },
    components: {
        'app-pagination': pagination
    },
    computed: {
        selectedList() {
            let temp = [];
            this.selected.forEach((pid) => {
                let purchase = this.purchases.find(purchase => purchase.id === pid);
                temp.push(purchase);
            });

            return temp;
        },

        total() {
            let total = 0;
            this.detail.products.forEach((product) => {
                total +=  (product.price * product.quantity);
            });

            return total.toFixed(2);
        },

        totalH() {
            let total = 0;
            this.detail.history.forEach((product) => {
                total +=  (product.price * product.quantity);
            });

            return total.toFixed(2);
        },

        totalPA() {
            let discount = parseInt(this.payments.discount) / 100;
            let discount_val = discount * parseFloat(this.payments.total);
            let total = parseFloat(this.payments.total) - discount_val;

            return total.toFixed(2);
        },

        balance() {
            let balance = this.totalPA - parseFloat(this.payments.paid);
            return balance.toFixed(2);
        },

        change() {
            let temp = this.payments.cash ? this.payments.cash : 0;
            let change = parseFloat(temp) - this.balance;

            return change.toFixed(2);
        }
    },
    created() {
        this.populateWH();
        this.populate();
    }
});