var appActions = new Vue({
    el: '#appActions',
    data: {
        searchWord: '',
        isProcessing: false,
        isCompleted: false
    },
    methods: {
        searchPurchase() {
            console.log(this.searchWord);
        },

        importDemo() {
            this.isProcessing = true;
            this.isCompleted = false;

            appTable.purchases = [];
            appTable.purchasesRaw = [];

            let counter = 1;
            let timer = setInterval(function() {
                if(counter === 170) {
                    appActions.isProcessing = false;
                    appActions.isCompleted = true;

                    clearInterval(timer);
                }

                let temp = {
                    id: counter,
                    reference: 11000224200 + counter,
                    datetime: 'Nov 11, 2018 06:20:11 PM',
                    total: '1,000.00',
                    supplier: 'Supplier',
                    status: 'pending'
                };

                appTable.purchasesRaw.push(temp);

                let rowLimit = 20;
                if(counter <= rowLimit)
                    appTable.purchases.push(temp);

                counter++;

                // set pagination
                appTable.pagination.total = Math.ceil(appTable.purchasesRaw.length / rowLimit);
            }, 0);
        },

        showModal(modal) {
            if(appTable.selectedPurchases.length > 0)
                appModal.showModal(modal);
            else
                swal('Hold on!', 'Select purchases from the table first.', 'warning');
        }
    }
});

var appTable = new Vue({
    el: '#appTable',
    components: {
        'pagination': pagination
    },
    data: {
        purchases: [],
        purchasesRaw: [],
        pagination: {
            current: 1,
            total: 0,
        },
        settings: {
            currency: 'PHP',
            currency_symbol: '₱',
            infinite: false
        },
        selectedPurchases: [],
    },
    methods: {
        loadPurchases() {
            $.post('load', function(data) {
                let jsonData = JSON.parse(data);
                if(jsonData.success) {
                    appTable.purchases = jsonData.content;
                    appTable.pagination = jsonData.setting;
                }
            });
        },

        setSelected(event) {
            let target = event.target;
            let type = event.type;

            if(type === 'change') {
                this.selectedPurchases = [];
                if(target.checked) {
                    this.purchases.forEach(function(element) {
                        let pId = element.id;
                        appTable.selectedPurchases.push(pId);
                    });
                }
            }else {
                if(target.localName !== 'input') {
                    let parent = target.parentElement;
                    let pId = parseInt(parent.firstChild.children[0].value);

                    if(!parent.firstChild.children[0].checked)
                        this.selectedPurchases.push(pId);
                    else {
                        let index = this.selectedPurchases.findIndex(purchase => purchase === pId);
                        this.selectedPurchases.splice(index, 1);
                    }
                }
            }
        },

        pageNavigate(pageNum) {
            let rowLimit = 20;
            let temp = [];

            let startIndex = (pageNum-1) * rowLimit;
            let endIndex = (startIndex + rowLimit) > (this.purchasesRaw.length -1) ? this.purchasesRaw.length : startIndex + rowLimit;
            
            this.purchases = [];
            for(let i = startIndex; i < endIndex; i++) {
                this.purchases.push(this.purchasesRaw[i]);
            }

            this.pagination.current = pageNum;
        }
    }
});

var appModal = new Vue({
    el: '#appModal',
    data: {
        modalName: '',
        purchases: [],
        viewPurchase: {},
        settings: {},

        // process toggler
        isVisible: false,
        isProcessing: false,
        currentView: 0,
        currentProcessing: 1
    },
    methods: {
        showModal(modal) {
            // do not load purchases while processing
            if(this.isProcessing === false) {
                this.modalName = modal;
                this.loadPurchases();
                this.settings = appTable.settings;
            }

            // set process mode
            this.isVisible = true;
            this.currentView =  0;
            this.viewPurchase = this.purchases[this.currentView];
        },

        loadPurchases() {
            this.purchases = [];
            appTable.selectedPurchases.forEach(function(element) {
                let purchase = appTable.purchases.find(purchase => purchase.id === element);
                appModal.purchases.push(purchase);
            });
        },

        restorePurchase(id) {
            let index = appTable.selectedPurchases.findIndex(purchase => purchase === id);
            appTable.selectedPurchases.splice(index, 1);
            this.loadPurchases();

            // automatically close modal if no more purchases left
            if(this.purchases.length === 0) this.isVisible = false;
        },

        startReceiving() {
            this.isProcessing = true;

            for(let i = 0; i < this.purchases.length; i++) {
                let pId = this.purchases[i].id;
                let index = appTable.purchases.findIndex(purchase => purchase.id === pId);

                // deactivate purchases
                appTable.purchases[index].status = 'received';
                this.loadPurchases();
            }

            // reset processing mode
            this.isVisible = false;
            this.isProcessing = false;
        },

        startReturning() {
            this.isProcessing = true;

            for(let i = 0; i < this.purchases.length; i++) {
                let pId = this.purchases[i].id;
                let index = appTable.purchases.findIndex(purchase => purchase.id === pId);

                // deactivate purchases
                appTable.purchases[index].status = 'returned';
                this.loadPurchases();
            }

            // reset processing mode
            this.isVisible = false;
            this.isProcessing = false;
        },

        viewNext() {
            if((this.currentView +1) < this.purchases.length) {
                this.currentView++;
                this.viewPurchase = this.purchases[this.currentView];
            }
        },

        viewPrevious() {
            if((this.currentView -1) >= 0) {
                this.currentView--;
                this.viewPurchase = this.purchases[this.currentView];
            }
        }
    }
});