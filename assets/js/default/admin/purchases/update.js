var approot = new Vue({
	el: '#approot',
	data: {
		dependencies: {
			warehouses: [],
			suppliers: [],
			taxes: []
		},

		search: '',
		products: [],

		information: {},
		orders: [],

		settings: {},

		// process toggler
		isProcess: false
	},
	methods: {
		populate() {
			$.post('../../misc/load_purchase_dependencies', (response) => {
				let data = JSON.parse(response);
				if(data.success) {
					approot.dependencies = data.content;
					approot.settings = data.setting;
				}else
					approot.default('dependencies');
			});
		},

		searchp() {
			if(this.search.length > 0) {
				let postdata = {
	                keyword: this.search,
	                pagenum: 1,
	                source: 'external'
	            };

				$.post('../../products/request_load', postdata, (response) => {
					let data = JSON.parse(response);
					if(data.success)
						approot.products = data.content;
				});
			}else
				this.products = [];
		},

		addToCart(pid) {
			let index = this.products.findIndex(product => product.id === pid);
			let product = this.products[index];

			let temp = {
				id: product.id,
				code: product.code,
				name_display: product.name_display,
				category: product.category,
				price: 1,
				quantity: 1
			};

			let isAdded = this.orders.find(product => product.id === temp.id);
			if(!isAdded) {
				this.orders.push(temp);
				this.search = '';
			}else
				swal('Hold on!', 'Product already added in cart.', 'warning');
		},

		removeFromCart(pid) {
			let index = this.orders.findIndex(product => product.id === pid);
        	this.orders.splice(index, 1);
		},

		update() {
			this.isProcess = true;

        	let postdata = {
        		info: this.information,
        		cart: this.orders
        	};

        	$.post('../request_update', postdata, (response) => {
        		let data = JSON.parse(response);
        		if(data.success) {
        			swal('Good job!', 'Purchase has been updated.', 'success').then(() => {
        				approot.gotoRoot();
					});
        		}else
        			swal('Oh noes!', 'Something went wrong while trying to save.', 'error');

        		// reset process mode
        		approot.isProcess = false;
        	});
		},

		// miscellaneous
		default() {
			let href = window.location.href.split('/');
			let pid = href[href.length -1];

			let postdata = { pid: pid };
			$.post('../request_detail', postdata, (response) => {
				let data = JSON.parse(response);
				if(data.success) {
					approot.information = data.info;
					approot.orders = data.cart;
					approot.settings = data.setting;
				}else {
					swal('Oh noes!', 'Could not load this purchase.', 'error').then(() => {
						approot.gotoRoot();
					});
				}
			});
		},

		generate() {
			let randCode = Math.floor(Math.random() * 8999) + 1000;
			let baseCode = [
				new Date().getMonth(),
				new Date().getDate(),
				new Date().getFullYear(),
				new Date().getHours(),
				new Date().getMinutes(),
				new Date().getSeconds(),
				new Date().getMilliseconds()
			];

			let bCodeVal = 0;
			baseCode.forEach((baseCode) => {
				bCodeVal += parseInt(baseCode);
			});

			// build barcode
			let initialCode = 'PUR';
			let finalCode = [initialCode, bCodeVal, randCode];

			this.information.reference = finalCode.join('-');
		},

		gotoRoot() {
			let href = window.location.href;
			let temp = href.split('/');

			temp.pop();
			temp.pop();

			window.location.href = temp.join('/');
		}
	},
	computed: {
		total() {
			let total = 0;
			this.orders.forEach((product) => {
				total +=  (product.price * product.quantity);
			});

			return total.toFixed(2);
		}
	},
	created() {
		this.populate();
		this.default();
	}
});