var approot = new Vue({
    el: '#approot',
    data: {
        // appcontroller variables
        warehouses: [],
        warehouse: {
            id: 1,
            name: 'General Warehouse'
        },

        // appaction variables
        search: '',

        // apptable variables
        stocks: [],
        pageinfo: {
            current: 1,
            maxpage: 0
        },

        // appmodal
        modal: '',
        vstock: {},

        // miscellaneous
        settings: {},
        selected: [],
        current: {
            view: 0,
            process: 1
        },

        // process toggler
        isVisible: false,
        isProcess: false,
        isConfirm: false
    },
    methods: {
        // appcontroller methods
        populateWH() {
            $.post('warehouses/request_load', (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.warehouses = data.content;
                    approot.warehouse = data.content[0];
                }
            });
        },

        selectWH(wid) {
            let index = this.warehouses.findIndex(warehouse => warehouse.id === wid);
            this.warehouse = this.warehouses[index];
            this.pageinfo.current = 1;

            this.populate();
        },

        // appaction methods
        gotoUpdate() {
            if(this.selected.length > 0) {
                let sid = this.selected[0];
                let pid = this.stocks.find(stock => stock.id === sid).product;

                window.location.href = 'products/update/'+ pid;
            }else
                swal('Hold on!', 'Select one item from the table.', 'warning');
        },

        // apptable methods
        populate() {
            let postdata = {
                keyword: this.search,
                warehouse: this.warehouse.id,
                pagenum: this.pageinfo.current
            };

            $.post('stocks/request_load', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.stocks = data.content;
                    this.settings = data.setting;
                    this.pageinfo = data.pageset;

                    // reset selected items
                    this.selected = [];
                }
            });
        },

        select(event) {
            let target = event.target;
            let type = event.type;

            if(type === 'change') {
                this.selected = [];
                if(target.checked) {
                    this.stocks.forEach((stock) => {
                        let sid = stock.id;
                        this.selected.push(sid);
                    });
                }
            }else {
                if(target.localName !== 'input') {
                    let parent = target.parentElement;
                    let sid = parent.firstChild.children[0].value;

                    if(!parent.firstChild.children[0].checked)
                        this.selected.push(sid);
                    else {
                        let index = this.selected.findIndex(stock => stock === sid);
                        this.selected.splice(index, 1);
                    }
                }
            }
        },

        navigate(pagenum) {
            this.pageinfo.current = pagenum;
            this.populate();
        },

        // appmodal methods
        adjust() {
            if(this.isConfirm) {
                this.isProcess = true;

                // build post parameter
                let temp = [];
                this.selected.forEach((sid) => {
                    let index = this.stocks.findIndex(stock => stock.id === sid);
                    let data = {
                        purchase: this.stocks[index].purchase,
                        product: this.stocks[index].product,
                        quantity: this.stocks[index].quantity
                    };

                    temp.push(data);
                });

                let postdata = { adjustments: temp };
                $.post('stocks/request_adjust', postdata, (response) => {
                    let data = JSON.parse(response);
                    if(data.success) {
                        approot.selected = [];
                        approot.isVisible = false;
                    }else
                        swal('Oh noes!', 'Some stocks were not updated.', 'error');

                    // reset process mode
                    approot.isProcess = false;
                });
            }else
                swal('Hold on!', 'Confirm the adjustments first to proceed.', 'warning');
        },

        update() {
            this.isProcess = true;

            // build post parameter
            let temp = [];
            this.selected.forEach((sid) => {
                let index = this.stocks.findIndex(stock => stock.id === sid);
                let data = {
                    sid: sid,
                    price: this.stocks[index].price
                };

                temp.push(data);
            });

            let postdata = { adjustments: temp };
            $.post('stocks/request_update', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    approot.selected = [];
                    approot.isVisible = false;
                }else
                    swal('Oh noes!', 'Some stocks were not updated.', 'error');

                // reset process mode
                approot.isProcess = false;
            });
        },

        expire() {
            this.isProcess = true;
            let postdata = { stocks: this.selected };
            $.post('stocks/request_expire', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success) {
                    this.populate();
                    this.isVisible = false;
                    this.isProcess = false;
                }else
                    swal('Oh noes!', 'Some stocks were not updated.', 'error');
            });
        },

        restore(sid) {
            let index = this.selected.findIndex(stock => stock === sid);
            this.selected.splice(index, 1);

            if(this.selected.length === 0) {
                this.isVisible = false;
                this.isProcess = false;
            }
        },

        load() {
            let index = this.current.view;
            let sid = this.selected[index];

            let postdata = { sid: sid };
            $.post('stocks/request_view', postdata, (response) => {
                let data = JSON.parse(response);
                if(data.success)
                    approot.vstock = data.content;
            });
        },

        next() {
            if((this.current.view +1) < this.selected.length) {
                this.current.view++;
                this.load();
            }
        },

        prev() {
            if((this.current.view -1) >= 0) {
                this.current.view--;
                this.load();
            }
        },

        // miscealleous
        show(name) {
            if(name === 'detail') {
                this.current.view = 0;
                this.load();
            }

            if(this.selected.length > 0) {
                // dont load if processing
                if(!this.isProcess)
                    this.modal = name;
                    
                // reset process mode
                this.isVisible = true;
            }else
                swal('Hold on!', 'Select at least one item from the table.', 'warning');
        },

        close() {
            if(!this.isProcess) {
                this.isVisible = false;
                this.populate();
            }else
                swal('Still updating...', 'Closing modal will possibly cause an error.', 'warning');
        }
    },
    components: {
        'app-pagination': pagination
    },
    computed: {
        selectedList() {
            let temp = [];
            this.selected.forEach((sid) => {
                let stock = this.stocks.find(stock => stock.id === sid);
                temp.push(stock);
            });

            return temp;
        }
    },
    created() {
        this.populateWH();
        this.populate();
    }
});