var approot = new Vue({
	el: '#approot',
	data: {
		dependencies: {
			categories: [],
			subcategories: [],
			brands: [],
			units: []
		},

		information: {},
		products: [],
		parent: '',

		// process toggler
		isProcess: false,
		isSearch: false
	},
	methods: {
		populate() {
			$.post('../misc/load_product_dependencies', (response) => {
				let data = JSON.parse(response);
				if(data.success)
					approot.dependencies = data.content;
				else
					approot.default('dependencies');
			});
		},

		search() {
			this.isSearch = true;

			let postdata = {
				keyword: this.parent,
				pagenum: 1
			};

			$.post('request_load', postdata, (response) => {
				let data = JSON.parse(response);
				if(data.success)
					approot.products = data.content;
			});
		},

		select(param) {
			this.information.cb_parent = param.id;
			this.parent = param.name_display;
			this.isSearch = false;
		},

		reset() {
			this.information.cb_parent = 0;
			this.parent = '';
			this.isSearch = false;
		},

		create() {
			this.isProcess = true;

			let postdata = { data: this.information };
			$.post('request_create', postdata, (response) => {				
				let data = JSON.parse(response);
				if(data.success) {
					swal('Good job!', 'Product has been created. More products?', 'success', {
						buttons: ['No', 'Yes, Create more']
					}).then((confirm) => {
						if(confirm) {
							approot.default();
							approot.isProcess = false;
						}else {
							let href = window.location.href;
							let temp = href.split('/');
							temp.pop();

							window.location.href = temp.join('/');
						}
					});
				}else
					swal('Oh noes!', 'Something went wrong while trying to create product.', 'error');

				approot.isProcess = false;
			});
		},

		// miscellaneous
		default() {
			this.parent = '';
			this.information = {
				product_type: 'standard',
				barcode_type: 'code128',
				code: '',
				code_hsn: '',
				name: '',
				name_display: '',
				slug: '',
				category: 1,
				category_sub: 1,
				brand: 1,
				unit: 1,
				trigger: 20,
				cb_parent: 0,
				cb_quantity: 1,
				image: null,
				description: '',
				is_tracked: 1
			};
		},

		generate() {
			let randCode = Math.floor(Math.random() * 8999) + 1000;
			let baseCode = [
				new Date().getMonth(),
				new Date().getDate(),
				new Date().getFullYear(),
				new Date().getHours(),
				new Date().getMinutes(),
				new Date().getSeconds(),
				new Date().getMilliseconds()
			];

			let bCodeVal = 0;
			baseCode.forEach((baseCode) => {
				bCodeVal += parseInt(baseCode);
			});

			// build barcode
			let initialCode = 'B';
			let finalCode = [initialCode, bCodeVal, randCode];

			this.information.code = finalCode.join('-');
		},

		slug() {
			let slug = this.information.name.toLowerCase().replace(/ /g, '-');
			this.information.slug = slug;
		}
	},
	computed: {
		image() {
			let image = this.information.image;
			return image ? image : 'Choose an image';
		}
	},
	created() {
		this.populate();
		this.default();
	}
});