var appActions = new Vue({
    el: '#appActions',
    data: {
        searchWord: '',
        isProcessing: false,
        isCompleted: false
    },
    methods: {
        searchProduct() {
            console.log(this.searchWord);
        },

        importDemo() {
            this.isProcessing = true;
            this.isCompleted = false;

            appTable.products = [];
            appTable.productsRaw = [];

            let counter = 1;
            let timer = setInterval(function() {
                if(counter === 170) {
                    appActions.isProcessing = false;
                    appActions.isCompleted = true;

                    clearInterval(timer);
                }

                let temp = {
                    id: counter,
                    code: 11000224200 + counter,
                    code_hsn: 11000224200 + counter,
                    name: 'This is a long product name for testing design ' + counter,
                    name_display: 'Product ' + counter,
                    slug: 'this-is-a-long-product-name-for-testing-design-' + counter,
                    category: 'Category ' + counter,
                    category_sub: 'Sub Category ' + counter,
                    brand: 'Brand ' + counter,
                    unit: 'Piece',
                    cb_parent: 0,
                    cb_quantity: 0,
                    image: 'default.jpg',
                    description: 'this is test description',
                    product_type: 'Standard',
                    barcode_type: 'code128',
                    trigger: 20,
                    is_tracked: 1,
                    is_hidden: 0 
                };

                appTable.productsRaw.push(temp);

                let rowLimit = 20;
                if(counter <= rowLimit)
                    appTable.products.push(temp);

                counter++;

                // set pagination
                appTable.settings.total = Math.ceil(appTable.productsRaw.length / rowLimit);
            }, 0);
        },

        showModal(modal) {
            if(appTable.selectedProducts.length > 0)
                appModal.showModal(modal);
            else
                swal('Hold on!', 'Select products from the table first.', 'warning');
        }
    }
});

var appTable = new Vue({
    el: '#appTable',
    components: {
        'pagination': pagination
    },
    data: {
        products: [],
        productsRaw: [],
        settings: {
            current: 1,
            total: 0,
            infinite: false
        },
        selectedProducts: [],
    },
    methods: {
        loadProducts() {
            $.post('load', function(data) {
                let jsonData = JSON.parse(data);
                if(jsonData.success) {
                    appTable.products = jsonData.content;
                    appTable.settings = jsonData.setting;
                }
            });
        },

        setSelected(event) {
            let target = event.target;
            let type = event.type;

            if(type === 'change') {
                this.selectedProducts = [];
                if(target.checked) {
                    this.products.forEach(function(element) {
                        let pId = element.id;
                        appTable.selectedProducts.push(pId);
                    });
                }
            }else {
                if(target.localName !== 'input') {
                    let parent = target.parentElement;
                    let pId = parseInt(parent.firstChild.children[0].value);

                    if(!parent.firstChild.children[0].checked)
                        this.selectedProducts.push(pId);
                    else {
                        let index = this.selectedProducts.findIndex(product => product === pId);
                        this.selectedProducts.splice(index, 1);
                    }
                }
            }
        },

        pageNavigate(pageNum) {
            let rowLimit = 20;
            let temp = [];

            let startIndex = (pageNum-1) * rowLimit;
            let endIndex = (startIndex + rowLimit) > (this.productsRaw.length -1) ? this.productsRaw.length : startIndex + rowLimit;
            
            this.products = [];
            for(let i = startIndex; i < endIndex; i++) {
                this.products.push(this.productsRaw[i]);
            }

            this.settings.current = pageNum;
        }
    }
});

var appModal = new Vue({
    el: '#appModal',
    data: {
        modalName: '',
        products: [],
        viewProduct: {},

        // process toggler
        isVisible: false,
        isProcessing: false,
        currentView: 0,
        currentProcessing: 1
    },
    methods: {
        showModal(modal) {
            // do not load products while processing
            if(this.isProcessing === false) {
                this.modalName = modal;
                this.loadProducts();
            }

            this.isVisible = true;
            this.currentView =  0;
            this.viewProduct = this.products[this.currentView];
        },

        loadProducts() {
            this.products = [];
            appTable.selectedProducts.forEach(function(element) {
                let product = appTable.products.find(product => product.id === element);
                appModal.products.push(product);
            });
        },

        restoreProduct(id) {
            let index = appTable.selectedProducts.findIndex(product => product === id);
            appTable.selectedProducts.splice(index, 1);
            this.loadProducts();

            // automatically close modal if no more products left
            if(this.products.length === 0) this.isVisible = false;
        },

        startDeleting() {
            this.isProcessing = true;

            for(let i = 0; i < this.products.length; i++) {
                let pId = this.products[i].id;
                let index = appTable.productsRaw.findIndex(product => product.id === pId);

                // deactivate products
                appTable.productsRaw[index].is_hidden = 1;
                this.loadProducts();
            }

            // reset processing mode
            this.isVisible = false;
            this.isProcessing = false;

            // reload table
            let current = appTable.settings.current;
            appTable.pageNavigate(current);
        },

        startPrinting() {
            this.isProcessing = true;
            let timer = setInterval(function() {
                if(appModal.currentProcessing === appModal.products.length) {
                    appModal.isVisible = false;
                    appModal.isProcessing = false;
                    appModal.currentProcessing = 1;

                    clearInterval(timer);
                }

                appModal.currentProcessing++;
            }, 500);
        },

        viewNext() {
            if((this.currentView +1) < this.products.length) {
                this.currentView++;
                this.viewProduct = this.products[this.currentView];
            }
        },

        viewPrevious() {
            if((this.currentView -1) >= 0) {
                this.currentView--;
                this.viewProduct = this.products[this.currentView];
            }
        }
    }
});