var approot = new Vue({
	el: '#approot',
	data: {
		dependencies: {
			categories: [],
			subcategories: [],
			brands: [],
			units: []
		},

		information: {},
		products: [],
		parent: '',

		// process togger
		isProcess: false,
		isSearch: false
	},
	methods: {
		populate() {
			$.post('../../misc/load_product_dependencies', (response) => {
				let data = JSON.parse(response);
				if(data.success)
					approot.dependencies = data.content;
				else
					approot.default('dependencies');
			});
		},

		search() {
			this.isSearch = true;

			let postdata = {
				keyword: this.parent,
				pagenum: 1
			};

			$.post('../request_load', postdata, (response) => {
				let data = JSON.parse(response);
				if(data.success)
					approot.products = data.content;
			});
		},

		select(param) {
			this.information.cb_parent = param.id;
			this.parent = param.name_display;
			this.isSearch = false;
		},

		reset() {
			this.information.cb_parent = 0;
			this.parent = '';
			this.isSearch = false;
		},

		update() {
			this.isProcess = true;

			let postdata = { data: this.information };
			$.post('../request_update', postdata, (response) => {				
				let data = JSON.parse(response);
				if(data.success) {
					swal('Good job!', 'Product information has been updated.', 'success').then(() => {
						approot.gotoRoot();
					});
				}else
					swal('Oh noes!', 'Something went wrong while trying to update product.', 'error');

				approot.isProcess = false;
			});
		},

		// miscellaneous
		default() {
			let href = window.location.href.split('/');
			let pid = href[href.length -1];

			let postdata = { pid: pid };
			$.post('../request_detail', postdata, (response) => {
				let data = JSON.parse(response);
				if(data.success) {
					approot.information = data.content;
					approot.parent = data.content.parent;
				}else {
					swal('Oh noes!', 'Could not load this product.', 'error').then(() => {
						approot.gotoRoot();
					});
				}
			});
		},

		generate() {
			let randCode = Math.floor(Math.random() * 8999) + 1000;
			let baseCode = [
				new Date().getMonth(),
				new Date().getDate(),
				new Date().getFullYear(),
				new Date().getHours(),
				new Date().getMinutes(),
				new Date().getSeconds(),
				new Date().getMilliseconds()
			];

			let bCodeVal = 0;
			baseCode.forEach((baseCode) => {
				bCodeVal += parseInt(baseCode);
			});

			// build barcode
			let initialCode = 'B';
			let finalCode = [initialCode, bCodeVal, randCode];

			this.information.code = finalCode.join('-');
		},

		slug() {
			let slug = this.information.name.toLowerCase().replace(/ /g, '-');
			this.information.slug = slug;
		},

		gotoRoot() {
			let href = window.location.href;
			let temp = href.split('/');

			temp.pop();
			temp.pop();

			window.location.href = temp.join('/');
		}
	},
	computed: {
		image() {
			let image = this.information.image;
			return image ? image : 'Choose an image';
		}
	},
	created() {
		this.populate();
		this.default();
	}
});