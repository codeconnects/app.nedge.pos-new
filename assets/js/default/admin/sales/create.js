var approot = new Vue({
	el: '#approot',
	data: {
		dependencies: {
			warehouses: [],
			customers: [],
			billers: []
		},

		search: '',
		products: [],

		information: {},
		orders: [],

		settings: {},

		// process toggler
		isProcess: false
	},
	methods: {
		populate() {
			$.post('../misc/load_sale_dependencies', (response) => {
				let data = JSON.parse(response);
				if(data.success) {
					approot.dependencies = data.content;
					approot.settings = data.setting;
				}else
					approot.default('dependencies');
			});
		},

		searchp() {
			if(this.search.length > 0) {
				let postdata = {
	                keyword: this.search,
	                warehouse: this.information.warehouse
	            };

				$.post('../pos/request_search', postdata, (response) => {
					let data = JSON.parse(response);
					if(data.success)
						approot.products = data.content;
				});
			}else
				this.products = [];
		},

		addToCart(pid) {
			let index = this.products.findIndex(product => product.id === pid);
			let product = this.products[index];

			let temp = {
				id: product.product,
				purchase: product.purchase,
				code: product.code,
				name_display: product.name_display,
				category: product.category,
				price: product.price,
				quantity: 1
			};

			let isAdded = this.orders.find(product => product.id === pid);
			if(!isAdded) {
				this.orders.push(temp);
				this.search = '';
			}else
				swal('Hold on!', 'Product already added in cart.', 'warning');
		},

		removeFromCart(pid) {
			let index = this.orders.findIndex(product => product.id === pid);
        	this.orders.splice(index, 1);
		},

		process() {
			if(this.information.method === 'cash' && this.change < 0)
				swal('Hold on!', 'Cash tendered is less than the amount due.', 'warning');
			else {
				this.isProcess = true;
	        	let postdata = {
	        		info: this.information,
	        		cart: this.orders
	        	};

	        	$.post('request_create', postdata, (response) => {
	        		let data = JSON.parse(response);
	        		if(data.success) {
	        			swal('Good job!', 'Sale has been created. More sales?', 'success', {
	        				buttons: ['No', 'Yes, Create more']
	        			}).then((confirm) => {
	        				if(confirm) {
	        					this.orders = [];
	        					this.default();
	        				}else {
	        					let href = window.location.href.split('/');
	        					href.pop();

	        					window.location.href = href.join('/');
	        				}
						});
	        		}else
	        			swal('Oh noes!', 'Something went wrong while trying to save.', 'error');

	        		// reset process mode
	        		this.reset();
	        	});
			}
		},

		// miscellaneous
		default() {
			this.information = {
				datetime: '',
				reference: '',
				warehouse: 1,
				discount: 0,
				method: 'cash',
				customer: 3,
				paid: 0,
				operator: 1
			};
		},

		generate() {
			let randCode = Math.floor(Math.random() * 8999) + 1000;
			let baseCode = [
				new Date().getMonth(),
				new Date().getDate(),
				new Date().getFullYear(),
				new Date().getHours(),
				new Date().getMinutes(),
				new Date().getSeconds(),
				new Date().getMilliseconds()
			];

			let bCodeVal = 0;
			baseCode.forEach((baseCode) => {
				bCodeVal += parseInt(baseCode);
			});

			// build barcode
			let initialCode = 'SLM';
			let finalCode = [initialCode, bCodeVal, randCode];

			this.information.reference = finalCode.join('-');
		},

		reset() {
			this.search = '';
			this.isProcess = false;

			this.products = [];
			this.orders = [];
		}
	},
	computed: {
		stotal() {
			let total = 0;
			this.orders.forEach((product) => {
				total +=  (product.price * product.quantity);
			});

			return total.toFixed(2);
		},

		atotal() {
			let discount = parseInt(this.information.discount);

			// discount limits
			if(discount > 100)
				discount = 100;
			else if(discount < 0 || isNaN(discount))
				discount = 0;

			let temp = (discount / 100) * this.stotal;
			let total = this.stotal - temp;

			return total.toFixed(2);
		},

		change() {
			let temp = parseFloat(this.information.paid) - parseFloat(this.atotal);
			return temp.toFixed(2);
		}
	},
	created() {
		this.populate();
		this.default();
	}
});