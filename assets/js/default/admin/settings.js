var appController = new Vue({
    el: '#appController',
    data: {
        controllerName: 'System Settings',
    },
    methods: {
        load: function(param) {
            switch(param) {
                case 'system':
                this.controllerName = "System Settings";
                appContent.pageContent = param;
                appContent.loadPageData();
                break;

                case 'pos':
                this.controllerName = "POS Settings";
                appContent.pageContent = param;
                appContent.loadPageData();
                break;
            }
        }
    }
});

var appContent = new Vue({
    el: '#appContent',
    data: {
        pageContent: 'system',
        pageLoadText: 'Initializing Components',
        pageLoaded: false,

        // system settings data
        dataSystem: {
            site: [],
            accounting: [],
            product: [],
            prefix: []
        },

        // pos settings data


        // notification data
        notif: {
            target: null,
            heading: null,
            message: null,
            footer: null
        },

        // other
        timeout: null
    },
    created: function() {
        this.pageLoadText = 'Loading Data';
        this.loadPageData();
    },
    methods: {
        // load settings
        loadPageData() {
            switch(this.pageContent) {
                case 'system':
                this.pageLoaded = false;
                $.post('settings/load/'+ this.pageContent, function(response){
                    var responseObj = JSON.parse(response);
                    appContent.pageLoaded = true;
                });

                break;

                case 'pos':
                this.pageLoaded = false;
                $.post('settings/load/'+ this.pageContent, function(response){
                    var responseObj = JSON.parse(response);
                    appContent.pageLoaded = true;
                });

                break;
            }
        },

        // system settings method
        onSubmitSystem: function(event) {
            var name = event.target.name;
            var form = $(event.target).serializeArray();

            $.post('settings/request/system', { source: name, data: form }, function(response) {
                var responseObj = JSON.parse(response);
                appContent.setupAlert('system.'+ name, responseObj);
            });
        },

        // notification function
        setupAlert: function(source, content) {
            // set values
            appContent.notif = {
                target: source,
                heading: content.heading,
                message: content.message,
                footer: content.footer
            }

            // reset back to null
            clearTimeout(this.timeout);
            var duration = content.success? 3000: 5000;
            this.timeout = setTimeout(function(){
                appContent.notif = {
                    target: null,
                    heading: null,
                    message: null,
                    footer: null
                }
            }, duration);
        }
    }
});