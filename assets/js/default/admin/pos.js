var approot = new Vue({
	el: '#approot',
	data: {
		// appaction variables
		keywordp: '',
		keywordc: '',
		products: [],
		customers: [],

		// appinfo variables
		customer: {},
		discount: 0,
		method: 'cash',
		paid: 0,

		// apptable variables
		orders: [],
		pageinfo: {
			current: 1,
			maxpage: 0
		},

		// appmodal variables
		modal: '',
		suspends: [],

		// miscellaneous
		settings: {},

		// process toggler
		grid: false,
		isVisible: false,
		isProcess: false,
		isLoaded: false,

		warehouse: 1,
		order: 0
	},
	methods: {
		// appcontent methods
		searchp() {
			if(this.keywordp.length > 0) {
				let postdata = {
	                keyword: this.keywordp,
	                warehouse: this.warehouse
	            };

				$.post('pos/request_search', postdata, (response) => {
					let data = JSON.parse(response);
					if(data.success) {
						approot.products = data.content;
						approot.isProcess = false;
					}
				});
			}else
				this.products = [];
		},

		addToCart(sid) {
			let index = this.products.findIndex(product => product.id === sid);
			let product = this.products[index];

			let temp = {
				id: product.id,
				code: product.code,
				name_display: product.name_display,
				unit: product.unit,
				price: product.price,
				quantity: 1,

				// backend data
				purchase: product.purchase,
				product: product.product
			};

			let isAdded = this.orders.find(product => product.id === temp.id);
			if(!isAdded) {
				this.orders.push(temp);
				this.keywordp = '';
			}else
				swal('Hold on!', 'Product already added in cart.', 'warning');
		},

		removeFromCart(sid) {
			let index = this.orders.findIndex(product => product.id === sid);
        	this.orders.splice(index, 1);
		},

		total(quantity, price) {
			// verify quantity value
			quantity = quantity ? quantity : 0;

			let total = parseInt(quantity) * parseFloat(price);
			return total.toFixed(2);
		},

		// appinfo methods
		searchc() {
			if(this.keywordc.length > 0) {
				let postdata = {
	                keyword: this.keywordc,
	                source: 'customer'
	            };

				$.post('pos/request_search', postdata, (response) => {
					let data = JSON.parse(response);
					if(data.success) {
						approot.customers = data.content;
						approot.isProcess = false;
					}
				});
			}else
				this.customers = [];
		},

		setCustomer(cid) {
			let index = this.customers.findIndex(customer => customer.id === cid);
			let customer = this.customers[index];

			this.keywordc = '';
			this.customer = {
				id: cid,
				name: customer.firstname +' '+ customer.lastname
			};
		},

		// appmodal methods
		payment() {
			if(this.orders.length > 0) {
				if(!this.isProcess && !isNaN(this.paid) && this.paid >= 0) {
					if(this.method === 'cash' && this.change < 0)
						swal('Hold on!', 'Cash tendered is less than the amount due.', 'warning');
					else {
						this.isProcess = true;
						let postdata = {
							info: {
								reference: this.generate('POS'),
								customer: this.customer.id,
								discount: this.discount,
								method: this.method,
								paid: this.paid
							},
							orders: this.orders,
							order: this.order
						};

						$.post('pos/request_payment', postdata, (response) => {
							let data = JSON.parse(response);
							if(data.success)
								this.show('overview');
							else{
								swal('Oh noes!', 'Something went wrong while processing.', 'error');
								approot.isProcess = false;
							}
						});
					}
				}
			}else
				swal('Reminder:', 'You cannot process an empty order.', 'info');
		},

		suspend() {
			if(this.orders.length > 0) {
				if(!this.isLoaded) {
					swal('Hold on!', 'Are you sure to suspend this sale?', 'warning', {
						buttons: ['No', 'Yes, Proceed']
					}).then((confirm) => {
						if(confirm) {
							// format parameters
							let stocks = [];
							this.orders.forEach((product) => {
								let temp = {
									stock: product.id,
									quantity: product.quantity
								};

								stocks.push(temp);
							});

							let postdata = {
								info: {
									reference: this.generate('SS'),
									customer: this.customer.id,
									discount: this.discount,
									method: this.method,
									paid: this.paid
								},
								products: stocks
							};

							$.post('pos/request_suspend', postdata, (response) => {
								let data = JSON.parse(response);
								if(data.success) {
									swal('Good job!', 'Order has been suspended.', 'success');
									approot.cancel();
								}
							});
						}
					});
				}else
					swal('Hold on!', 'This order is already suspended.', 'warning');
			}else
				swal('Reminder:', 'You cannot suspend an empty order.', 'info');
		},

		populate() {
        	$.post('pos/request_suspended', (response) => {
        		let data = JSON.parse(response);
        		if(data.success)
        			this.suspends = data.content;
        	});
        },

		load(oid) {
			let postdata = { order: oid };
        	$.post('pos/request_detail', postdata, (response) => {
        		let data = JSON.parse(response);
        		if(data.success) {
        			// order info
					approot.customer.id = data.content.cid;
					approot.customer.name = data.content.cname;
					approot.discount = data.content.discount;
					approot.method = data.content.method;
					approot.paid = data.content.paid;

					// order cart
					approot.orders = data.content.products;

					approot.order = data.content.id;
					approot.isLoaded = true;
					approot.isVisible = false;
        		}else
        			swal('Oh noes!', 'Unable to load selected order.', 'error');
        	});
		},

		setDiscount() {
			swal({
				text: 'Add discount to this order',
				content: "input",
				button: {
					text: "Add Discount"
				}
			}).then(discount => {
				if(discount) {
					let temp = parseInt(discount.trim());
					let isNumber = !isNaN(parseFloat(temp)) && isFinite(temp);

					if(isNumber) {
						if(temp > 100)
							approot.discount = 100;
						else if(temp < 0)
							approot.discount = 0;
						else
							approot.discount = temp;
					}else
						approot.discount = 0;
				}
			});
		},

		cancel() {
			this.customer = {
				id: 3,
				name: 'Walk In Customer'
			};

			this.discount = 0;
			this.method = 'cash';
			this.paid = 0;

			this.order = 0;
			this.orders = [];

			this.isProcess = false;
			this.isVisible = false;
			this.isLoaded = false;
		},

		// miscellaneous
        show(name) {
        	if(name === 'suspended')
        		this.populate();

	        if(!this.isProcess || name === 'overview')
	            this.modal = name;
	            
	        this.isVisible = true;
        },

        generate(prefix = 'POS') {
			let randCode = Math.floor(Math.random() * 8999) + 1000;
			let baseCode = [
				new Date().getMonth(),
				new Date().getDate(),
				new Date().getFullYear(),
				new Date().getHours(),
				new Date().getMinutes(),
				new Date().getSeconds(),
				new Date().getMilliseconds()
			];

			let bCodeVal = 0;
			baseCode.forEach((baseCode) => {
				bCodeVal += parseInt(baseCode);
			});

			// build barcode
			let initialCode = prefix.toUpperCase();
			let finalCode = [initialCode, bCodeVal, randCode];

			return finalCode.join('-');
		},

		config() {
			$.post('misc/load_settings', (response) => {
				let data = JSON.parse(response);
				this.settings = data;
			});
		},

		toggle() {
			if(this.grid) this.grid = false;
			else this.grid = true;

			// clear search
			this.keywordp = '';
		}
	},
	computed: {
		cart() {
			let cart = { base: 0, full: 0 };
			if(this.orders.length > 0) {
				let total = 0;
				this.orders.forEach((product) => {
					if(product.quantity)
						total += parseInt(product.quantity);
				});

				cart.base = this.orders.length;
				cart.full = total;
			}

			return cart;
		},

		stotal() {
			let total = 0.00;
			if(this.orders.length > 0) {
				this.orders.forEach((product) => {
					if(product.quantity) {
						let temp = parseInt(product.quantity) * parseFloat(product.price);
						total += temp;
					}
				});
			}

			return total.toFixed(2);
		},

		atotal() {
			let temp = parseFloat(this.discount / 100) * this.stotal;
			return parseFloat(this.stotal - temp).toFixed(2);
		},

		change() {
			let temp = parseFloat(this.paid) - parseFloat(this.atotal);
			return temp.toFixed(2);
		}
	},
    components: {
        'app-pagination': pagination
    },
	created() {
		this.cancel();
		this.config();
	}
});