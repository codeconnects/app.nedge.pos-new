var pagination = {
	props: {
		current: {
			type: Number,
			required: true
		},
		margin:{
			type: Number,
			required: true
		},
		total: {
			type: Number,
			required: true
		}
	},
	computed: {
		pages() {
			let marginDiff = this.current - this.margin;
			let marginSum = this.current + this.margin;
			let margin = (this.margin *2);

			let loopStart;
			let loopEnd;

			if(this.total > (margin +1)) {
				loopStart = marginDiff > 1 ? marginDiff : 1;
				loopEnd = marginSum > this.total ? this.total : marginSum;
				loopEnd = (loopStart + margin) > this.total ? loopEnd : loopStart + margin;
			}else {
				loopStart = 1;
				loopEnd = this.total;
			}

			let temp = [];
			for(let i = loopStart; i <= loopEnd; i++) {
				temp.push({ value: i, active: i === this.current });
			}

			return temp;
		},

		pagePrev() {
			let diff = this.current -1;
			return diff > 1 ? diff : 1;
		},

		pageNext() {
			let sum = this.current +1;
			return sum > this.total ? this.total : sum;
		}
	},
	template: `
	<ul v-if="total > 1" class="pagination">
	    <li v-if="total > ((margin * 2) +1)" class="page-item">
	        <a @click="$emit('click', 1)" class="page-link">
	            <i class="fas fa-angle-double-left"></i>
	        </a>
	    </li>
	    <li class="page-item">
	        <a @click="$emit('click', pagePrev)" class="page-link">
	            <i class="fas fa-angle-left"></i>
	        </a>
	    </li>
	    <li v-for="page in pages" class="page-item" v-bind:class="{ active: page.active }">
	        <a @click="$emit('click', page.value)" class="page-link">{{ page.value }}</a>
	    </li>
	    <li class="page-item">
	        <a @click="$emit('click', pageNext)" class="page-link">
	            <i class="fas fa-angle-right"></i>
	        </a>
	    </li>
	    <li v-if="total > ((margin * 2) +1)" class="page-item">
	        <a @click="$emit('click', total)" class="page-link">
	            <i class="fas fa-angle-double-right"></i>
	        </a>
	    </li>
	</ul>`
};