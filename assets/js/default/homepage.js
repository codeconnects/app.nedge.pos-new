var approot = new Vue({
	el: '#approot',
	data: {
		user: {
			email: '',
			pass: ''
		},

		// miscellaneous
		isProcess: false,
	},
	methods: {
		login() {
			this.isProcess = true;

			let postdata = { user: this.user };
			$.post('admin/account/login', postdata, (response) => {
				let data = JSON.parse(response);
                if(!data.success) {
                	let error_msg = 'No account associated with this email.';

                	// set error message
                	if(data.error === 1) error_msg = 'You entered an invalid password.';
                	else if(data.error === 2) error_msg = 'This account is currently inactive.'; 

                	swal('Hold on!', error_msg, 'warning');
                	this.isProcess = false;
                }else
                	window.location.href = 'admin/overview';
			});
		}
	}
});