/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.30-MariaDB : Database - app.nedge.pos-new
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `audit_trail` */

DROP TABLE IF EXISTS `audit_trail`;

CREATE TABLE `audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affected_table` varchar(50) DEFAULT NULL,
  `process` enum('create','update','delete') DEFAULT 'create',
  `value` text,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `audit_trail` */

/*Table structure for table `brands` */

DROP TABLE IF EXISTS `brands`;

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `brands` */

insert  into `brands`(`id`,`name`,`is_active`) values (1,'General',1),(2,'Brand 1',1),(3,'Brand 2',1),(4,'Brand 3',0);

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`is_active`) values (1,'General',1),(2,'Foods',1),(3,'Drinks',1),(4,'Services',0);

/*Table structure for table `category_sub` */

DROP TABLE IF EXISTS `category_sub`;

CREATE TABLE `category_sub` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `category_sub` */

insert  into `category_sub`(`id`,`code`,`name`,`parent`,`is_active`) values (1,'General','General',1,1),(2,'SUB-1','Junkfoods',2,1),(3,'SUB-2','Bread',2,1),(4,'SUB-3','Water',3,1),(5,'SUB-4','Beer',3,1),(6,'SUB-5','Juice',3,0);

/*Table structure for table `currency` */

DROP TABLE IF EXISTS `currency`;

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT '-',
  `name` varchar(50) DEFAULT '-',
  `exchange` double(11,4) DEFAULT '0.0000',
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `currency` */

insert  into `currency`(`id`,`code`,`name`,`exchange`,`is_active`) values (1,'USD','US Dollar',0.0000,1),(2,'PHP','Philippine Peso',0.0180,1);

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login_attempts` */

/*Table structure for table `login_logs` */

DROP TABLE IF EXISTS `login_logs`;

CREATE TABLE `login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login_logs` */

/*Table structure for table `login_session` */

DROP TABLE IF EXISTS `login_session`;

CREATE TABLE `login_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(50) DEFAULT NULL,
  `type` enum('login','logout') DEFAULT 'login',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login_session` */

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT '-',
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT '-',
  `country` varchar(255) DEFAULT NULL,
  `postal` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT '-',
  `phone` varchar(20) DEFAULT '-',
  `company` varchar(255) DEFAULT '-',
  `vat_number` varchar(20) DEFAULT '-',
  `group` int(11) DEFAULT NULL,
  `price_group` int(11) DEFAULT NULL,
  `footer` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `people` */

insert  into `people`(`id`,`firstname`,`lastname`,`address`,`city`,`state`,`country`,`postal`,`email`,`phone`,`company`,`vat_number`,`group`,`price_group`,`footer`,`is_active`) values (1,'General','Biller','-','Panabo','Davao del Norte','PH','8105','biller@app.nedge.com','-','-','-',1,1,NULL,1),(2,'General','Supplier','-','Panabo','Davao del Norte','PH','8105','supplier@app.nedge.com','-','-','-',2,2,NULL,1),(3,'Walk In','Customer','-','Panabo','Davao del Norte','PH','8105','customer@app.nedge.com','-','-','-',3,3,NULL,1),(4,'James','Malatabon','-','Panabo','Davao del Norte','PH','8105','james@app.nedge.com','-','-','-',3,3,NULL,1),(5,'Juliane','Sering','-','Mandug','Davao del Sur','PH','8000','juliane@app.nedge.com','-','-','-',3,3,NULL,1),(6,'Nick','Alcala','-','Toril','Davao del Sur','PH','8000','nick@app.nedge.com','-','-','-',3,3,NULL,1);

/*Table structure for table `people_group` */

DROP TABLE IF EXISTS `people_group`;

CREATE TABLE `people_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `discount` double(11,2) DEFAULT '0.00',
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `people_group` */

insert  into `people_group`(`id`,`name`,`discount`,`description`,`is_active`) values (1,'Biller',0.00,NULL,1),(2,'Supplier',0.00,NULL,1),(3,'Customer',0.00,NULL,1);

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) unsigned NOT NULL,
  `product_type` enum('standard','combo','digital','service') DEFAULT 'standard',
  `barcode_type` varchar(20) DEFAULT 'code128',
  `code` varchar(20) DEFAULT NULL,
  `code_hsn` varchar(20) DEFAULT '-',
  `name` varchar(255) DEFAULT NULL,
  `name_display` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `category` int(11) unsigned DEFAULT '1',
  `category_sub` int(11) DEFAULT '1',
  `brand` int(11) unsigned DEFAULT '1',
  `unit` int(11) unsigned DEFAULT '1',
  `cb_parent` int(11) DEFAULT '0',
  `cb_quantity` int(11) DEFAULT '0',
  `trigger` int(11) DEFAULT '20',
  `image` varchar(50) DEFAULT 'default.jpg',
  `description` varchar(255) DEFAULT NULL,
  `is_tracked` tinyint(1) DEFAULT '1',
  `is_hidden` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`id`,`product_type`,`barcode_type`,`code`,`code_hsn`,`name`,`name_display`,`slug`,`category`,`category_sub`,`brand`,`unit`,`cb_parent`,`cb_quantity`,`trigger`,`image`,`description`,`is_tracked`,`is_hidden`) values (1,'standard','code128','B-2223-4870','-','Chippy Blue BBQ','Chippy','chippy-blue-bbq',2,2,1,2,0,1,20,'default.jpg',NULL,1,0),(2,'standard','code128','B-2799-9656','-','Piatos Blue Cheese Large','Piatos','piatos-blue-cheese-large',2,2,1,2,0,1,20,'default.jpg',NULL,1,0),(3,'standard','code128','B-2628-6857','-','Nova Red Large Cheese','Nova','nova-red-large-cheese',2,2,1,2,0,1,20,'default.jpg',NULL,1,0),(4,'standard','code128','B-2818-2651','-','Absolute Drinking Water Large','Absolute','absolute-drinking-water-large',3,4,1,2,0,1,20,'default.jpg',NULL,1,0),(5,'standard','code128','B-2148-1710','-','Natures Spring Drinking Water Medium','Natures Spring','natures-spring-drinking-water-medium',2,4,1,2,0,1,20,'default.jpg',NULL,1,0),(6,'standard','code128','B-2460-8665','-','Summit Drinking Water Small','Summit','summit-drinking-water-small',3,4,1,2,0,1,20,'default.jpg',NULL,1,0),(7,'standard','code128','B-2376-3877','-','Red Horse Beer Large','Red Horse','red-horse-beer-large',3,5,1,2,0,1,20,'default.jpg',NULL,1,0),(8,'standard','code128','B-2481-1994','-','This is a test product','Test','this-is-a-test-product',1,1,1,1,0,1,20,'default.jpg',NULL,1,1);

/*Table structure for table `product_promotion` */

DROP TABLE IF EXISTS `product_promotion`;

CREATE TABLE `product_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) DEFAULT NULL,
  `price` double(11,2) DEFAULT '0.00',
  `dt_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_end` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `warehouses` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_promotion` */

/*Table structure for table `product_variants` */

DROP TABLE IF EXISTS `product_variants`;

CREATE TABLE `product_variants` (
  `id` int(11) NOT NULL,
  `product` int(11) DEFAULT NULL,
  `variant` int(11) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `method` enum('add','subtract') DEFAULT 'add',
  `rate` double(11,2) DEFAULT '0.00',
  `type` enum('percentage','fixed') DEFAULT 'fixed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_variants` */

/*Table structure for table `purchase` */

DROP TABLE IF EXISTS `purchase`;

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) DEFAULT NULL,
  `supplier` int(11) DEFAULT '2',
  `warehouse` int(11) DEFAULT '1',
  `tax` int(11) DEFAULT '1',
  `discount` int(11) DEFAULT '0',
  `note` varchar(255) DEFAULT NULL,
  `status` enum('pending','received','returned') DEFAULT 'pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchase` */

insert  into `purchase`(`id`,`datetime`,`reference`,`supplier`,`warehouse`,`tax`,`discount`,`note`,`status`) values (1,'2018-12-20 18:06:20','PUR-2754-9088',2,1,1,0,NULL,'received'),(2,'2018-12-20 18:06:41','PUR-2435-4258',2,1,1,0,NULL,'received'),(3,'2018-12-20 18:07:01','PUR-2828-9164',2,1,1,0,NULL,'received'),(4,'2018-12-20 18:08:05','PUR-2665-6160',2,1,1,0,NULL,'returned'),(5,'2018-12-20 18:08:22','PUR-2494-7120',2,1,1,0,NULL,'returned');

/*Table structure for table `purchase_adjust` */

DROP TABLE IF EXISTS `purchase_adjust`;

CREATE TABLE `purchase_adjust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `purchase` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_adjust` */

insert  into `purchase_adjust`(`id`,`datetime`,`purchase`,`product`,`quantity`) values (34,'2018-12-20 18:09:39',1,1,50),(35,'2018-12-20 18:09:39',1,2,50),(36,'2018-12-20 18:09:39',1,3,50),(37,'2018-12-20 18:09:39',2,4,50),(38,'2018-12-20 18:09:39',2,5,50),(39,'2018-12-20 18:09:39',2,6,50);

/*Table structure for table `purchase_detail` */

DROP TABLE IF EXISTS `purchase_detail`;

CREATE TABLE `purchase_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_detail` */

insert  into `purchase_detail`(`id`,`purchase`,`product`,`price`,`quantity`) values (34,1,1,20.00,30),(35,1,2,20.00,30),(36,1,3,20.00,30),(37,2,4,25.00,30),(38,2,5,25.00,30),(39,2,6,25.00,30),(40,3,7,80.00,50),(41,4,1,10.00,10),(42,4,2,10.00,10),(43,4,3,10.00,10),(44,5,4,10.00,10),(45,5,5,10.00,10),(46,5,6,10.00,10);

/*Table structure for table `purchase_payment` */

DROP TABLE IF EXISTS `purchase_payment`;

CREATE TABLE `purchase_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase` int(11) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` double(11,2) DEFAULT '0.00',
  `operator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_payment` */

insert  into `purchase_payment`(`id`,`purchase`,`datetime`,`amount`,`operator`) values (1,1,'2018-12-26 17:20:41',1500.00,2),(2,1,'2018-12-26 17:20:49',200.00,2),(3,1,'2018-12-26 17:20:50',100.00,2),(4,2,'2018-12-26 17:21:11',1050.00,2),(5,2,'2018-12-26 17:21:13',1100.00,2),(6,2,'2018-12-26 17:21:15',50.00,2),(7,2,'2018-12-26 17:21:16',50.00,2),(8,3,'2018-12-26 17:21:34',1500.00,2),(9,3,'2018-12-26 17:21:35',1500.00,2),(10,3,'2018-12-26 17:21:38',1000.00,2);

/*Table structure for table `purchase_return` */

DROP TABLE IF EXISTS `purchase_return`;

CREATE TABLE `purchase_return` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_return` */

insert  into `purchase_return`(`id`,`purchase`,`product`,`price`,`quantity`) values (7,4,1,10.00,10),(8,4,2,10.00,10),(9,4,3,10.00,10);

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT '0',
  `method` enum('cash','debit','gift-card') DEFAULT 'cash',
  `paid` double(10,2) DEFAULT '0.00',
  `warehouse` int(11) DEFAULT '1',
  `operator` int(11) DEFAULT NULL,
  `status` enum('completed','returned') DEFAULT 'completed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales` */

insert  into `sales`(`id`,`datetime`,`reference`,`customer`,`discount`,`method`,`paid`,`warehouse`,`operator`,`status`) values (1,'2018-12-26 15:22:48','POS-2982-6565',4,0,'debit',0.00,1,1,'completed'),(2,'2018-12-26 15:23:34','POS-2601-3976',5,0,'debit',0.00,1,1,'completed'),(3,'2018-12-26 15:23:54','POS-3023-8709',6,15,'cash',306.00,1,1,'completed'),(4,'2018-12-26 15:25:36','SLM-3124-8883',3,0,'cash',0.00,1,1,'returned'),(5,'2018-12-26 15:26:19','SLM-2260-6313',3,0,'cash',90.00,1,1,'completed'),(6,'2018-12-26 15:26:40','SLM-2473-5957',3,0,'cash',120.00,1,1,'completed'),(7,'2018-12-26 16:00:34','SLM-2287-3482',3,0,'cash',0.00,1,1,'completed'),(8,'2018-12-26 16:09:14','SLM-2924-5155',3,0,'cash',150.00,1,1,'completed'),(9,'2018-12-26 16:09:26','SLM-2727-6103',3,0,'debit',0.00,1,1,'completed');

/*Table structure for table `sales_detail` */

DROP TABLE IF EXISTS `sales_detail`;

CREATE TABLE `sales_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale` int(11) DEFAULT NULL,
  `purchase` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

/*Data for the table `sales_detail` */

insert  into `sales_detail`(`id`,`sale`,`purchase`,`product`,`price`,`quantity`) values (49,1,1,3,15.00,2),(50,1,1,2,15.00,2),(51,1,1,1,15.00,2),(52,1,2,4,25.00,1),(53,2,1,3,15.00,3),(54,2,1,2,15.00,3),(55,2,2,6,25.00,3),(56,3,1,3,15.00,3),(57,3,2,4,25.00,3),(58,3,3,7,80.00,3),(59,4,1,3,15.00,5),(60,4,1,2,15.00,5),(61,4,2,4,25.00,5),(62,5,2,6,25.00,1),(63,5,2,5,25.00,1),(64,5,2,4,25.00,1),(65,5,1,3,15.00,1),(66,6,1,3,15.00,1),(67,6,1,2,15.00,1),(68,6,1,1,15.00,1),(69,6,2,6,25.00,1),(70,6,2,5,25.00,1),(71,6,2,4,25.00,1),(72,7,1,3,15.00,3),(73,7,1,2,15.00,3),(74,7,1,1,15.00,3),(75,8,2,6,25.00,2),(76,8,2,5,25.00,2),(77,8,2,4,25.00,2),(78,9,3,7,80.00,10);

/*Table structure for table `sales_payment` */

DROP TABLE IF EXISTS `sales_payment`;

CREATE TABLE `sales_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale` int(11) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` double(11,2) DEFAULT '0.00',
  `operator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `sales_payment` */

insert  into `sales_payment`(`id`,`sale`,`datetime`,`amount`,`operator`) values (23,1,'2018-12-26 15:43:19',10.00,2),(24,1,'2018-12-26 15:43:21',10.00,2),(25,1,'2018-12-26 15:43:22',10.00,2),(26,1,'2018-12-26 15:43:23',10.00,2),(27,1,'2018-12-26 15:44:16',75.00,2),(28,2,'2018-12-26 15:44:54',100.00,2),(29,2,'2018-12-26 15:44:55',50.00,2),(30,2,'2018-12-26 15:44:57',15.00,2),(31,4,'2018-12-26 15:45:46',275.00,2),(32,7,'2018-12-26 16:08:18',135.00,2),(33,9,'2018-12-26 16:09:53',100.00,2),(34,9,'2018-12-26 16:09:54',100.00,2),(35,9,'2018-12-26 16:09:59',100.00,2),(36,9,'2018-12-26 16:10:03',150.00,2),(37,9,'2018-12-26 16:10:04',250.00,2),(38,9,'2018-12-26 16:10:06',100.00,2);

/*Table structure for table `stocks` */

DROP TABLE IF EXISTS `stocks`;

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `price` double(11,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  `is_expired` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `stocks` */

insert  into `stocks`(`id`,`purchase`,`product`,`price`,`quantity`,`is_expired`) values (25,1,1,15.00,14,0),(26,1,2,15.00,7,0),(27,1,3,15.00,1,0),(28,2,4,25.00,10,0),(29,2,5,25.00,16,0),(30,2,6,25.00,13,0),(31,3,7,80.00,37,0),(32,4,1,10.00,0,0),(33,4,2,10.00,0,0),(34,4,3,10.00,0,0);

/*Table structure for table `suspended` */

DROP TABLE IF EXISTS `suspended`;

CREATE TABLE `suspended` (
  `id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference` varchar(50) DEFAULT NULL,
  `customer` int(11) DEFAULT '3',
  `discount` int(11) DEFAULT '0',
  `method` enum('cash','debit','gift-card') DEFAULT 'cash',
  `paid` double(10,2) DEFAULT '0.00',
  `operator` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `suspended` */

insert  into `suspended`(`id`,`datetime`,`reference`,`customer`,`discount`,`method`,`paid`,`operator`,`is_completed`) values (1,'2018-12-20 18:11:08','SS-2134-8921',4,5,'cash',0.00,1,0),(2,'2018-12-20 18:11:38','SS-2941-5938',5,10,'cash',0.00,1,0),(3,'2018-12-26 15:23:54','SS-2936-1449',6,15,'cash',0.00,1,1),(4,'2018-12-26 15:05:40','SS-2122-3282',3,0,'cash',0.00,1,0);

/*Table structure for table `suspended_detail` */

DROP TABLE IF EXISTS `suspended_detail`;

CREATE TABLE `suspended_detail` (
  `suspended` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `suspended_detail` */

insert  into `suspended_detail`(`suspended`,`stock`,`quantity`) values (1,27,1),(1,26,1),(1,25,1),(2,28,2),(2,30,2),(2,29,2),(3,27,3),(3,28,3),(3,31,3),(4,27,3),(4,26,3),(4,28,1);

/*Table structure for table `tax_rates` */

DROP TABLE IF EXISTS `tax_rates`;

CREATE TABLE `tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `rate` double(11,2) DEFAULT '0.00',
  `type` enum('percentage','fixed') DEFAULT 'fixed',
  `method` enum('exclusive','inclusive') DEFAULT 'inclusive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tax_rates` */

insert  into `tax_rates`(`id`,`name`,`code`,`rate`,`type`,`method`) values (1,'VAT Free','VFREE',0.00,'fixed','inclusive'),(2,'VAT10','V10',10.00,'percentage','inclusive'),(3,'VAT20','V20',20.00,'percentage','exclusive'),(4,'VAT25','V25',25.00,'fixed','exclusive');

/*Table structure for table `units` */

DROP TABLE IF EXISTS `units`;

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT '-',
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `units` */

insert  into `units`(`id`,`name`,`value`,`is_active`) values (1,'General','-',1),(2,'Piece','PCS',1),(3,'Dozen','DZN',1),(4,'Bundle','BND',1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT 'male',
  `bday` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` int(1) DEFAULT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`firstname`,`lastname`,`gender`,`bday`,`email`,`mobile`,`pass`,`role`,`created_dt`,`created_by`,`is_active`) values (1,'James','Malatabon','male','1994-11-19 00:00:00','james@nedge.com','09123456789','$2y$10$u3Rec2Az0TQCaZgLy5PdI.lWm5jUsKiNYIGk2i9svpBr52RGjlxoi',1,'2019-01-26 04:57:09',1,1),(2,'Juliane Amethyst','Sering','female','1994-10-05 00:00:00','sering@nedge.com','09123456789','$2y$10$7GtAjtZyXquj3ga2Pnx1BuVDQRs8GHuxtf4QHshNHdB0CzRUaIcqq',2,'2019-01-26 04:57:49',1,1),(3,'Nick','Alcala','male','1994-05-21 00:00:00','nick@nedge.com','09987654321','$2y$10$wTGZ6Ar9thckdBH9MSFLzOL9ncN/bWKIK3kZPXKHRiVw.1mYM7YKq',3,'2019-01-26 04:59:10',1,1),(4,'John','Doe','male','1994-10-21 00:00:00','john@nedge.com','09123459876','$2y$10$QcZwsgt58IDAzSivRgVZk.2V8XbjurnWTx3nxQJHwJ15bI6dxmv/e',2,'2019-01-26 05:00:33',1,1),(5,'Jane','Doe','female','1994-10-21 00:00:00','jane@nedge.com','09987612345','$2y$10$m221Ql8lBos5a3zqR9pUeulsPZrxNfRIj74CQoYvS4qJHhBWSQiZS',3,'2019-01-26 05:00:48',1,0),(6,'C','Test','male','1994-12-12 00:00:00','c@nedge.com','123','$2y$10$Y78VO0slBcxAhF5/jFOHoesGEYYRG2WRwggI1NFTPuvzIbyqxfQmO',3,'2019-01-26 05:01:06',1,1);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`name`,`permission`) values (1,'super admin',NULL),(2,'admin',NULL),(3,'cashier',NULL),(4,'manager',NULL);

/*Table structure for table `variants` */

DROP TABLE IF EXISTS `variants`;

CREATE TABLE `variants` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `variants` */

insert  into `variants`(`id`,`name`,`description`,`is_active`) values (1,'Size','Product size',1),(2,'Color','Product color',1);

/*Table structure for table `warehouse` */

DROP TABLE IF EXISTS `warehouse`;

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT '-',
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT '-',
  `phone` varchar(20) DEFAULT '-',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `warehouse` */

insert  into `warehouse`(`id`,`code`,`name`,`address`,`city`,`state`,`country`,`email`,`phone`,`is_deleted`) values (1,'General','General Warehouse','-','-','-','-','-','-',0),(2,'WH-1','Warehouse 1','-','Tagum','Davao del Norte','Philippines','-','-',0),(3,'WH-2','Warehouse 2','-','Panabo','Davao del Norte','Philippines','-','-',0),(4,'WH-3','Warehouse 3','-','Davao','Davao','Philippines','-','-',0),(5,'WH-4','Warehouse 4','-','Taguig','Manila','Philippines','-','-',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
